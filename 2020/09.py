from aocd.models import Puzzle
from dotenv import load_dotenv
import copy
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2020, day=9)
print(puzzle.input_data)

# data = []
# with open("2020/input/09_test.txt") as file:
#     data = [int(line[:-1]) for line in file]

data = [int(n) for n in puzzle.input_data.split('\n')]

preamble = set()
preamble_ordered = deque()
p1 = None
for i, n in enumerate(data):
    if i < 25:
        preamble.add(n)
        preamble_ordered.append(n)
    else:
        match = False
        for j in preamble:
            diff = n - j
            if diff in preamble:
                match = True
                break
        if not match:
            p1 = n
            p1_idx = i
            break
        preamble.add(n)
        preamble_ordered.append(n)
        preamble.remove(preamble_ordered.popleft())

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = None
for l in range(p1_idx):
    for r in range(l+1, p1_idx):
        r = data[l:r+1]
        if sum(r) == p1:
            p2 = min(r) + max(r)
            break
    if p2: break

print('Part 2:', p2)
# puzzle.answer_b = p2