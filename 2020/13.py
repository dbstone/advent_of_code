from aocd.models import Puzzle
from dotenv import load_dotenv
from utils.chinese_remainder import chinese_remainder as crt

load_dotenv()
puzzle = Puzzle(year=2020, day=13)

earliest, busses = puzzle.input_data.split('\n')
busses = busses.split(',')
active_busses = [int(n) for n in busses if n != 'x']

wait, bus = min((n - int(earliest) % n, n) for n in active_busses)
pt1 = wait * bus

print('Part 1:', pt1)
# puzzle.answer_a = pt1

moduli = [-i for i, n in enumerate(busses) if n != 'x']
pt2 = crt(active_busses, moduli)

print('Part 2:', pt2)
# puzzle.answer_b = pt2