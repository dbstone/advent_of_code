from aocd.models import Puzzle
from dotenv import load_dotenv
import copy
# from collections import deque

load_dotenv()
puzzle = Puzzle(year=2020, day=11)
# print(puzzle.input_data)

# seats = []
# with open("2020/input/11_test.txt") as file:
#     seats = [line[:-1] for line in file]

seats = [list(n) for n in puzzle.input_data.split('\n')]
# data = [n for n in puzzle.input_data.split('\n')]

# seats = list(rowcopy.deepcopy(puzzle.input_data)
# seats = [list(row) for row in seats]
# i = 0
while True:
    # print(i)
    # i+=1
    changed = 0
    new_seats = copy.deepcopy(seats)
    for y in range(len(new_seats)):
        for x in range(len(new_seats[0])):
            # print(x, y)
            c = seats[y][x]
            if c == 'L':
                adjs = 0
                for x2 in range(-1, 2):
                    for y2 in range(-1, 2):
                        if x2 == 0 and y2 == 0:
                            continue
                        yy = y
                        xx = x
                        while True:
                            yy += y2
                            xx += x2
                            if yy not in range(len(seats)) or xx not in range(len(seats[0])):
                                break
                            if seats[yy][xx] == '#':
                                adjs += 1
                                break
                            elif seats[yy][xx] == 'L':
                                break
                if adjs == 0:
                    new_seats[y][x] = '#'
                    changed += 1
            elif c == '#':
                adjs = 0
                for x2 in range(-1, 2):
                    for y2 in range(-1, 2):
                        if x2 == 0 and y2 == 0:
                            continue
                        yy = y
                        xx = x
                        while True:
                            yy += y2
                            xx += x2
                            if yy not in range(len(seats)) or xx not in range(len(seats[0])):
                                break
                            if seats[yy][xx] == '#':
                                adjs += 1
                                break
                            elif seats[yy][xx] == 'L':
                                break
                if adjs >= 5:
                    new_seats[y][x] = 'L'
                    changed += 1

    seats = new_seats
    # [print(row) for row in seats]
    # print()

    if changed == 0:
        break

occupied = 0
for y in range(len(seats)):
    for x in range(len(seats[0])):
        if seats[y][x] == '#':
            occupied += 1

# print('Part 1:', occupied)
# puzzle.answer_a = occupied

print('Part 2:', occupied)
# puzzle.answer_b = occupied