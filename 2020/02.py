valid1 = 0
valid2 = 0
with open("2020/input/02.txt") as file:
    for line in file:
        policy = line[:line.find(':')]
        p_min = int(policy[:policy.find('-')])
        p_max = int(policy[policy.find('-')+1:policy.find(' ')])
        c = policy[-1]
        password = line[line.find(':')+2:]
        count = password.count(c)
        if count >= p_min and count <= p_max:
            valid1 += 1
        if (password[p_min-1] == c) != (password[p_max-1] == c):
            valid2 += 1

print('Part 1:', valid1)
print('Part 2:', valid2)