from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import *
import copy

load_dotenv()
puzzle = Puzzle(year=2020, day=17)

data = puzzle.input_data.split('\n')
ya, yb = 0, len(data)-1
xa, xb = 0, len(data[0])-1
za, zb = 0, 0

active = set()
for y, row in enumerate(data):
    for x in range(len(row)):
        if row[x] == '#':
            active.add((x, y, 0))

def get_active_neighbors(x, y, z, active):
    num_active = 0
    for x2 in range(x-1, x+2):
        for y2 in range(y-1, y+2):
            for z2 in range(z-1, z+2):
                if x2 == x and y2 == y and z2 == z:
                    continue
                if (x2, y2, z2) in active:
                    num_active += 1
    return num_active

for _ in range(6):
    new_active = copy.deepcopy(active)
    for z in range(za-1, zb+2):
        for y in range(ya-1, yb+2):
            for x in range(xa-1, xb+2):
                neighbors = get_active_neighbors(x, y, z, active)
                if (x, y, z) in active:
                    if neighbors != 2 and neighbors != 3:
                        new_active.remove((x, y, z))
                else:
                    if neighbors == 3:
                        new_active.add((x, y, z))
    active = new_active
    xs, ys, zs = zip(*active)
    xa, xb = min(xs), max(xs)
    ya, yb = min(ys), max(ys)
    za, zb = min(zs), max(zs)

pt1 = len(active)
print('Part 1:', pt1)
# puzzle.answer_a = pt1