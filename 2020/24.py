from aocd.models import Puzzle
from dotenv import load_dotenv
import math
from collections import *
import copy

load_dotenv()
puzzle = Puzzle(year=2020, day=24)

data = puzzle.input_data.split('\n')

black = set()
for d in data:
    i = 0
    x, y, z = 0, 0, 0
    while i < len(d):
        c = d[i]
        if c == 's':
            i += 1
            if d[i] == 'w':
                x -= 1
                z += 1
            elif d[i] == 'e':
                y -= 1
                z += 1
        if c == 'n':
            i += 1
            if d[i] == 'w':
                y += 1
                z -= 1
            elif d[i] == 'e':
                x += 1
                z -= 1
        if c == 'e':
            x += 1
            y -= 1
        if c == 'w':
            x -= 1
            y += 1
        
        i += 1
    
    tile = (x, y, z)
    if tile in black:
        black.remove(tile)
    else:
        black.add(tile)

pt1 = len(black)    
print('Part 1:', pt1)
# puzzle.answer_a = pt1

neighbors = [(0, -1, 1), (1, -1, 0), (1, 0, -1), (0, 1, -1), (-1, 1, 0), (-1, 0, 1)]
for _ in range(100):
    new_black = black.copy()
    white_to_check = set()
    for tile in black:
        black_neighbors = 0
        for neighbor in neighbors:
            tile2 = (tile[0]+neighbor[0], tile[1]+neighbor[1], tile[2]+neighbor[2])
            if tile2 in black:
                black_neighbors += 1
            else:
                white_to_check.add(tile2)
        if black_neighbors == 0 or black_neighbors > 2:
            new_black.remove(tile)
    
    for tile in white_to_check:
        black_neighbors = 0
        for neighbor in neighbors:
            tile2 = (tile[0]+neighbor[0], tile[1]+neighbor[1], tile[2]+neighbor[2])
            if tile2 in black:
                black_neighbors += 1

        if black_neighbors == 2:
            new_black.add(tile)

    black = new_black

pt2 = len(black)
print('Part 2:', pt2)
# puzzle.answer_b = pt2