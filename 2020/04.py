from aocd.models import Puzzle
from dotenv import load_dotenv
import re

load_dotenv()
puzzle = Puzzle(year=2020, day=4)

data = [line for line in puzzle.input_data.split('\n\n')]

passports = []
for d in data:
    passports.append(re.split('[\\n\\r\\s]+', d))

reqs = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}

valid = 0
for passport in passports:
    if reqs.issubset(set(field[:3] for field in passport)):
        valid += 1

print('Part 1:', valid)
# puzzle.answer_a = valid

res = {'hcl':'#[0-9a-f]{6}', 'ecl':'(amb|beu|brn|gry|grn|hzl|oth)', 'pid':'[0-9]{9}'}

valid = 0
for passport in passports:
    valids = {'byr':False, 'iyr':False, 'eyr':False, 'hgt':False, 'hcl':False, 'ecl':False, 'pid':False}

    for field in passport:
        label = field[:3]
        if label in valids:
            s = field[4:]
            if label == 'byr':
                if int(s) <= 2002 and int(s) >= 1920:
                    valids[label] = True
            elif label == 'iyr':
                if int(s) <= 2020 and int(s) >= 2010:
                    valids[label] = True
            elif label == 'eyr':
                if int(s) <= 2030 and int(s) >= 2020:
                    valids[label] = True
            elif label == 'hgt':
                if s[-2:] == 'in' and int(s[:-2]) >= 59 and int(s[:-2]) <= 76:
                    valids[label] = True
                elif s[-2:] == 'cm' and int(s[:-2]) >= 150 and int(s[:-2]) <= 193:
                    valids[label] = True
            else:
                pat = res[label]
                if re.fullmatch(pat, s):
                    valids[label] = True

    if all(valids.values()):
        valid += 1

print('Part 2:', valid)
# puzzle.answer_b = valid
