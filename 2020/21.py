from aocd.models import Puzzle
from dotenv import load_dotenv
import math
from collections import *
import copy

load_dotenv()
puzzle = Puzzle(year=2020, day=21)

# data = open('2020/input/test.txt').read().split('\n')
data = puzzle.input_data.split('\n')

recipes = []
all_ingredients = set()
all_allergens = set()
for line in data:
    ingredients, allergens = line[:-1].split(' (contains ')
    ingredients = set(ingredients.split(' '))
    allergens = set(allergens.split(', '))
    recipes.append((ingredients, allergens))
    all_ingredients |= ingredients
    all_allergens |= allergens

possible_matches = {}
for allergen in all_allergens:
    for recipe in recipes:
        if allergen in recipe[1]:
            if allergen not in possible_matches:
                possible_matches[allergen] = recipe[0].copy()
            else:
                possible_matches[allergen] &= recipe[0]


possible_allergens = set.union(*possible_matches.values())

pt1 = 0
for recipe in recipes:
    for ingredient in recipe[0]:
        if ingredient not in possible_allergens:
            pt1 += 1

print('Part 1:', pt1)
# puzzle.answer_a = pt1

not_possible = all_ingredients - possible_allergens

for match in possible_matches:
    possible_matches[match] -= not_possible

matches = {}
while len(matches) < len(all_allergens):
    for allergen, ingredients in possible_matches.items():
        if len(ingredients) == 1:
            matches[allergen] = next(iter(ingredients))
        else:
            for match in matches.values():
                possible_matches[allergen].discard(match)

pt2 = ','.join([matches[allergen] for allergen in sorted(matches)])
print('Part 2:', pt2)
# puzzle.answer_b = pt2