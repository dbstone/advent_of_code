from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cache

load_dotenv()
puzzle = Puzzle(year=2020, day=19)

rules, messages = puzzle.input_data.split('\n\n')
rules = dict([tuple(rule.split(': ')) for rule in rules.split('\n')])
messages = set(messages.split('\n'))

@cache
def get_matches(rule):
    if '\"' in rule:
        return set(rule[1])
    elif '|' in rule:
        m = set()
        subrules = rule.split(' | ')
        for subrule in subrules:
            m |= get_matches(subrule)
        return m
    else:
        m = set()
        subrules = rule.split(' ')
        first = get_matches(rules[subrules[0]])
        if len(subrules) == 1:
            return first
        rest = get_matches(' '.join(subrules[1:]))
        for match in first:
            for match2 in rest:
                m.add(match + match2)
        return m

matches_0 = get_matches(rules['0'])
pt1 = len(messages & matches_0)
print('Part 1:', pt1)
# puzzle.answer_a = pt1

matches_42 = get_matches('42')
N_42 = len(next(iter(matches_42))) # length of strings matching rule 42
matches_31 = get_matches('31')
N_31 = len(next(iter(matches_31))) # length of strings matching rule 31

pt2 = 0
for message in messages:
    m2 = message

    num_42 = 0
    while m2[:N_42] in matches_42:
        num_42 += 1
        m2 = m2[N_42:]
    
    num_31 = 0
    while m2[:N_31] in matches_31:
        num_31 += 1
        m2 = m2[N_31:]
    
    if not m2 and num_31 > 0 and num_42 > num_31:
        pt2 += 1

print('Part 2:', pt2)
# puzzle.answer_b = pt2