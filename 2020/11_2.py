from aocd.models import Puzzle
from dotenv import load_dotenv
import copy

load_dotenv()
puzzle = Puzzle(year=2020, day=11)
seats = [list(n) for n in puzzle.input_data.split('\n')]

def get_num_adjacent(x, y, is_p1, seats):
    adjs = 0
    for x2 in range(-1, 2):
        for y2 in range(-1, 2):
            if x2 == 0 and y2 == 0:
                continue
            if is_p1:
                yy = y+y2
                xx = x+x2
                if yy in range(len(seats)) and xx in range(len(seats[0])):
                    if seats[yy][xx] == '#':
                        adjs += 1
            else:
                yy = y
                xx = x
                while True:
                    yy += y2
                    xx += x2
                    if yy not in range(len(seats)) or xx not in range(len(seats[0])):
                        break
                    if seats[yy][xx] == '#':
                        adjs += 1
                        break
                    elif seats[yy][xx] == 'L':
                        break
    return adjs

def solve(is_p1, seats):
    while True:
        changed = 0
        new_seats = copy.deepcopy(seats)
        for y in range(len(new_seats)):
            for x in range(len(new_seats[0])):
                c = seats[y][x]
                if c == 'L':
                    if get_num_adjacent(x, y, is_p1, seats) == 0:
                        new_seats[y][x] = '#'
                        changed += 1
                elif c == '#':
                    if get_num_adjacent(x, y, is_p1, seats) >= (4 if is_p1 else 5):
                        new_seats[y][x] = 'L'
                        changed += 1

        seats = new_seats

        if changed == 0:
            break

    occupied = sum(row.count('#') for row in seats)
    
    return occupied

p1 = solve(True, seats)
print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = solve(False, seats)
print('Part 2:', p2)
# puzzle.answer_b = p2