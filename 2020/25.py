from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2020, day=25)

data = puzzle.input_data.split('\n')

def get_loops(public):
    subj = 1
    loops = 0
    while subj != public: 
        loops += 1
        subj = (subj * 7) % 20201227
    return loops

card_public = int(data[0])
door_public = int(data[1])
door_loops = get_loops(door_public)

pt1 = pow(card_public, door_loops, 20201227)
print('Part 1:', pt1)
# puzzle.answer_a = pt1