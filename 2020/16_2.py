from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import *
from itertools import permutations
import copy

load_dotenv()
puzzle = Puzzle(year=2020, day=16)

# rules, your_ticket, nearby_tickets = [n for n in open("2020/input/12_test.txt").read().split('\n\n')]
rules, your_ticket, nearby_tickets = [n for n in puzzle.input_data.split('\n\n')]
rules = rules.split('\n')
nearby_tickets = set(nearby_tickets.split('\n')[1:])
rules2 = {}
for rule in rules:
    name, ranges = rule.split(': ')
    ranges = ranges.split(' or ')
    ranges2 = []
    for r in ranges:
        a, b = r.split('-')
        ranges2.append((int(a), int(b)))
    rules2[name] = ranges2

def ticket_is_valid(ruleset, ticket):
    for v in ticket.split(','):
        v = int(v)
        match = False
        for rule in rules2.values():
            if (v >= rule[0][0] and v <= rule[0][1]) or (v >= rule[1][0] and v <= rule[1][1]):
                match = True
                break
        if not match:
            return False
    return True
    
invalid = 0
valid_tickets = set()
for ticket in nearby_tickets:
    if ticket_is_valid(rules2, ticket):
        valid_tickets.add(ticket)

# print(len(nearby_tickets))
# print(len(valid_tickets))

# def get_ordered_rules(unused_rules, tickets, depth=0):
#     for rule in rules
#     rules = rules.copy()
#     return None

# print(len(list(permutations(rules2))))
# num_valid = defaultdict(int)
# for r in rules2:
#     good = True
#     perm = list(perm)
#     for ticket in valid_tickets:
#         ticket = ticket.split(',')
#         for i, v in enumerate(ticket):
#             rule = rules2[perm[i]]
#             v = int(v)
#             match = False
#             if (v >= rule[0][0] and v <= rule[0][1]) or (v >= rule[1][0] and v <= rule[1][1]):
#                 match = True
#             if not match:
#                 good = False
#                 break
#         if not good:
#             break
#     if good:
#         ordered_rules = perm
#         break
#         num_valid[r] += 1

valid_positions = defaultdict(set)
for rule in rules2:
    r = rules2[rule]
    for pos in range(len(rules2)):
        good = True
        for ticket in valid_tickets:
            ticket = ticket.split(',')
            v = int(ticket[pos])
            if (v < r[0][0] or v > r[0][1]) and (v < r[1][0] or v > r[1][1]):
                good = False
                break
        if good:
            valid_positions[rule].add(pos)

# print(valid_positions)
# def get_valid_order(valid_positions):
#     if len(valid_positions) == 0:
#         return []
#     name, positions = valid_positions.pop()
#     for n in positions:
#         new_positions = copy.deepcopy(valid_positions)
#         for rule in new_positions:
#             if n in rule:
#                 rule.remove(n)
#         order = get_valid_order(new_positions)
#         if order != None:
#             order.append(name)
#             return order
#     valid_positions.append((name, positions))
#     return None

done = set()

while len(done) < len(valid_positions):
    for positions in valid_positions:
        if positions in done:
            continue
        if len(valid_positions[positions]) == 1:
            for positionsb in valid_positions:
                if positions == positionsb:
                    continue
                pos = list(valid_positions[positions])[0]
                valid_positions[positionsb].discard(pos)
            done.add(positions)

print(valid_positions)
your_ticket = [int(n) for n in your_ticket.split('\n')[1].split(',')]

pt2 = 1
for position in valid_positions:
    if position.find('departure') != -1:
        i = valid_positions[position].pop()
        pt2 *= your_ticket[i]

# order = get_valid_order(list(valid_positions.items()))
# order.reverse()

# print(ordered_rules)

# ordered_rules = get_ordered_rules(rules2, valid_tickets)
# print(ordered_rules)

print('Part 2:', pt2)
# puzzle.answer_b = pt2