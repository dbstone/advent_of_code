from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2020, day=7)

data = []
with open("2020/input/07_test2.txt") as file:
    data = [line[:-1] for line in file]
dp = {}
contained_by = {}

# data = puzzle.input_data.split('\n\n')
# data = puzzle.input_data.split('\n')

for line in data:
    line = line[:-1]
    bag = line[:line.find('bag')-1]
    contains = line[line.find('contain')+8:]
    if contains != 'no other bags':
        contains = contains.split(', ')
        for contained in contains:
            n = int(contained[0])
            color = contained[2:contained.find('bag')-1]
            if color not in contained_by:
                contained_by[color] = {}
            contained_by[color][bag] = n

def get_num_contained_by(bag):
    if bag not in contained_by:
        return 0
    cby = {}
    for container in contained_by[bag]:
        n  + get_num_contained_by(container)
    return n

print(get_num_contained_by('shiny gold'))
#     if bag in dp:
#         return dp[bag]
#     shinygold = 0
#     for b, n in bags[bag].items():
#         if b == 'shiny gold':
#             shinygold += n
#         else:
#             shinygold += get_num_shiny(b) * n

#     dp[bag] = shinygold
#     return shinygold

# for bag in bags:
#     get_num_shiny(bag)
#     # if 'shiny gold' in bags[bag]:
#     #     shinygold += bags[bag]['shiny gold']

# p1 = len([v for v in dp.values() if v > 0])
# print('Part 1:', p1)
# # puzzle.answer_a = p1

# contained_dp = {}

# def get_num_contained(bag):
#     if bag in contained_dp:
#         return contained_dp[bag]
#     cont = 0
#     for b, n in bags[bag].items():
#         cont += get_num_contained(b) * n + n
    
#     contained_dp[bag] = cont
#     return cont

# p2 = get_num_contained('shiny gold')
# print('Part 2:', p2)
# # puzzle.answer_b = p2
