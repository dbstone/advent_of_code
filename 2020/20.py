from aocd.models import Puzzle
from dotenv import load_dotenv
import math

load_dotenv()
puzzle = Puzzle(year=2020, day=20)
data = puzzle.input_data.split('\n\n')

tiles = {}
for d in data:
    lines = d.split('\n')
    uid = int(lines[0].split(' ')[1][:-1])
    tile = []
    for line in lines[1:]:
        row = []
        for c in line:
            row.append(c)
        tile.append(row)
    tiles[uid] = tile

puzz_side_length = int(math.sqrt(len(tiles)))
tile_side_length = len(list(tiles.values())[0][0])

def rotate_tile_90_cw(tile):
    list_of_tuples = zip(*tile[::-1])
    return [list(elem) for elem in list_of_tuples]

def solve_puzzle(unused_tiles, puzz):
    if len(unused_tiles) == 0:
        return puzz
    for uid, tile in list(unused_tiles.items()):
        for j in range(2):
            if j != 0:
                tile.reverse()
            for i in range(4):
                if i != 0:
                    tile = rotate_tile_90_cw(tile)
                if len(puzz) > 1:
                    # handle top
                    top = tile[0]
                    above_bottom = puzz[len(puzz)-2][len(puzz[-1])][1][-1]
                    match = True
                    for a, b in zip(top, above_bottom):
                        if a != b:
                            match = False
                            break
                    if not match:
                        continue
                if puzz and len(puzz[-1]) > 0:
                    # handle left
                    left = rotate_tile_90_cw(tile)[0]
                    left_right = rotate_tile_90_cw(puzz[-1][len(puzz[-1])-1][1])[-1]
                    match = True
                    for a, b in zip(left, left_right):
                        if a != b:
                            match = False
                            break
                    if not match:
                        continue
                del unused_tiles[uid]
                puzz[-1].append((uid, tile))
                if len(puzz[-1]) == puzz_side_length:
                    puzz.append([])
                solved = solve_puzzle(unused_tiles, puzz)
                if solved:
                    return solved
                if not puzz[-1]:
                    puzz.pop()
                puzz[-1].pop()
                unused_tiles[uid] = tile
    return None

puzz = solve_puzzle(tiles, [[]])
puzz = puzz[:-1]
pt1 = math.prod([puzz[0][0][0], puzz[0][-1][0], puzz[-1][0][0], puzz[-1][-1][0]])
print('Part 1:', pt1)
# puzzle.answer_a = pt1

def remove_borders(tile):
    retval = []
    tile = tile[1:-1]
    for row in tile:
        retval.append(row[1:-1])
    return retval

no_border_puzz = []
for row in puzz:
    new_row = []
    for uid, tile in row:
        new_row.append(remove_borders(tile))
    no_border_puzz.append(new_row)

s = puzz_side_length * tile_side_length # loch side length

loch = []
for row in no_border_puzz:
    for y in range(tile_side_length-2):
        sub_row = []
        for tile in row:
            sub_row += tile[y]
        loch.append(sub_row)

f = open("2020/20.loch", "w")
for line in loch:
    for c in line:
        f.write(c)
    f.write('\n')
f.close()

sea_monster = ['                  # ',
               '#    ##    ##    ###',
               ' #  #  #  #  #  #   ']

sea_monster = [list(line) for line in sea_monster]
sea_monster_size = sum(line.count('#') for line in sea_monster)

def count_objects_in_image(obj, img):
    obj_w = len(obj[0])
    obj_h = len(obj)
    count = 0
    for y in range(len(img)-obj_h):
        for x in range(len(img)-obj_w):
            match = True
            for y_offset in range(obj_h):
                for x_offset in range(obj_w):
                    x2 = x+x_offset
                    y2 = y+y_offset
                    if loch[y2][x2] == '.' and sea_monster[y_offset][x_offset] == '#':
                        match = False
                        break
                if not match: break
            if match:
                count += 1
    return count

num_sea_monsters = 0
for flip in range(2):
    for rot in range(4):
        num_sea_monsters = max(num_sea_monsters, count_objects_in_image(sea_monster, loch))
        sea_monster = rotate_tile_90_cw(sea_monster)
    sea_monster.reverse()

surf_size = sum(line.count('#') for line in loch)
pt2 = surf_size - sea_monster_size * num_sea_monsters
print('Part 2:', pt2)
# puzzle.answer_b = pt2

# time to complete = 1:49:36