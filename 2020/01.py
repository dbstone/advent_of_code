entries = []
with open("2020/input/01.txt") as file:
    entries = [int(line) for line in file]

for i in range(len(entries)):
    for j in range(i+1, len(entries)):
            if entries[i] + entries[j] == 2020:
                print('Part 1:', entries[i] * entries[j])

for i in range(len(entries)):
    for j in range(i+1, len(entries)):
        for k in range(j+1, len(entries)):
            if entries[i] + entries[j] + entries[k] == 2020:
                print('Part 2:', entries[i] * entries[j] * entries[k])


