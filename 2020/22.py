from aocd.models import Puzzle
from dotenv import load_dotenv
import math
from collections import *
import copy

load_dotenv()
puzzle = Puzzle(year=2020, day=22)

# p1, p2 = open('2020/input/test.txt').read().split('\n\n')
p1, p2 = puzzle.input_data.split('\n\n')

p1 = deque([int(i) for i in p1.split('\n')[1:]])
p2 = deque([int(i) for i in p2.split('\n')[1:]])

# print(p1)

# while len(p1) > 0 and len(p2) > 0:
#     card1 = p1.popleft()
#     card2 = p2.popleft()

#     if card1 > card2:
#         p1.append(card1)
#         p1.append(card2)
#     else:
#         p2.append(card2)
#         p2.append(card1)

# deck = p1 if p1 else p2
# pt1 = 0
# for i, card in enumerate(reversed(deck)):
#     pt1 += (i + 1) * card

# print('Part 1:', pt1)
# puzzle.answer_a = pt1

def play(p1, p2):
    previous = set()
    while len(p1) > 0 and len(p2) > 0:
        if (tuple(p1), tuple(p2)) in previous:
            return 1
        else:
            previous.add((tuple(p1), tuple(p2)))

        card1 = p1.popleft()
        card2 = p2.popleft()
        if len(p1) >= card1 and len(p2) >= card2:
            # recurse
            winner = play(deque(list(p1)[:card1]).copy(), deque(list(p2)[:card2]).copy())
            if winner == 1:
                p1.append(card1)
                p1.append(card2)
            else:
                p2.append(card2)
                p2.append(card1)
        else: 
            if card1 > card2:
                p1.append(card1)
                p1.append(card2)
            else:
                p2.append(card2)
                p2.append(card1)
    if p1:
        return 1
    else:
        return 2

play(p1, p2)

deck = p1 if p1 else p2
pt2 = 0
for i, card in enumerate(reversed(deck)):
    pt2 += (i + 1) * card

print('Part 2:', pt2)
puzzle.answer_b = pt2