from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2020, day=5)

# data = []
# with open("2020/input/05_test.txt") as file:
#     data = [line[:-1] for line in file]

plane = []
for _ in range(128):
    plane.append([False] * 8)

data = [line for line in puzzle.input_data.split('\n')]

def bin_part(code, l_chr, l, r):
    for c in code:
        m = (r + l) // 2
        if c == l_chr:
            r = m - 1
        else: 
            l = m + 1
    return l

def get_seat_num(code):
    row = bin_part(code[:7], 'F', 0, 127)
    col = bin_part(code[7:], 'L', 0, 7)

    plane[row][col] = True
    return row * 8 + col

p1 = int(max(get_seat_num(code) for code in data))
print('Part 1:', p1)
# puzzle.answer_a = p1

got_first = False
done = False
for row in range(128):
    for col in range(8):
        if plane[row][col]:
            got_first = True
        else:
            if got_first:
                done = True
                p2 = row * 8 + col
                print('Part 2:', p2)
                # puzzle.answer_b = p2
                break
    if done: break

    