from aocd.models import Puzzle
from dotenv import load_dotenv
from math import prod

load_dotenv()
puzzle = Puzzle(year=2020, day=3)
tree_map = [line for line in puzzle.input_data.split('\n')]

def get_trees(dx, dy):
    trees = 0
    for y in range(0, len(tree_map), dy):
        x = (y * dx // dy) % len(tree_map[0])
        if tree_map[y][x] == '#':
            trees += 1
    return trees

print('Part 1:', get_trees(3, 1))

slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
product = prod(get_trees(*slope) for slope in slopes)

print('Part 2:', product)
