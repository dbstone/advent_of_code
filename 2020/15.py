from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2020, day=15)

data = [int(n) for n in puzzle.input_data.split(',')]

def solve(data, last_turn):
    last_spoken = {}
    for i, n in enumerate(data[:-1]):
        last_spoken[n] = i

    prev = data[-1]
    i = len(data)-1
    while i < last_turn-1:
        n = 0
        if prev in last_spoken:
            n = i - last_spoken[prev]
        last_spoken[prev] = i
        i += 1
        prev = n
    return prev

pt1 = solve(data, 2020)
print('Part 1:', pt1)
# puzzle.answer_a = pt1

pt2 = solve(data, 30000000)
print('Part 2:', pt2)
# puzzle.answer_b = pt2