from math import prod

tree_map = []
with open("2020/input/03.txt") as file:
    tree_map = [line[:-1] for line in file]

def get_trees(dx, dy):
    x = 0
    y = 0
    trees = 0
    while y < len(tree_map):
        if tree_map[y][x] == '#':
            trees += 1
        x = (x + dx) % len(tree_map[0])
        y += dy
    return trees

print('Part 1:', get_trees(3, 1))

slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
product = prod(get_trees(*slope) for slope in slopes)

print('Part 2:', product)
