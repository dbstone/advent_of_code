from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cache
from collections import defaultdict

load_dotenv()
puzzle = Puzzle(year=2020, day=7)

data = [line[:-1] for line in puzzle.input_data.split('\n')]

bags = {}
for line in data:
    bag, contents = line.split(' bags contain ')
    bags[bag] = {}
    if contents != 'no other bags':
        for b in contents.split(', '):
            n = int(b[0])
            color = b[2:b.find('bag')-1]
            bags[bag][color] = n

@cache
def contains_shiny_gold(bag):
    for b in bags[bag]:
        if b == 'shiny gold' or contains_shiny_gold(b):
            return True
    return False

p1 = sum([1 for bag in bags if contains_shiny_gold(bag)])
print('Part 1:', p1)
# puzzle.answer_a = p1

@cache
def num_contained(bag):
    cont = 0
    for b, n in bags[bag].items():
        cont += num_contained(b) * n + n
    return cont

p2 = num_contained('shiny gold')
print('Part 2:', p2)
# puzzle.answer_b = p2
