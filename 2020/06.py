from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2020, day=6)

groups = puzzle.input_data.split('\n\n')

p1 = 0
for group in groups:
    s = set(group)
    s.discard('\n')
    p1 += len(s)

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for group in groups:
    lines = [set(line) for line in group.split('\n')]
    p2 += len(set.intersection(*lines))

print('Part 2:', p2)
# puzzle.answer_b = p2
