from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2020, day=23)
data = puzzle.input_data

def play(is_p2):
    cups = [0] * 10
    cups[0] = int(data[0])
    for i, c in enumerate(data[1:]):
        cups[int(data[i])] = int(c)
    
    if is_p2:
        cups[int(data[-1])] = 10
        for i in range(11, 1000001):
            cups.append(i)
        cups.append(cups[0])
    else:
        cups[int(data[-1])] = cups[0]

    highest = max(cups)
    N = len(cups)
    curr = cups[0]
    for _ in range(10000000 if is_p2 else 100):
        first = cups[curr]
        second = cups[first]
        third = cups[second]
        dest = (curr-2) % (N-1) + 1
        while dest in [first, second, third]:
            dest = (dest-2) % (N-1) + 1
        cups[curr] = cups[third]
        cups[third] = cups[dest]
        cups[dest] = first
        curr = cups[curr]

    if is_p2:
        c2 = cups[1]
        c3 = cups[c2]
        return c2 * c3
    else:
        retval = ''
        last = 1
        for _ in range(len(cups)-2):
            retval += str(cups[last])
            last = cups[last]
        return retval

pt1 = play(False)
print('Part 1:', pt1)
# puzzle.answer_a = pt1

pt2 = play(True)
print('Part 2:', pt2)
# puzzle.answer_b = pt2