from aocd.models import Puzzle
from dotenv import load_dotenv
import copy

load_dotenv()
puzzle = Puzzle(year=2020, day=8)
# print(puzzle.input_data)

# data = []
# with open("2020/input/07_test.txt") as file:
#     data = [line[:-1] for line in file]

# data = puzzle.input_data.split('\n\n')
data = puzzle.input_data.split('\n')

def run(i_to_change=None):
    data2 = data
    
    if i_to_change:
        data2 = data.copy()
        if data2[i][:3] == 'nop':
            data2[i] = 'jmp'+data2[i][3:]
        elif data2[i][:3] == 'jmp':
            data2[i] = 'nop'+data2[i][3:]
        else:
            continue

    executed = set()
    acc = 0
    ptr = 0
    p1 = 0
    while ptr < len(data):
        if ptr in executed:
            p1 = acc
            break
        executed.add(ptr)
        instruction = data[ptr]
        instruction, val = instruction.split(' ')
        if instruction == 'acc':
            acc += int(val)
            ptr += 1
        elif instruction == 'jmp':
            ptr += int(val)
        else:
            ptr += 1

print('Part 1:', p1)
puzzle.answer_a = p1

for i in range(len(data)):
    fixed_data = copy.deepcopy(data)
    if fixed_data[i][:3] == 'nop':
        fixed_data[i] = 'jmp'+fixed_data[i][3:]
    elif fixed_data[i][:3] == 'jmp':
        fixed_data[i] = 'nop'+fixed_data[i][3:]
    else:
        continue
       
    executed = set()
    acc = 0
    ptr = 0
    broke = False
    while ptr < len(fixed_data):
        if ptr in executed:
            broke = True
            print(i)
            break
        executed.add(ptr)
        instruction = fixed_data[ptr]
        instruction, val = instruction.split(' ')
        if instruction == 'acc':
            acc += int(val)
            ptr += 1
        elif instruction == 'jmp':
            ptr += int(val)
        elif instruction == 'nop':
            ptr += 1
        else:
            pass
    if not broke:
        p2 = acc
        break

print('Part 2:', p2)
puzzle.answer_b = p2