from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import *

load_dotenv()
puzzle = Puzzle(year=2020, day=14)

mem = {}
mask = ''
for line in puzzle.input_data.split('\n'):
    a, b = line.split(' = ')
    if a == 'mask':
        mask = b
    else:
        addr = a[a.find('[')+1:a.find(']')]
        val = format(int(b), 'b')
        val = '0' * (36 - len(val)) + val
        val = list(val)

        for i, c in enumerate(mask):
            if c != 'X':
                val[i] = c

        mem[addr] = int(''.join(val), 2)

pt1 = sum(mem.values())
print('Part 1:', pt1)
# puzzle.answer_a = pt1

def assign_floating(addr, val, mem):
    i = addr.find('X')
    if i == -1:
        mem[int(addr, 2)] = val
    else:
        for b in ['0', '1']:
            assign_floating(addr[:i]+b+addr[i+1:], val, mem)

mem = {}
mask = ''
for line in puzzle.input_data.split('\n'):
    a, b = line.split(' = ')
    if a == 'mask':
        mask = b
    else:
        addr = a[a.find('[')+1:a.find(']')]
        addr = "{0:b}".format(int(addr))
        addr = '0' * (36 - len(addr)) + addr
        addr = list(addr)
        val = b

        for i, c in enumerate(mask):
            if c == '1' or c == 'X':
                addr[i] = c
        
        assign_floating(''.join(addr), int(val), mem)

pt2 = sum(mem.values())
print('Part 2:', pt2)
# puzzle.answer_b = pt2