from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2020, day=12)

dirs = ['N', 'E', 'S', 'W']

x, y = 0, 0
face = 'E'
for d in puzzle.input_data.split('\n'):
    action = d[0]
    mag = int(d[1:])
    
    if action == 'F':
        action = face

    if action == 'N':
        y += mag
    elif action == 'S':
        y -= mag
    elif action == 'E':
        x += mag
    elif action == 'W':
        x -= mag
    elif action == 'R':
        mag //= 90
        face = dirs[(dirs.index(face) + mag) % len(dirs)]
    elif action == 'L':
        mag //= 90
        face = dirs[(dirs.index(face) - mag) % len(dirs)]

p1 = abs(x) + abs(y)
print('Part 1:', p1)
# puzzle.answer_a = p1

def rotate_waypoint(x, y, direction, mag):
    turns = (mag // 90) % 4
    if direction == 'L':
        turns = 4 - turns

    if turns == 1:
        return y, -x
    elif turns == 2:
        return -x, -y
    elif turns == 3:
        return -y, x

wx, wy = 10, 1 # waypoint
x, y = 0, 0 # ship
for d in puzzle.input_data.split('\n'):
    action = d[0]
    mag = int(d[1:])
    if action == 'F':
        x += wx * mag
        y += wy * mag
    elif action == 'N':
        wy += mag
    elif action == 'S':
        wy -= mag
    elif action == 'E':
        wx += mag
    elif action == 'W':
        wx -= mag
    elif action == 'R' or action == 'L':
        wx, wy = rotate_waypoint(wx, wy, action, mag)

p2 = abs(x) + abs(y)
print('Part 2:', p2)
# puzzle.answer_b = p2