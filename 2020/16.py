from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import *

load_dotenv()
puzzle = Puzzle(year=2020, day=16)

# seats = []
# with open("2020/input/11_test.txt") as file:
#     seats = [line[:-1] for line in file]
rules, your_ticket, nearby_tickets = [n for n in puzzle.input_data.split('\n\n')]

# rules, your_ticket, nearby_tickets = [n for n in open("2020/input/12_test.txt").read().split('\n\n')]
rules = rules.split('\n')
nearby_tickets = set(nearby_tickets.split('\n')[1:])
rules2 = {}
for rule in rules:
    name, ranges = rule.split(': ')
    ranges = ranges.split(' or ')
    ranges2 = []
    for r in ranges:
        a, b = r.split('-')
        ranges2.append((int(a), int(b)))
    rules2[name] = ranges2

def ticket_is_valid(ruleset, ticket):
    for v in ticket.split(','):
        v = int(v)
        match = False
        for rule in rules2.values():
            if (v >= rule[0][0] and v <= rule[0][1]) or (v >= rule[1][0] and v <= rule[1][1]):
                match = True
                break
        if not match:
            return False
    return True
    
invalid = 0
good_tickets = set()
for ticket in nearby_tickets:
    if ticket_is_valid(rules2, ticket):
        good_tickets.add(ticket)

pt1 = invalid
print('Part 1:', pt1)
# puzzle.answer_a = pt1

# pt2 = None
# print('Part 2:', pt2)
# puzzle.answer_b = pt2