data = []
with open("2020/input/10.txt") as file:
    data = sorted([int(line[:-1]) for line in file])

# ones = 0
# threes = 1
# last = 0
# for n in data:
#     if n - last == 1:
#         ones += 1 
#     elif n - last == 3:
#         threes += 1
#     last = n

# p1 = ones * threes
# print('Part 1:', p1)

data_set = set(data)
dp = {}

def get_ways(n):
    ways = 0
    if n in dp:
        return dp[n]
    for i in range(1, 4):
        n2 = n - i
        if n2 == 0:
            ways += 1
            break
        if n2 > 0 and n2 in data_set:
            ways += get_ways(n2)
    dp[n] = ways
    return ways

p2 = get_ways(max(data) + 3)
print(p2)

# prevs = [(-1, 1), (-2, 0), (-3, 0)]
# for i, n in enumerate(data):
#     curr = 0
#     for prev in prevs:
#         if i - prev[0] <= 3:
#             curr += prev[1]
#     prevs[2] = prevs[1]
#     prevs[1] = prevs[0]
#     prevs[0] = (i, curr)

# print(curr)

prevs = [(0, 1), (-4, 0), (-4, 0)] # (voltage, ways)
for n in data:
    ways = 0
    for prev in prevs:
        if n - prev[0] <= 3:
            ways += prev[1]
    prevs[2] = prevs[1]
    prevs[1] = prevs[0]
    prevs[0] = (n, ways)

print(ways)

from collections import defaultdict
ways = defaultdict(int)
ways[0] = 1
for n in sorted(data):
    ways[n] = ways[n-1] + ways[n-2] + ways[n-3]
print(ways[max(data)])

# print('Part 2:', p2)