from aocd.models import Puzzle
from dotenv import load_dotenv
import math

load_dotenv()
puzzle = Puzzle(year=2020, day=18)
data = puzzle.input_data.split('\n')

def solve_pt1(eq):
    # eval parens
    while '(' in eq:
        last_open = eq.rfind('(')
        close = eq[last_open:].find(')') + last_open
        eq = eq[:last_open] + str(solve_pt1(eq[last_open+1:close])) + eq[close+1:]
    
    # eval remaining
    eqs = eq.split(' ')
    while len(eqs) > 1:
        a, op, b = eqs[:3]
        answer = None
        if op == '*':
            answer = int(a) * int(b)
        elif op == '+':
            answer = int(a) + int(b)
        eqs = [str(answer)] + eqs[3:]
    return int(eqs[0])
            
pt1 = sum(solve_pt1(eq) for eq in data)
print('Part 1:', pt1)
puzzle.answer_a = pt1

def solve_pt2(eq):
    # eval parens
    while '(' in eq:
        last_open = eq.rfind('(')
        close = eq[last_open:].find(')') + last_open
        eq = eq[:last_open] + str(solve_pt2(eq[last_open+1:close])) + eq[close+1:]
    
    # eval +
    eqs = eq.split(' ')
    while '+' in eqs:
        plus = eqs.index('+')
        answer = str(int(eqs[plus-1]) + int(eqs[plus+1]))
        eqs = eqs[:plus-1] + [answer] + eqs[plus+2:]

    # eval *
    return math.prod(int(n) for i, n in enumerate(eqs) if i % 2 == 0)

pt2 = sum(solve_pt2(eq) for eq in data)
print('Part 2:', pt2)
# puzzle.answer_a = pt2