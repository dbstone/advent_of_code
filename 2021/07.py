from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=7)
crabs = [int(i) for i in puzzle.input_data.split(',')]
# crabs = [16,1,2,0,4,2,7,1,2,14]
part1 = float('inf')
for i in range(max(crabs)):
    cost = sum(abs(c-i) for c in crabs)
    if cost < part1:
        part1 = cost

print('Part 1:', part1)
puzzle.answer_a = part1

part2 = float('inf')
for i in range(max(crabs)):
    cost = 0
    for c in crabs:
        dist = abs(c-i)
        cost += dist * (dist + 1) // 2
    if cost < part2:
        part2 = cost

print('Part 2:', part2)
puzzle.answer_b = part2