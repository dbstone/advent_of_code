from aocd.models import Puzzle
from dotenv import load_dotenv
import binascii
from math import prod

load_dotenv()
puzzle = Puzzle(year=2021, day=16)

hex_to_bin = {'0': '0000',
    '1': '0001',
    '2': '0010',
    '3': '0011',
    '4': '0100',
    '5': '0101',
    '6': '0110',
    '7': '0111',
    '8': '1000',
    '9': '1001',
    'A': '1010',
    'B': '1011',
    'C': '1100',
    'D': '1101',
    'E': '1110',
    'F': '1111'}

bits = ''
for h in puzzle.input_data:
    bits += hex_to_bin[h]

def parse_literal(packet):
    i = 0
    version = int(packet[i:i+3], 2)
    i += 6

    val = ''
    while packet[i] != '0':
        val += packet[i+1:i+5]
        i += 5
    val += packet[i+1:i+5]
    i += 5

    val = int(val, 2)

    return version, i, val


def parse_operator(packet):
    i = 0
    version = int(packet[i:i+3], 2)
    version_sum = version
    i += 3
    type_id = int(packet[i:i+3], 2)
    i += 3
    length_type_id = packet[i]
    i += 1

    sub_vals = []

    # TODO: remove redundancy across if and else blocks
    if length_type_id == '0':
        length = int(packet[i:i+15], 2)
        i += 15
        while i-22 < length and i < len(packet):
            test = packet[i+3:i+6]
            version, sub_length = 0, 0
            sub_type_id = int(packet[i+3:i+6], 2)
            if sub_type_id == 4:
                # literal
                version, sub_length, sub_val = parse_literal(packet[i:])
            else:
                # operator
                version, sub_length, sub_val = parse_operator(packet[i:])
            version_sum += version
            sub_vals.append(sub_val)
            i += sub_length
    else:
        num_subpackets = int(packet[i:i+11], 2)
        i += 11
        subpackets_processed = 0
        for _ in range(num_subpackets):
            test = packet[i+3:i+6]
            version, sub_length = 0, 0
            sub_type_id = int(packet[i+3:i+6], 2)
            if sub_type_id == 4:
                # literal
                version, sub_length, sub_val = parse_literal(packet[i:])
            else:
                # operator
                version, sub_length, sub_val = parse_operator(packet[i:])
            version_sum += version
            sub_vals.append(sub_val)
            i += sub_length

    val = None
    if type_id == 0:
        val = sum(sub_vals)
    elif type_id == 1:
        val = prod(sub_vals)
    elif type_id == 2:
        val = min(sub_vals)
    elif type_id == 3:
        val = max(sub_vals)
    elif type_id == 5:
        val = 1 if sub_vals[0] > sub_vals[1] else 0
    elif type_id == 6:
        val = 1 if sub_vals[0] < sub_vals[1] else 0
    elif type_id == 7:
        val = 1 if sub_vals[0] == sub_vals[1] else 0

    return version_sum, i, val

part1, _, part2 = parse_operator(bits)

print('Part 1:', part1)
puzzle.answer_a = part1

print('Part 2:', part2)
puzzle.answer_b = part2