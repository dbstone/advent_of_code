from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=22)

onoffs = []
cuboids = []

data = puzzle.input_data
data = open('2021/test.txt').read()

for line in data.splitlines():
    onoff, coords = line.split(' ')
    onoffs.append(True if onoff == 'on' else False)
    
    coords = coords.split(',')
    coords2 = []
    for coord in coords:
        coord = coord.split('=')[1]
        l, h = (int(x)+50 for x in coord.split('..'))
        coords2.append((l, h))
    cuboids.append(tuple(coords2))

# [print(line) for line in cuboids]

def add_ranges(a, b):
    if not a:
        return [b]
    
    out = a.copy()

    added = False
    for r in a:
        l, h = r
        if b[1] >= l and b[0] <= l:
            l = b[0]
            added = True
        if b[0] <= h and b[1] >= h:
            h = b[1]
            added = True
    if not added:
        out.append(b)


    return min(a[0], b[0]), max(a[1], b[1])

# a - b
def sub_ranges(a, b):
    if not a:
        return None
    l, h, = a
    if b[1] >= a[1]:
        h = min(a[1], b[0]-1)
    if b[0] <= a[0]:
        l = max(a[0], b[1]+1)
    return l, h

cubes = [[[] for _ in range(101)] for _ in range(101)]

for i in range(len(cuboids)):
    cuboid = cuboids[i]
    num_on = 0
    for z in range(101):
        for y in range(101):
            if cubes[z][y]:
                num_on += cubes[z][y][1] - cubes[z][y][0] + 1
    print(num_on)
    for z in range(cuboid[2][0], cuboid[2][1]+1):
        if z in range(101):
            for y in range(cuboid[1][0], cuboid[1][1]+1):
                if y in range(101):
                    if onoffs[i]:
                        cubes[z][y] = add_ranges(cubes[z][y], cuboid[0])
                    else:
                        cubes[z][y] = sub_ranges(cubes[z][y], cuboid[0])

num_on = 0
for z in range(101):
    for y in range(101):
        if cubes[z][y]:
            num_on += cubes[z][y][1] - cubes[z][y][0] + 1

part1 = num_on
print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
