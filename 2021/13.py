from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=13)

dot_coord_strs, folds = (i.split('\n') for i in puzzle.input_data.split('\n\n'))

w, h = 0, 0
dot_coords = []
for line in dot_coord_strs:
    x, y = (int(i) for i in line.split(','))
    dot_coords.append((x, y))
    w = max(w, x)
    h = max(h, y)

w += 1
h += 1

paper = [['.' for _ in range(w)] for _ in range(h)]

for x, y in dot_coords:
    paper[y][x] = '#'

def fold(paper, instruction):
    line = instruction.split(' ')[-1]
    axis, value = line.split('=')
    value = int(value)
    
    folded = []
    if axis == 'x':
        for row in paper:
            left = row[:value]
            right = row[value+1:]
            right.reverse()
            diff = len(left) - len(right)
            if diff >= 0:
                folded_row = left[0:diff]
                for i in range(diff, len(left)):
                    if left[i] == '#' or right[i-diff] == '#':
                        folded_row.append('#')
                    else:
                        folded_row.append('.')
            else:
                folded_row = right[0:-diff]
                for i in range(-diff, len(right)):
                    if left[i+diff] == '#' or right[i] == '#':
                        folded_row.append('#')
                    else:
                        folded_row.append('.')

            folded.append(folded_row)
    else:
        top = paper[:value]
        bottom = paper[value+1:]
        bottom.reverse()
        diff = len(top) - len(bottom)
        if diff >= 0:
            folded = top[0:diff]
            for i in range(diff, len(top)):
                folded_row = []
                top_row = top[i]
                bottom_row = bottom[i-diff]
                for j in range(len(top_row)):
                    if top_row[j] == '#' or bottom_row[j] == '#':
                        folded_row.append('#')
                    else:
                        folded_row.append('.')
                folded.append(folded_row)
        else:
            folded = bottom[0:-diff]
            for i in range(-diff, len(bottom)):
                folded_row = []
                top_row = top[i+diff]
                bottom_row = bottom[i]
                for j in range(len(top_row)):
                    if top_row[j] == '#' or bottom_row[j] == '#':
                        folded_row.append('#')
                    else:
                        folded_row.append('.')
                folded.append(folded_row)
    
    return folded

part1 = sum(row.count('#') for row in fold(paper, folds[0]))
print('Part 1:', part1)
puzzle.answer_a = part1

# part 2
for f in folds:
    paper = fold(paper, f)

[print(line) for line in paper] # TODO: OCR (lol)
