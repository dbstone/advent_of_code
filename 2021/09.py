from aocd.models import Puzzle
from dotenv import load_dotenv
from math import prod

load_dotenv()
puzzle = Puzzle(year=2021, day=9)

# bounds check
def is_valid_point(x, y, grid):
    if x >= 0 and x < len(grid[0]):
        if y >= 0 and y < len(grid):
            return True
    return False

grid = [[int(c) for c in line] for line in puzzle.input_data.split('\n')]
low_points = []
neighbors = [(-1, 0), (0, -1), (1, 0), (0, 1)]

for y in range(len(grid)):
    for x in range(len(grid[0])):
        is_low_point = True
        for nx, ny in neighbors:
            px, py = x+nx, y+ny
            if is_valid_point(px, py, grid):
                if grid[y][x] >= grid[py][px]:
                    is_low_point = False
                    break
        if is_low_point:
            low_points.append((x, y))

part1 = sum(grid[y][x]+1 for x, y in low_points)

print('Part 1:', part1)
puzzle.answer_a = part1

def fill_basin(x, y, grid, visited):
    visited.add((x, y))
    filled = 0
    for nx, ny in neighbors:
        px, py = x+nx, y+ny
        if ((px, py) not in visited and 
            is_valid_point(px, py, grid) and
            grid[py][px] >= grid[y][x] and
            grid[py][px] < 9):
            filled += fill_basin(px, py, grid, visited)
    return 1 + filled

# flood fill from each low point to the edge of its basin,
# i.e. a point lower than the previous, an edge, or a 9
basin_sizes = []
for x, y in low_points:
    visited = set()
    basin_sizes.append(fill_basin(x, y, grid, visited))

part2 = prod(sorted(basin_sizes)[-3:])
print('Part 2:', part2)
puzzle.answer_b = part2