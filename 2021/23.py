from aocd.models import Puzzle
from dotenv import load_dotenv
from copy import deepcopy

load_dotenv()
puzzle = Puzzle(year=2021, day=23)

print(puzzle.input_data)
# part1 = None
# print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2

#############
#...........#
###D#B#A#C###
  #D#C#B#A#
  #D#B#A#C#
  #B#D#A#C#
  #########

#############
#01.2.3.4.56#
###A#B#C#D###
  # # # # #
  # # # # #
  # # # # #
  #########

# adjacency matrix listing distance between each pair of points 
dist = {'A':[3,2,2,4,6,8,9],
    'B':[5,4,2,2,4,6,7],
    'C':[7,6,4,2,2,4,5],
    'D':[9,8,6,4,2,2,3]}

costs = {'A':1,
    'B':10,
    'C':100,
    'D':1000}

# adjacency matrix listing locations that must be unobstructed for travel between each pair of points
adj = {'A':{'0':{'1'}, '1':{}, '2':{}, '3':{'2'}, '4':{'2', '3'}, '5':{'2','3','4'}, '6':{'2','3','4','5'}, 'B':{'2'}, 'C':{'2','3'}, 'D':{'2','3','4'}},
    'B':{'0':{'1','2'}, '1':{'2'}, '2':{}, '3':{}, '4':{'3'}, '5':{'3','4'}, '6':{'3','4','5'}, 'A':{'2'}, 'C':{'3'}, 'D':{'3','4'}},
    'C':{'0':{'1','2','3'}, '1':{'2','3'}, '2':{'3'}, '3':{}, '4':{}, '5':{'4'}, '6':{'4','5'}, 'A':{'2','3'}, 'B':{'3'}, 'D':{'4'}},
    'D':{'0':{'1','2','3','4'}, '1':{'2','3','4'}, '2':{'3','4'}, '3':{'4'}, '4':{}, '5':{}, '6':{'5'}, 'A':{'2','3','4'}, 'B':{'3','4'}, 'C':{'4'}}}

rooms = ['A','B','C','D']
temps = ['0','1','2','3','4','5','6']

rooms_state = {'A':['B','D','D','D'],'B':['D','B','C','B'],'C':['A','A','B','A'],'D':['C','C','A','C']}
temps_state = {'0':'', '1':'','2':'','3':'','4':'','5':'','6':''}

good = {'A':False, 'B':False, 'C':False, 'D':False}

def insert_if_possible(rooms_state, temps_state, good):
    inserted = True
    cost = 0
    while inserted:
        inserted = False
        for temp in temps:
            if temps_state[temp]:
                for room in rooms:
                    if good[room] and not any(temps_state[x] for x in adj[room][temp]):
                        cost += (dist[room][temp]+4-len(rooms_state[room]))*costs[temps_state[temp]]
                        rooms_state[room].append(temps_state[temp])
                        temps_state[temp] = ''
                        inserted = True
    return cost

def get_cost(rooms_state, temps_state, good):
    cost = insert_if_possible(rooms_state, temps_state, good)

    if all(good.values()) and all(len(x) == 4 for x in rooms_state.values()):
        # win!
        return cost

    if temps_state['1'] and temps_state['2'] and temps_state['3'] and temps_state['4'] and temps_state['5']:
        # lose :(
        return -1

    for room in rooms:
        if not good[room] and rooms_state[room]:
            for temp in temps:
                if not temps_state[temp]:
                    if not any(temps_state[x] for x in adj[room][temp]):
                        c = (dist[room][int(temp)]+4-len(rooms_state[room]))*costs[rooms_state[room][-1]]
                        new_temps_state = deepcopy(temps_state)
                        new_rooms_state = deepcopy(rooms_state)
                        new_temps_state[temp] = new_rooms_state[room].pop()
                        c2 = get_cost(new_rooms_state, new_temps_state, good)
                        if c2 != -1:
                            return c + c2 + cost
    
    return -1

part2 = get_cost(rooms_state, temps_state, good)
print('Part 2:', part2)
# puzzle.answer_b = part2
