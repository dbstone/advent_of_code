from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=8)

entries = []
for line in puzzle.input_data.split('\n'):
    patterns, output = line.split(' | ')
    patterns = [p for p in patterns.split()]
    output = [d for d in output.split()]
    entries.append((patterns, output))

unique_lengths = {2:1, 4:4, 3:7, 7:8} # digits 1, 4, 7, 8, respectfully

part1 = 0
for e in entries:
    output = e[1]
    for digit in output:
        if len(digit) in unique_lengths:
            part1 += 1

print('Part 1:', part1)
puzzle.answer_a = part1

part2 = 0
for e in entries:
    signals = set()
    for s in e[0]+e[1]:
        signals.add(frozenset(s))

    digit_to_signal_map = {} # dict mapping numerical digit (in str form) to set of characters

    # find digits with unique number of segments (1, 4, 7, 8)
    for s in signals:
        if len(digit_to_signal_map) >= 4: break
        if len(s) in unique_lengths:
            digit = unique_lengths[len(s)]
            if digit not in digit_to_signal_map:
                digit_to_signal_map[digit] = frozenset(s)
    
    candidates235 = set(filter(lambda x: len(x) == 5, signals))
    candidates690 = set(filter(lambda x: len(x) == 6, signals))

    # compare signals pairwise
    done = False
    for ca in candidates235:
        if done: break
        for cb in candidates235:
            if done: break
            if ca is not cb:
                if len(ca.intersection(cb)) == 3:
                    # these are the 2 and 5
                    for c in candidates235:
                        if c is not ca and c is not cb:
                            digit_to_signal_map[3] = c
                            done = True
                            break
    
    for c in candidates690:
        if len(c.intersection(digit_to_signal_map[1])) == 1:
            digit_to_signal_map[6] = c
            break

    # 4 shares 3 with 0 and 4 with 9
    for c in candidates690:
        if c is not digit_to_signal_map[6]:
            shared = len(c.intersection(digit_to_signal_map[4]))
            if shared == 3:
                digit_to_signal_map[0] = c
            elif shared == 4:
                digit_to_signal_map[9] = c

    # 4 shares 3 with 5 and 2 with 2
    for c in candidates235:
        if c is not digit_to_signal_map[3]:
            shared = len(c.intersection(digit_to_signal_map[4]))
            if shared == 3:
                digit_to_signal_map[5] = c
            elif shared == 2:
                digit_to_signal_map[2] = c

    signal_to_digit_map = {v: k for k, v in digit_to_signal_map.items()}

    # decode output
    num_str = ''
    for digit in e[1]:
        num_str += str(signal_to_digit_map[frozenset(digit)])
    part2 += int(num_str)

print('Part 2:', part2)
puzzle.answer_b = part2