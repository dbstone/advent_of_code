from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cache
from collections import defaultdict

load_dotenv()
puzzle = Puzzle(year=2021, day=15)

cavern = [[int(c) for c in line] for line in puzzle.input_data.splitlines()]

def get_nearest(dists, closedSet, openSet, is_part2):
    nearest = None
    min_dist = float('inf')

    for u in openSet:
        if u not in closedSet and dists[u] < min_dist:
            nearest = u
            min_dist = dists[u]
    
    return nearest

# modified dijkstra's
def dijkstra(cavern, is_part2):
    dists = defaultdict(lambda: float('inf'))
    dists[(0, 0)] = 0

    closedSet = set()
    openSet = set()
    openSet.add((0, 0))

    neighbors = [(1, 0), (0, 1), (-1, 0), (0, -1)]
    
    # bounds check
    def is_valid_point(x, y):
        multiplier = 5 if is_part2 else 1
        if x >= 0 and x < len(cavern[0])*multiplier:
            if y >= 0 and y < len(cavern)*multiplier:
                return True
        return False

    current = None
    N = len(cavern) * len(cavern[0])
    if is_part2:
        N *= 25

    while len(closedSet) < N:
        current = get_nearest(dists, closedSet, openSet, is_part2)
        closedSet.add(current)
        openSet.remove(current) # TODO: Use a heap for this instead (python heapq library)
        multiplier = 5 if is_part2 else 1
        if current == (len(cavern[0])*multiplier-1, len(cavern)*multiplier-1):
            return dists
        x, y = current
        for dx, dy in neighbors:
            nx, ny = x+dx, y+dy
            n = (nx, ny)
            if is_valid_point(nx, ny) and n not in closedSet:
                nx_offset = nx // len(cavern[0])
                ny_offset = ny // len(cavern)
                nv = cavern[ny%len(cavern)][nx%len(cavern[0])]
                nv = (nv + nx_offset + ny_offset - 1) % 9 + 1
                dist = dists[current] + nv
                if dist < dists[n]:
                    dists[n] = dist
                    openSet.add(n)
        if len(closedSet) % 1000 == 0:
            print(len(closedSet))

    return dists

dists = dijkstra(cavern, False)
part1 = dists[(len(cavern[0])-1, len(cavern)-1)]
print('Part 1:', part1)
puzzle.answer_a = part1

dists = dijkstra(cavern, True)
part2 = dists[(len(cavern[0])*5-1, len(cavern)*5-1)]
print('Part 2:', part2)
puzzle.answer_b = part2