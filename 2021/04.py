from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=4)

first = True
draws = []
boards = []
board_marks = []
board_marks2 = []

for chunk in puzzle.input_data.split('\n\n'):
    if first:
        draws = chunk.split(',')
        first = False
    else:
        boards.append([line.split() for line in chunk.split('\n')])
        board_marks.append([[False] * 5 for _ in range(5)])

score = None

for draw in draws:
    if score: break
    for i in range(len(boards)):
        if score: break
        board = boards[i]
        for row in range(len(board)):
            if score: break
            for col in range(len(board[row])):
                if score: break
                if board[row][col] == draw:
                    marks = board_marks[i]
                    marks[row][col] = True
                    if all(marks[row]) or all(rowrow[col] for rowrow in marks):
                        # winner!
                        score = 0
                        for row2 in range(len(marks)):
                            for col2 in range(len(marks[row2])):
                                if not marks[row2][col2]:
                                    score += int(board[row2][col2])
                        score *= int(draw)

print(score)
puzzle.answer_a = score

board_marks = [[[False for _ in range(5)] for _ in range(5)] for _ in range(len(boards))]
scores = [0 for _ in range(len(boards))]
last = 0

# TODO: redundant!
for draw in draws:
    for i in range(len(boards)):
        if scores[i] == 0:
            board = boards[i]
            for row in range(len(board)):
                for col in range(len(board[row])):
                        if board[row][col] == draw:
                            marks = board_marks[i]
                            marks[row][col] = True
                            if all(marks[row]) or all(rowrow[col] for rowrow in marks):
                                # winner!
                                for row2 in range(len(marks)):
                                    for col2 in range(len(marks[row2])):
                                        if not marks[row2][col2]:
                                            scores[i] += int(board[row2][col2])
                                scores[i] *= int(draw)
                                if all(scores):
                                    last = scores[i]

print(last)
puzzle.answer_b = last




                        
