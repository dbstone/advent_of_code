from aocd.models import Puzzle
from dotenv import load_dotenv
import functools

load_dotenv()
puzzle = Puzzle(year=2021, day=6)
fish = [int(i) for i in puzzle.input_data.split(',')]

@functools.cache
def num_fish(days):
    if days < 1: return 1
    return num_fish(days-7) + num_fish(days-9)

part1 = sum(num_fish(80-f) for f in fish)
print('Part 1:', part1)
puzzle.answer_a = part1

part2 = sum(num_fish(256-f) for f in fish)
print('Part 2:', part2)
puzzle.answer_b = part2