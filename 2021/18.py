from aocd.models import Puzzle
from dotenv import load_dotenv
from math import floor, ceil
import re
from itertools import permutations

load_dotenv()
puzzle = Puzzle(year=2021, day=18)

def explode(num):
    offset = 0
    for pair in re.findall('\[\d+,\d+\]', num):
        match = re.search(re.escape(pair), num[offset:])
   
        left_brackets = num[:match.start() + offset].count('[')
        right_brackets = num[:match.start() + offset].count(']')
        if left_brackets - right_brackets >= 4:
            a, b = match.group()[1:-1].split(',')
            left = num[:match.start() + offset][::-1] # reverse so we can search backward from end
            right = num[match.end() + offset :]
            
            search_left = re.search('\d+', left)
            if search_left:
                n = int(left[search_left.start():search_left.end()][::-1]) + int(a)
                left = f'{left[:search_left.start()]}{str(n)[::-1]}{left[search_left.end():]}'
 
            search_right = re.search('\d+', right)
            if search_right:
                n = int(right[search_right.start():search_right.end()]) + int(b)
                right = f'{right[:search_right.start()]}{n}{right[search_right.end():]}'
        
            num = f'{left[::-1]}0{right}'
            break
        else:
            offset += match.end()
    
    return num 

def split(num):
    dd = re.search('\d\d', num)
    if dd:
        left = num[:dd.start()]
        right = num[dd.end():]
        val = int(dd.group()) / 2
        a = int(floor(val))
        b = int(ceil(val))
        num = f'{left}[{a},{b}]{right}'
    return num
    
def reduce(num):
    exploded = explode(num)
    if exploded != num:
        return reduce(exploded)
    else:
        splitted = split(num)
        if splitted != num:
            return reduce(splitted)
        else:
            return splitted

def magnitude(num):
    while num.count(',') > 1:
        for pair in re.findall('\[\d+,\d+\]', num):
            match = re.search(re.escape(pair), num)
            left, right = pair[1:-1].split(',')
            num = f'{num[:match.start()]}{int(left) * 3 + int(right) * 2}{num[match.end():]}'
    left, right = num[1:-1].split(',')
    return int(left) * 3 + int(right) * 2

def add(num1, num2):
    return f'[{num1},{num2}]'

nums = puzzle.input_data.splitlines()

summ = ''
while nums:
    num = nums.pop(0)
    if summ:
        summ = add(summ, num)
    else:
        summ = add(num, nums.pop(0))
    summ = reduce(summ)
    
part1 = magnitude(summ)
print('Part 1:', part1)
puzzle.answer_a = part1

nums = puzzle.input_data.splitlines()
pairs = list(permutations(nums, 2))
highest = 0
for a, b in pairs:
    highest = max(highest, magnitude(reduce(add(a, b))))

print('Part 2:', highest)
puzzle.answer_b = highest