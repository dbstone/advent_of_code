from typing import Counter
from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=5)
puzzle_data = puzzle.input_data.split('\n')

def solve(data, is_part2):
    points = Counter()
    for line in data:
        a, b = line.split(' -> ')
        x1, y1 = [int(i) for i in a.split(',')]
        x2, y2 = [int(i) for i in b.split(',')]
        if x1 == x2:
            # vertical
            if y1 > y2:
                y1, y2 = y2, y1
            for y in range(y1, y2+1):
                points[(x1,y)] += 1
        elif y1 == y2:
            # horizontal
            if x1 > x2:
                x1, x2 = x2, x1
            for x in range(x1, x2+1):
                points[(x,y1)] += 1
        elif is_part2 and abs(y2-y1) == abs(x2-x1):
            # diagonal (part 2 only)
            dx = 1 if x2 > x1 else -1
            dy = 1 if y2 > y1 else -1
            for i in range(abs(x2-x1)+1):
                points[(x1+dx*i,y1+dy*i)] += 1
    
    return len([i for i in points.items() if i[1] >= 2])

part1 = solve(puzzle_data, False)
print(part1)
puzzle.answer_a = part1

part2 = solve(puzzle_data, True)
print(part2)
puzzle.answer_b = part2