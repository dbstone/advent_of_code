import re
from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=1)

# part1 = None
# print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2

# returns 'z' value after digit processed
def process_digit(digit, z, reduce, x_add, y_add):
    x = z % 26
    
    if reduce:
        z //= 26
    
    x += x_add
    if x != digit:
        z *= 26
        z += (digit + y_add)
    
    return z

def run_debug(model_number):
    z = 0
    z = process_digit(int(model_number[0]), z, False, 12, 6)
    print(z)
    z = process_digit(int(model_number[1]), z, False, 10, 2)
    print(z)
    z = process_digit(int(model_number[2]), z, False, 10, 13)
    print(z)
    z = process_digit(int(model_number[3]), z, True, -6, 8)
    print(z)
    z = process_digit(int(model_number[4]), z, False, 11, 13)
    print(z)
    z = process_digit(int(model_number[5]), z, True, -12, 8)
    print(z)
    z = process_digit(int(model_number[6]), z, False, 11, 3)
    print(z)
    z = process_digit(int(model_number[7]), z, False, 12, 11)
    print(z)
    z = process_digit(int(model_number[8]), z, False, 12, 10)
    print(z)
    z = process_digit(int(model_number[9]), z, True, -2, 8)
    print(z)
    z = process_digit(int(model_number[10]), z, True, -5, 14)
    print(z)
    z = process_digit(int(model_number[11]), z, True, -4, 6)
    print(z)
    z = process_digit(int(model_number[12]), z, True, -4, 8)
    print(z)
    z = process_digit(int(model_number[13]), z, True, -12, 2)
    print(z)
    return z

def run(model_number):
    z = 0
    z = process_digit(int(model_number[0]), z, False, 12, 6) #1
    z = process_digit(int(model_number[1]), z, False, 10, 2) #2

    z = process_digit(int(model_number[2]), z, False, 10, 13) #3
    z = process_digit(int(model_number[3]), z, True, -6, 8) #3

    z = process_digit(int(model_number[4]), z, False, 11, 13) #3
    z = process_digit(int(model_number[5]), z, True, -12, 8) #3

    z = process_digit(int(model_number[6]), z, False, 11, 3) #3
    z = process_digit(int(model_number[7]), z, False, 12, 11) #4
    z = process_digit(int(model_number[8]), z, False, 12, 10) #5
    z = process_digit(int(model_number[9]), z, True, -2, 8) #5
    z = process_digit(int(model_number[10]), z, True, -5, 14) #4
    z = process_digit(int(model_number[11]), z, True, -4, 6) #3

    z = process_digit(int(model_number[12]), z, True, -4, 8) #2
    z = process_digit(int(model_number[13]), z, True, -12, 2) #1
    return z

# for n in range(11111111111111, 100000000000000):
#     z = run(str(n))
#     if z == 0:
#         print(n)
#         break

# run_debug('11181211197011')
print()
# run_debug('99298993199873')
run_debug('73181221197111')