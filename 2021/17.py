from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cache

load_dotenv()
puzzle = Puzzle(year=2021, day=17)

target = puzzle.input_data[13:]
x_target, y_target = target.split(', ')
x_min, x_max = (int(c) for c in x_target[2:].split('..'))
y_min, y_max = (int(c) for c in y_target[2:].split('..'))

max_init_dy = -y_min - 1
acme = max_init_dy * (max_init_dy + 1) // 2

print('Part 1:', acme)
puzzle.answer_a = acme

@cache
def get_matching_xs(x_min, x_max, t):
    max_init_dx = None
    min_init_dx = None
    init_dx = 1
    hit = False
    while True:
        x = 0
        dx = init_dx
        for _ in range(t):
            x += dx
            dx = max(0, dx-1)
        
        if hit:
            if x > x_max:
                break
            max_init_dx = init_dx
        else:
            if x >= x_min and x <= x_max:
                hit = True
                min_init_dx = init_dx
                max_init_dx = init_dx
        init_dx += 1
    
    if not min_init_dx:
        return 0

    ts = set()
    for t in range(min_init_dx, max_init_dx+1):
        ts.add(t)
    return ts

min_init_dy = y_min
ds = set()
for init_dy in range(min_init_dy, max_init_dy+1):
    y = 0
    dy = init_dy
    t = 0
    hit = False
    while True:
        t += 1
        y += dy
        dy -= 1
        if hit:
            if y < y_min:
                # overshot, so stop
                break
            else:
                dxs = get_matching_xs(x_min, x_max, t)
                for dx in dxs:
                    ds.add((dx, init_dy))
        else:
            if y >= y_min and y <= y_max:
                hit = True
                dxs = get_matching_xs(x_min, x_max, t)
                for dx in dxs:
                    ds.add((dx, init_dy))
            elif y < y_min:
                # no hit for this init_dy
                break

part2 = len(ds)
print('Part 2:', part2)
puzzle.answer_b = part2