from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=2)

data = puzzle.input_data.split('\n')
x = 0
y = 0
for line in data:
    direction, magnitude = line.split(' ')
    if direction == 'forward':
        x += int(magnitude)
    elif direction == 'up':
        y -= int(magnitude)
    elif direction == 'down':
        y += int(magnitude)

print('Part 1:', x * y)
# puzzle.answer_a = x * y

aim = 0
x = 0
y = 0
for line in data:
    direction, magnitude = line.split(' ')
    if direction == 'forward':
        x += int(magnitude)
        y += int(magnitude) * aim
    elif direction == 'up':
        aim -= int(magnitude)
    elif direction == 'down':
        aim += int(magnitude)

print('Part 2:', x * y)
puzzle.answer_b = x * y


