from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=1)

# part1 = None
# print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
