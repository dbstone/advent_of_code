from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=3)

data = puzzle.input_data.split('\n')

bits = len(data[0])
ones = [0] * bits
zeros = [0] * bits
for line in data:
    for i in range(bits):
        if line[i] == '0':
            zeros[i] += 1
        else:
            ones[i] += 1

gamma = ''
epsilon = ''
for i in range(bits):
    if ones[i] > zeros[i]:
        gamma += '1'
        epsilon += '0'
    else:
        gamma += '0'
        epsilon += '1'

p1 = int(gamma, 2) * int(epsilon, 2)
print('Part 1:', p1)
# puzzle.answer_a = p1

o2_data = set(data)

for i in range(bits):
    if len(o2_data) == 1:
        break
    zeros = 0
    ones = 0
    for line in o2_data:
        if line[i] == '0':
            zeros += 1
        else:
            ones += 1
    
    for b in list(o2_data):
        if zeros > ones:
            if b[i] == '1':
                o2_data.remove(b)
        elif b[i] == '0':
            o2_data.remove(b)

co2_data = set(data)
for i in range(bits):
    if len(co2_data) == 1:
        break
    zeros = 0
    ones = 0
    for line in co2_data:
        if line[i] == '0':
            zeros += 1
        else:
            ones += 1
    for b in list(co2_data):
        if zeros > ones:
            if b[i] == '0':
                co2_data.remove(b)
        elif b[i] == '1':
            co2_data.remove(b)

p2 = int(list(o2_data)[0], 2) * int(list(co2_data)[0], 2)
print('Part 2:', p2)
puzzle.answer_b = p2


