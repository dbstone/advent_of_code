from aocd.models import Puzzle
from dotenv import load_dotenv
from itertools import product
from functools import cache

load_dotenv()
puzzle = Puzzle(year=2021, day=21)

pos = [int(line[-1])-1 for line in puzzle.input_data.splitlines()]
pos2 = tuple(pos) # save orig for part 2

score = [0, 0]

last_roll = 0
winner = -1
while winner < 0:
    for player in range(2):
        rollsum = last_roll * 3 + 6
        last_roll += 3
        pos[player] = (pos[player] + rollsum) % 10
        score[player] += pos[player] + 1
        if score[player] >= 1000:
            winner = player
            break

loser_score = score[0] if winner == 1 else score[1]
part1 = loser_score * last_roll
print('Part 1:', part1)
puzzle.answer_a = part1

rolls = [sum(roll) for roll in product(range(1,4), repeat=3)]

@cache
def who_wins(pos, score, turn):
    wins = [0, 0]
    for roll in rolls:
        new_pos = list(pos)
        new_score = list(score)
        new_pos[turn] = (pos[turn] + roll) % 10
        new_score[turn] = score[turn] + new_pos[turn] + 1
        if new_score[turn] >= 21:
            wins[turn] += 1
        else:
            wins0, wins1 = who_wins(tuple(new_pos), tuple(new_score), 0 if turn == 1 else 1)
            wins[0] += wins0
            wins[1] += wins1

    return tuple(wins)

wins = who_wins(pos2, (0, 0), 0)
part2 = max(wins)
print('Part 2:', part2)
puzzle.answer_b = part2
