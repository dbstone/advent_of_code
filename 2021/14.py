from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import Counter

from numpy import poly

load_dotenv()
puzzle = Puzzle(year=2021, day=14)

polymer, rules = puzzle.input_data.split('\n\n')
rules = rules.splitlines()

pair_counts = Counter()
for i in range(len(polymer)-1):
    pair_counts[polymer[i:i+2]] += 1

for _ in range(10):
    new_pair_counts = Counter()
    for rule in rules:
        pair, element = rule.split(' -> ')
        new_pair_counts[pair[0]+element] += pair_counts[pair]
        new_pair_counts[element+pair[1]] += pair_counts[pair]
    pair_counts = new_pair_counts

element_counts = Counter()
for pair, count in pair_counts.items():
    for c in pair:
        element_counts[c] += count

element_counts[polymer[0]] -= 1
element_counts[polymer[-1]] -= 1

for element, count in element_counts.items():
    element_counts[element] = count // 2

element_counts[polymer[0]] += 1
element_counts[polymer[-1]] += 1

# for _ in range(10):
#     print(Counter(polymer).most_common()[-1])
#     insertions = []
#     for rule in rules:
#         pair, element = rule.split(' -> ')
#         insertion_locations = [i+1 for i in range(len(polymer)) if polymer.startswith(pair, i)]
#         for l in insertion_locations:
#             insertions.append((l, element))

#     for i, insertion in enumerate(sorted(insertions)):
#         location, element = insertion
#         location += i # offset to account for prior insertions
#         polymer = polymer[:location] + element + polymer[location:]

# ctr = Counter(polymer)
most_common = element_counts.most_common()
part1 = most_common[0][1] - most_common[-1][1]
print('Part 1:', part1)
puzzle.answer_a = part1

pair_counts = Counter()
for i in range(len(polymer)-1):
    pair_counts[polymer[i:i+2]] += 1

# TODO abstract this into a function that takes N steps as parameter
for _ in range(40):
    new_pair_counts = Counter()
    for rule in rules:
        pair, element = rule.split(' -> ')
        new_pair_counts[pair[0]+element] += pair_counts[pair]
        new_pair_counts[element+pair[1]] += pair_counts[pair]
    pair_counts = new_pair_counts

element_counts = Counter()
for pair, count in pair_counts.items():
    for c in pair:
        element_counts[c] += count

element_counts[polymer[0]] -= 1
element_counts[polymer[-1]] -= 1

for element, count in element_counts.items():
    element_counts[element] = count // 2

element_counts[polymer[0]] += 1
element_counts[polymer[-1]] += 1

most_common = element_counts.most_common()
part2 = most_common[0][1] - most_common[-1][1]
print('Part 2:', part2)
puzzle.answer_b = part2