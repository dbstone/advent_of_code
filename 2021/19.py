from aocd.models import Puzzle
from dotenv import load_dotenv
from ast import literal_eval
from itertools import combinations

load_dotenv()
puzzle = Puzzle(year=2021, day=19)

data = puzzle.input_data

scanners = []
for scanner in data.split('\n\n'):
    beacons = []
    for beacon in scanner.splitlines()[1:]:
        beacons.append(literal_eval(beacon))
    scanners.append(beacons)

def add(a, b):
    return (a[0]+b[0],a[1]+b[1],a[2]+b[2])

def sub(a, b):
    return (a[0]-b[0],a[1]-b[1],a[2]-b[2])

# 3d distance formula
def square_dist(a, b):
    return (a[0] - b[0])**2 + (a[1] - b[1])**2 + (a[2] - b[2])**2

def manhattan_distance(a, b):
    return sum([abs(x-y) for x,y in zip(a,b)])

# util functions for rotation generator below
def roll(v): return (v[0],v[2],-v[1])
def turn(v): return (-v[1],v[0],v[2])

# yield all possible rotations of a given 3d coord
def rotation(v):
    for cycle in range(2):
        for step in range(3):  # Yield RTTT 3 times
            v = roll(v)
            yield(v)           #    Yield R
            for i in range(3): #    Yield TTT
                v = turn(v)
                yield(v)
        v = roll(turn(roll(v)))  # Do RTR

# return a specific rotation of a given 3d coord
def rotation_n(v, n):
    return next(x for i,x in enumerate(rotation(v)) if i==n)

dists = []

for scanner in scanners:
    dist_dict = {}
    for a, b in combinations(scanner, 2):
        dist_dict[square_dist(a, b)] = (a, b)
    dists.append(dist_dict)

aligned_scanners = [None] * len(scanners)
aligned_scanners[0] = scanners[0]

aligned_dists = [None] * len(scanners)
aligned_dists[0] = dists[0]

scanner_positions = [None] * len(scanners)
scanner_positions[0] = (0, 0, 0)

unaligned = set(n for n in range(len(scanners)))
unaligned.remove(0)

while unaligned:
    # TODO OPTIMIZATION: Maybe better for outer loop to be unaligned and inner aligned?
    for a in range(len(aligned_scanners)):
        if aligned_scanners[a]:
            scanner_a = aligned_dists[a]
            for b in unaligned.copy(): # use a copy so we can modify original within loop
                scanner_b = dists[b]
                keys_a = set(k for k,v in scanner_a.items())
                keys_b = set(k for k,v in scanner_b.items())
                intersection = set.intersection(keys_a, keys_b)
                if len(intersection) >= 66:
                    # Match!
                    # Now, align scanner b to scanner a.
                    
                    # First, pair up the coordinates of each of the
                    # shared beacons from each scanner's perspective.
                    shared_beacons = set() # form: (scanner_a_coord, scanner_b_coord)
                    for dist in intersection:
                        aa, ab = scanner_a[dist]

                        # We know these beacons match, but we don't know which is which.
                        # Find another overlapping dist in a that contains aa.
                        for dist2 in intersection:
                            if dist2 != dist:
                                if aa in scanner_a[dist2]:
                                    # find corresponding dist2 beacon in 
                                    # scanner_b and pair it with aa
                                    ba = None
                                    for p in scanner_b[dist2]:
                                        if p in scanner_b[dist]:
                                            shared_beacons.add((aa, p))
                                            ba = p
                                    # by process of elimination, the other
                                    # scanner_b[dist] beacon must match with ab
                                    for p in scanner_b[dist]:
                                        if p != ba:
                                            shared_beacons.add((ab, p))

                    # next, find the rotational and translation
                    # transforms between the two matching scanners:
                    
                    shared_beacons = list(shared_beacons) # convert to list to maintain order

                    # get all the possible rotations of scanner_b's beacons
                    scanner_b_rotations = []
                    for _, beacon_b in shared_beacons:
                        scanner_b_rotations.append(list(rotation(beacon_b)))

                    aligned_rotation = None # index of the rotation which aligns the two scanners
                    aligned_translation = None # translation which aligns the two scanners at the given rotation
                    for rot in range(len(scanner_b_rotations[0])):
                        beacon_a0 = shared_beacons[0][0]
                        beacon_b0 = scanner_b_rotations[0][rot]
                        translation = sub(beacon_a0, beacon_b0)
                        match = True
                        for i in range(len(shared_beacons)):
                            if i == 0: continue
                            beacon_a = shared_beacons[i][0]
                            beacon_b = scanner_b_rotations[i][rot]
                            if sub(beacon_a, beacon_b) != translation:
                                match = False
                                break
                        if match:
                            aligned_rotation = rot
                            aligned_translation = translation
                            break
                    
                    scanner_positions[b] = aligned_translation
                    
                    # use derived transform to completely align scanner b
                    # TODO: OPTIMIZATION: skip beacons which we've already transformed above
                    aligned_scanners[b] = []
                    for beacon in scanners[b]:
                        rotated = rotation_n(beacon, aligned_rotation)
                        translated = add(rotated, aligned_translation)
                        aligned_scanners[b].append(translated)
                    
                    # recalculate dists (actually just repairing dists with aligned coords)
                    aligned_dists[b] = {}
                    for beacon_a, beacon_b in combinations(aligned_scanners[b], 2):
                        aligned_dists[b][square_dist(beacon_a, beacon_b)] = (beacon_a, beacon_b)
                    
                    unaligned.remove(b)
                    
unique_beacons = set()
for scanner in aligned_scanners:
    unique_beacons = unique_beacons | set(scanner)

part1 = len(unique_beacons)
print('Part 1:', part1)
puzzle.answer_a = part1

part2 = max(manhattan_distance(a, b) for a, b in combinations(scanner_positions, 2))
print('Part 2:', part2)
puzzle.answer_b = part2
