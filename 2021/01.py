from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=1)

last = None
n_increments = 0
for line in puzzle.input_data.split('\n'):
    n = int(line.strip())
    if last and n > last:
        n_increments += 1
    last = n

print('Part 1:', n_increments)
# puzzle.answer_a = n_increments

n_increments = 0
data = puzzle.input_data.split('\n')
for i in range(3, len(data)):
    outgoing = int(data[i-3].strip())
    incoming = int(data[i].strip())
    if incoming > outgoing:
        n_increments += 1

print('Part 2:', n_increments)
# puzzle.answer_b = n_increments


