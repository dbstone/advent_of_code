from tkinter import DoubleVar
from aocd.models import Puzzle
from dotenv import load_dotenv
from numpy import double

load_dotenv()
puzzle = Puzzle(year=2021, day=12)

cxns = {}
for line in puzzle.input_data.split('\n'):
    a, b = line.split('-')
    for p, c in [(a,b), (b,a)]:
        if p not in cxns:
            cxns[p] = set()
        cxns[p].add(c)
    
def get_paths(cave, visited, path, double_visited, cxns, is_part2):
    if cave == 'end':
        return [path + ['end']]

    if cave.islower():
        visited = visited.copy()
        visited.add(cave)
    
    path = path+[cave]

    paths = []
    for cxn in cxns[cave]:
        if cxn in visited:
            if is_part2 and cxn != 'start' and not double_visited:
                paths += get_paths(cxn, visited, path, True, cxns, is_part2)
        else:
            paths += get_paths(cxn, visited, path, double_visited, cxns, is_part2)

    return paths

part1 = len(get_paths('start', set(), [], False, cxns, False))
print('Part 1:', part1)
puzzle.answer_a = part1

part2 = len(get_paths('start', set(), [], False, cxns, True))
print('Part 2:', part2)
puzzle.answer_b = part2