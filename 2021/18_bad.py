from aocd.models import Puzzle
from dotenv import load_dotenv
from math import floor, ceil

from matplotlib.pyplot import isinteractive

load_dotenv()
puzzle = Puzzle(year=2021, day=18)

# parse the text representation of a snailfish number and return a binary tree representation
def parse_num(num):
    if num.isdigit():
        return int(num)
    else:
        depth = 0
        center = None
        for i, c in enumerate(num):
            if c == '[':
                depth += 1
            elif c == ']':
                depth -= 1
            elif c == ',':
                if depth == 1:
                    center = i
                    break
        
        return (parse_num(num[1:center]), parse_num(num[center+1:-1]))
    
def explode_num(num, depth):
    if isinstance(num, int):
        return False, 0, 0, num
    a, b = num
    if isinstance(a, int) and isinstance(b, int):
        if depth >= 4:
            # explode!
            # TODO
            return 3342
        else:
            return False, 0, 0, num
    
    a_exploded = explode_num(a, depth+1)
    if a_exploded:
        if isinstance(b, int):
            b += a_exploded[2]
        else:
        return True, a_exploded[1], 
    
    if isinstance()
        return None

    return left, right, center

def split_num(num):
    if isinstance(num, int):
        if num > 10:
            return (floor(num/2), ceil(num/2))
    else:
        a, b = num
        a_split = split_num(a)
        if a_split:
            return (a_split, b)
        else:
            b_split = split_num(b)
            if b_split:
                return (a, b_split)
    
    return None
    
def reduce_num(num):
    if isinstance(num, int):
        return num
    
    reduced = num

    while True:
        exploded = explode_num(reduced, 0)
        if exploded:
            reduced = exploded[0]
        else:
            split = split_num(reduced)
            if split:
                reduced = split
            else:
                break

    return reduced


# test data first
nums = []
with open("2021/test.txt") as file:
    data = file.read()
    for line in data.splitlines():
        nums.append(parse_num(line))

print(nums)

# part1 = None
# print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2