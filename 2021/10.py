from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=10)

#FIXME Recursion is terrible here. Just use a stack.
def get_syntax_error(s, closer, openers_to_closers, scores):
    if not s:
        return None, None
    if closer and s[0] == closer:
        return None, s[1:]
    elif s[0] in scores:
        return s[0], s[1:]
    elif s[0] in openers_to_closers:
        error, s = get_syntax_error(s[1:], openers_to_closers[s[0]], openers_to_closers, scores)
        return (error, s) if error else get_syntax_error(s, closer, openers_to_closers, scores)
    return None # incomplete

scores = {')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137}

openers_to_closers = {'(': ')',
    '[': ']',
    '{': '}',
    '<': '>'}

part1 = 0

sanitized = []

for line in puzzle.input_data.split('\n'):
    error, _ = get_syntax_error(line, None, openers_to_closers, scores)
    if not error:
        sanitized.append(line)
    else:
        part1 += scores[error]

print('Part 1:', part1)
puzzle.answer_a = part1

def get_completion(s):
    completion = ''
    for c in s:
        if c in openers_to_closers:
            completion += openers_to_closers[c]
        else:
            completion = completion[:-1]
    return completion[::-1]

completion_values = {')': 1,
    ']': 2,
    '}': 3,
    '>': 4}

scores = []
for line in sanitized:
    completion = get_completion(line)
    score = 0
    for c in completion:
        score *= 5
        score += completion_values[c]
    scores.append(score)

part2 = sorted(scores)[len(scores)//2]
print('Part 2:', part2)
puzzle.answer_b = part2