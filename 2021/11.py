from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=11)

def is_valid_point(x, y, grid):
    if x >= 0 and x < len(grid[0]):
        if y >= 0 and y < len(grid):
            return True
    return False

grid = [[int(c) for c in line] for line in puzzle.input_data.split('\n')]
N = len(grid) * len(grid[0])

part1 = 0
part2 = None

step = 0
while not part2:
    flashed = set()
    to_flash = set()

    # increment energy off all octopi
    for x in range(len(grid[0])):
        for y in range(len(grid)):
            grid[y][x] += 1
            if grid[y][x] > 9:
                to_flash.add((x, y))

    # flash octopi with energy > 9 
    while to_flash:
        x, y = to_flash.pop()
        flashed.add((x, y)) # flash current
        
        # increment neighbors
        for ny in range(y-1, y+2):
            for nx in range(x-1, x+2):
                if (is_valid_point(nx, ny, grid) and 
                    (nx, ny) not in flashed and
                    (nx, ny) not in to_flash):
                    grid[ny][nx] += 1
                    if grid[ny][nx] > 9:
                        to_flash.add((nx, ny))

    if step < 100:
        part1 += len(flashed)

    if len(flashed) == N:
        part2 = step + 1

    # reset energy of all flashed octopi  
    for x, y in flashed:
        grid[y][x] = 0
    
    step += 1


print('Part 1:', part1)
puzzle.answer_a = part1

print('Part 2:', part2)
puzzle.answer_b = part2