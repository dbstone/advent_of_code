from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=20)

algo, img = puzzle.input_data.split('\n\n')
img = img.splitlines()

def is_valid_point(x, y, img):
    if x >= 0 and x < len(img[0]):
        if y >= 0 and y < len(img):
            return True
    return False

def get_new_pixel(img, x, y, inf_char):
    binary_str = ''
    for ny in range(y-1, y+2):
        for nx in range(x-1, x+2):
            if is_valid_point(nx, ny, img):
                binary_str += '1' if img[ny][nx] == '#' else '0'
            else:
                binary_str += '0' if inf_char == '.' else '1'
    return int(binary_str, 2)

def get_new_img_2_pass(algo, img):
    # first pass
    new_img = [[algo[0]] * (len(img[0])+2) for _ in range(len(img)+2)]
    for y in range(len(new_img[0])):
        for x in range(len(new_img)):
            new_img[y][x] = algo[get_new_pixel(img, x-1, y-1, '.')]

    img = new_img

    # second pass
    new_img = [[algo[0]] * (len(img[0])+2) for _ in range(len(img)+2)]
    for y in range(len(new_img[0])):
        for x in range(len(new_img)):
            new_img[y][x] = algo[get_new_pixel(img, x-1, y-1, algo[0])]
    
    return new_img

img = get_new_img_2_pass(algo, img)

part1 = sum(line.count('#') for line in img)
print('Part 1:', part1)
puzzle.answer_a = part1

for _ in range(24):
    img = get_new_img_2_pass(algo, img)

part2 = sum(line.count('#') for line in img)
print('Part 2:', part2)
puzzle.answer_b = part2
