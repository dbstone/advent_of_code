from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=25)

data = puzzle.input_data.splitlines()
# data = open('2021/test.txt').read().splitlines() # test

H = len(data)
W = len(data[0])

easts = set()
souths = set()

for row, line in enumerate(data):
    for col, c in enumerate(line):
        if c == '>':
            easts.add((row, col))
        elif c == 'v':
            souths.add((row, col))

def step(easts, souths):
    next_easts = set()
    next_souths = set()

    moved = False

    for cuke in easts:
        adj = (cuke[0], (cuke[1]+1)%W)
        if adj not in easts and adj not in souths:
            moved = True
            next_easts.add(adj)
        else:
            next_easts.add(cuke)
    
    for cuke in souths:
        adj = ((cuke[0]+1)%H, cuke[1])
        if adj not in next_easts and adj not in souths:
            moved = True
            next_souths.add(adj)
        else:
            next_souths.add(cuke)

    return next_easts, next_souths, moved

moved = True
steps = 0
while moved:
    easts, souths, moved = step(easts, souths)
    steps += 1

part1 = steps
print('Part 1:', part1)
puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
