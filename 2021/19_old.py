from aocd.models import Puzzle
from dotenv import load_dotenv
from ast import literal_eval
from itertools import combinations, permutations, product
import numpy as np

load_dotenv()
puzzle = Puzzle(year=2021, day=19)

data = []
for scanner in puzzle.input_data.split('\n\n'):
    beacons = []
    for beacon in scanner.splitlines()[1:]:
        beacons.append(literal_eval(beacon))
    data.append(beacons)

# print(data[0])

scanner_positions = [None] * len(data)

# TODO: use this later
# for scanner_a in data:
#     for scanner_b in data:
#         if scanner_a != scanner_b:

# [1, 1, 1] # positive x
# [-1, 1, -1] # negative x
# positive y
# negative y
# positive z
# negative z
# rotations = [(1, 1, 1),
#     (-1, 1, 1),
#     (-1, -1, 1),
#     (-1, -1, -1),
#     (1, -1, -1),
#     (1, 1, -1),
#     (-1, 1, -1),
#     (1, -1, 1)]

def add(a, b):
    return (a[0]+b[0],a[1]+b[1],a[2]+b[2])

def sub(a, b):
    return (a[0]-b[0],a[1]-b[1],a[2]-b[2])

def rotations(array):
    for x, y, z in permutations([0, 1, 2]):
        for sx, sy, sz in product([-1, 1], repeat=3):
            rotation_matrix = np.zeros((3, 3))
            rotation_matrix[0, x] = sx
            rotation_matrix[1, y] = sy
            rotation_matrix[2, z] = sz
            if np.linalg.det(rotation_matrix) == 1:
                yield np.matmul(rotation_matrix, array)

def roll(v): return (v[0],v[2],-v[1])
def turn(v): return (-v[1],v[0],v[2])
def sequence (v):
    for cycle in range(2):
        for step in range(3):  # Yield RTTT 3 times
            v = roll(v)
            yield(v)           #    Yield R
            for i in range(3): #    Yield TTT
                v = turn(v)
                yield(v)
        v = roll(turn(roll(v)))  # Do RTR

# p = sequence(( 1, 1, 1))
# q = sequence((-1,-1, 1))
# for i in sorted(zip(p,q)):
#     print i

# first, let's just find a scanner that overlaps with scanner 0
scanner0_rotations = []
for beacon in data[0]:
    # scanner0_rotations.append(list(rotations(np.array(beacon))))
    scanner0_rotations.append(list(sequence(beacon)))

scanner0_rotations = [list(x) for x in zip(*scanner0_rotations)]
print(len(data))
for scanner in data[1:]:
    # get all of this scanner's rotations
    scanner_rotations = []
    for beacon in scanner:
        scanner_rotations.append(list(sequence(beacon)))
    
    scanner_rotations = [list(x) for x in zip(*scanner_rotations)]
    for i, rot in enumerate(scanner_rotations):
        # assume a beacon from this scanner and scanner are the same
        rot_set = set(rot)
        for beacon in rot:
            for beacon0 in scanner0_rotations[0]:
                # check for alignment with other beacons
                for beacon0b in scanner0_rotations[0]:
                    if beacon0 != beacon0b:
                        dist = sub(beacon0, beacon0b)
                        for beaconb in rot:
                            if beaconb != beacon:
                                if sub(beacon, beaconb) == dist:
                                    print('hooray!')
                                    break

        


    



    
    # compare this scanners rotations to scanner0's rotations


    # for r in rotations:
    #     for 






# part1 = None
# print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2


TODO:
- compile a list of each pairwise distance between beacons for each scanner
- compare these distance lists (use sets). pairs of scanners with 12*11 matching distances have overlapping vision cubes 