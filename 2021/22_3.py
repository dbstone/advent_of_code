from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2021, day=22)

onoffs = []
cuboids = []

data = puzzle.input_data
# data = open('2021/test.txt').read()

for line in data.splitlines():
    onoff, coords = line.split(' ')
    onoffs.append(True if onoff == 'on' else False)
    
    coords = coords.split(',')
    coords2 = []
    for coord in coords:
        coord = coord.split('=')[1]
        l, h = (int(x)+50 for x in coord.split('..'))
        coords2.append((l, h))
    cuboids.append(tuple(coords2))

# returns (bool: keep_existing, []: remainder)
def subtract(existing, off):
    ex, ey, ez = existing
    nx, ny, nz = off
    
    # handle non-intersection
    if (ex[1] < nx[0] or
        ex[0] > nx[1] or
        ey[1] < ny[0] or
        ey[0] > ny[1] or
        ez[1] < nz[0] or
        ez[0] > nz[1]):
        return True, None, [off]
    
    # handle new surrounds existing
    if (ex[0] >= nx[0] and
        ex[1] <= nx[1] and
        ey[0] >= ny[0] and
        ey[1] <= ny[1] and
        ez[0] >= nz[0] and
        ez[1] <= nz[1]):
        return False, None, [off] # TODO: OPTIMIZATION: we should generate new OFF cuboids for this
    
    # UNNECESSARY, BECAUSE ALREADY HANDLED BY BELOW LOGIC
    # # handle existing surrounds new
    # if (ex[0] < nx[0] and
    #     ex[1] > nx[1] and
    #     ey[0] < ny[0] and
    #     ey[1] > ny[1] and
    #     ez[0] < nz[0] and
    #     ez[1] > nz[1]):
    #     on_remainder = []
    #     # right
    #     on_remainder.append(((nx[1]+1, ex[1]), ey, ez))
    #     # left
    #     on_remainder.append(((ex[0], nx[0]-1), ey, ez))
    #     # top
    #     on_remainder.append((nx, (ny[1]+1, ey[1]), ez))
    #     # bottom
    #     on_remainder.append((nx, (ey[0], ny[0]-1), ez))
    #     # front
    #     on_remainder.append((nx, ny, (nz[1]+1, ez[1])))
    #     # back
    #     on_remainder.append((nx, ny, (ez[0], nz[0]-1)))
    #     return False, on_remainder, None
    
    ex, nx = nx, ex
    ey, ny = ny, ey
    ez, nz = nz, ez
    remainder = []
    # x axis +
    if nx[1] > ex[1]:
        remainder.append(((ex[1]+1, nx[1]), ny, nz))
    # x axis -
    if nx[0] < ex[0]:
        remainder.append(((nx[0], ex[0]-1), ny, nz))
    # y axis +
    if ny[1] > ey[1]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (ey[1]+1, ny[1]), nz))
    # y axis -
    if ny[0] < ey[0]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (ny[0], ey[0]-1), nz))
    # z axis +
    if nz[1] > ez[1]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (max(ey[0], ny[0]), min(ey[1], ny[1])), (ez[1]+1, nz[1])))
    # z axis -
    if nz[0] < ez[0]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (max(ey[0], ny[0]), min(ey[1], ny[1])), (nz[0], ez[0]-1)))

    return False, remainder, [off] # TODO: OPTIMIZATION: generate new OFFs for this
    
# returns (bool: keep_existing, []: remainder)
def intersect(existing, new):
    ex, ey, ez = existing
    nx, ny, nz = new
    
    # handle non-intersection
    if (ex[1] < nx[0] or
        ex[0] > nx[1] or
        ey[1] < ny[0] or
        ey[0] > ny[1] or
        ez[1] < nz[0] or
        ez[0] > nz[1]):
        return True, [new]
    
    # handle existing surrounds new
    if (ex[0] <= nx[0] and
        ex[1] >= nx[1] and
        ey[0] <= ny[0] and
        ey[1] >= ny[1] and
        ez[0] <= nz[0] and
        ez[1] >= nz[1]):
        return True, None

    # handle new surrounds existing
    if (ex[0] >= nx[0] and
        ex[1] <= nx[1] and
        ey[0] >= ny[0] and
        ey[1] <= ny[1] and
        ez[0] >= nz[0] and
        ez[1] <= nz[1]):
        return False, [new] # TODO: OPTIMIZATION: generate new news using exclusion
    
    remainder = []
    # x axis +
    if nx[1] > ex[1]:
        remainder.append(((ex[1]+1, nx[1]), ny, nz))
    # x axis -
    if nx[0] < ex[0]:
        remainder.append(((nx[0], ex[0]-1), ny, nz))
    # y axis +
    if ny[1] > ey[1]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (ey[1]+1, ny[1]), nz))
    # y axis -
    if ny[0] < ey[0]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (ny[0], ey[0]-1), nz))
    # z axis +
    if nz[1] > ez[1]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (max(ey[0], ny[0]), min(ey[1], ny[1])), (ez[1]+1, nz[1])))
    # z axis -
    if nz[0] < ez[0]:
        remainder.append(((max(ex[0], nx[0]), min(ex[1], nx[1])), (max(ey[0], ny[0]), min(ey[1], ny[1])), (nz[0], ez[0]-1)))

    return True, remainder

# account for case when first cuboid is OFF
while onoffs[0] == False:
    onoffs.pop(0)
    cuboids.pop(0)

model = set([cuboids.pop(0)]) # collection of independent, non-overlapping cuboids
onoffs.pop(0)

for i in range(len(cuboids)):
    queue = [cuboids[i]]
    if onoffs[i]:
        for existing in model.copy():
            remainders = []
            while queue:
                c = queue.pop()
                keep_existing, remainder = intersect(existing, c)
                if remainder:
                    remainders += remainder
                if not keep_existing:
                    model.remove(existing)
                    break
            queue += remainders
            if not queue:
                break
        model |= set(queue)
    else:
        for existing in model.copy():
            remainders = []
            while queue:
                c = queue.pop()
                keep_existing, on_rem, off_rem = subtract(existing, c)
                if off_rem:
                    remainders += off_rem
                if on_rem:
                    for rem in on_rem:
                        model.add(rem)
                if not keep_existing:
                    model.remove(existing)
                    break
            queue += remainders
            
            if not queue:
                break

# sum volume of cuboids in model
part2 = sum((x[1]-x[0]+1)*(y[1]-y[0]+1)*(z[1]-z[0]+1) for x,y,z in model)

print('Part 2:', part2)
puzzle.answer_b = part2

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
