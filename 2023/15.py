from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=15)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

def hash_label(label):
    val = 0
    for c in label:
        val += ord(c)
        val *= 17
        val = val % 256
    return val

steps = data.replace('\n', '')
steps = steps.split(',')
p1 = 0
for step in steps:
    p1 += hash_label(step)

print('Part 1:', p1)
puzzle.answer_a = p1

boxes = [[] for _ in range(256)]
data = data.replace('\n', '')
steps = data.split(',')
for step in steps:
    if '=' in step:
        label, focal_length = step.split('=')
        box = hash_label(label)
        loc = None
        for i in range(len(boxes[box])):
            if boxes[box][i][0] == label:
                loc = i
        if loc != None:
            boxes[box].pop(loc)
            boxes[box].insert(loc, (label,int(focal_length)))
        else:
            boxes[box].append((label,int(focal_length)))
    else:
        label = step[:-1]
        box = hash_label(label)
        loc = None
        for i in range(len(boxes[box])):
            if boxes[box][i][0] == label:
                loc = i
        if loc != None:
            boxes[box].pop(loc)

p2 = 0
for i in range(len(boxes)):
    for j in range(len(boxes[i])):
        p2 += (1+i) * (1+j) * boxes[i][j][1]

print('Part 2:', p2)
puzzle.answer_b = p2