from aocd.models import Puzzle
from dotenv import load_dotenv
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=5)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

# parse
data = data.split('\n\n')
seeds = data[0]
_, seeds = seeds.split(': ')
seeds = [int(seed) for seed in seeds.split()]
mps = []
for m in data[1:]:
    mp = []
    for line in m.splitlines()[1:]:
        mp.append(tuple(int(s) for s in line.split()))
    mps.append(mp)

# convert
locations = []
for s in seeds:
    for mp in mps:
        for rs in mp:
            d = s-rs[1]
            if d >= 0 and d < rs[2]:
                # match
                s = rs[0] + d
                break
    locations.append(s)

p1 = min(locations)
print('Part 1:', p1)
# puzzle.answer_a = p1

srs = set()
for i,s in enumerate(seeds):
    if i % 2 == 0:
        srs.add(range(s,s+seeds[i+1]))

for mp in mps:
    new_srs = set()
    while srs:
        s = srs.pop()
        intersection = False
        for r in mp:
            rs = range(r[1], r[1] + r[2])
            if s.start < rs.stop and s.stop > rs.start: 
                intersection = True
                # convert intersection and add to new set
                i1 = max(s.start, rs.start)
                i1 = i1 - rs.start + r[0]
                i2 = min(s.stop, rs.stop)
                i2 = i2 - rs.start + r[0]
                new_srs.add(range(i1, i2))
                
                # put leftovers back into srs
                # left side
                if s.start < rs.start:
                    srs.add(range(s.start, rs.start))
                # right side
                if s.stop > rs.stop:
                    srs.add(range(rs.stop, s.stop))
        if not intersection:
            new_srs.add(s)
    srs = new_srs

p2 = min(r.start for r in srs)
print('Part 2:', p2)
puzzle.answer_b = p2
