from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import functools
import ast
from itertools import combinations
import numpy

load_dotenv()
puzzle = Puzzle(year=2023, day=11)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

data = [list(s) for s in data.splitlines()]

# W = len(data[0])
# H = len(data)

# new_data = []
# for s in data:
#     if all(c == '.' for c in s):
#         new_data.append(['.']*W)
#         new_data.append(['.']*W)
#     else:
#         new_data.append(s.copy())

# new_data = numpy.array(new_data)
# # print(new_data.shape)
# # print(len(new_data), len(new_data[0]))
# new_data = numpy.transpose(new_data)
# # print(len(new_data), len(new_data[0]))
# new_new_data = []
# for s in new_data:
#     if all(c == '.' for c in s):
#         new_new_data.append(['.']*len(s))
#         new_new_data.append(['.']*len(s))
#     else:
#         new_new_data.append(s.copy())

# print(len(new_new_data), len(new_new_data[0]))
# new_new_data = numpy.array(new_new_data)
# new_new_data = numpy.transpose(new_new_data)

def manhattan_distance(a, b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])

# print(new_new_data)
# print(new_new_data.shape)

# galaxies = set()
# for x in range(len(new_new_data[0])):
#     for y in range(len(new_new_data)):
#         if new_new_data[y][x] == '#':
#             galaxies.add((x,y))

# print(new_new_data)
# print(len(list(combinations(galaxies,2))))
# p1 = sum(manhattan_distance(a,b) for a,b in combinations(galaxies, 2))


data = numpy.array(data)
yempty = set()
xempty = set()
for y,s in enumerate(data):
    if all(c == '.' for c in s):
        yempty.add(y)
for x,s in enumerate(data.transpose()):
    if all(c == '.' for c in s):
        xempty.add(x)

galaxies = set()
for x in range(len(data[0])):
    for y in range(len(data)):
        if data[y][x] == '#':
            galaxies.add((x,y))

def dist(a, b, expand_factor):
    d = manhattan_distance(a,b)
    for y in range(min(a[1],b[1]),max(a[1],b[1])):
        if y in yempty:
            d += expand_factor-1
    for x in range(min(a[0],b[0]),max(a[0],b[0])):
        if x in xempty:
            d += expand_factor-1
    return d

p1 = sum(dist(a,b,2) for a,b in combinations(galaxies, 2))
print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = sum(dist(a,b,1000000) for a,b in combinations(galaxies, 2))
print('Part 2:', p2)
# puzzle.answer_b = p2