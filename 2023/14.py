from aocd.models import Puzzle
from dotenv import load_dotenv
import utils

load_dotenv()
puzzle = Puzzle(year=2023, day=14)
data = puzzle.input_data

def load(grid):
    l = 0
    grid = utils.transpose(grid)
    for line in grid:
        for i,v in enumerate(line):
            if v == 'O':
                l += len(line)-i
    return l

def east_or_west(grid, east):
    new_grid = []
    for line in grid:
        line = ''.join(line)
        segs = line.split('#')
        new_segs = []
        for seg in segs:
            if east:
                new_seg = '.' * seg.count('.') + 'O' * seg.count('O')
            else: # west
                new_seg = 'O' * seg.count('O') + '.' * seg.count('.')
            new_segs.append(new_seg)
        new_grid.append('#'.join(new_segs))
    return new_grid

def east(grid):
    return east_or_west(grid, True)

def west(grid):
    return east_or_west(grid, False)

def north(grid):
    grid = utils.transpose(grid)
    grid = west(grid)
    grid = utils.transpose(grid)
    return grid

def south(grid):
    grid = utils.transpose(grid)
    grid = east(grid)
    grid = utils.transpose(grid)
    return grid

def cycle(grid):
    grid = north(grid)
    grid = west(grid)
    grid = south(grid)
    grid = east(grid)
    return grid

grid = utils.string_to_grid(data)
grid = north(grid)
p1 = load(grid)
print('Part 1:', p1)
# puzzle.answer_a = p1

grid = utils.string_to_grid(data)
grids = {str(grid):0}

intercept = None
period = None
for i in range(1,1000000001):
    grid = cycle(grid)
    hsh = str(grid)
    if hsh in grids:
        period = i - grids[hsh]
        intercept = grids[hsh]
        break
    grids[hsh] = i

todo = (1000000000 - intercept) % period

for _ in range(todo):
    grid = cycle(grid)

p2 = load(grid)
print('Part 2:', p2)
# puzzle.answer_b = p2