def transpose(grid):
    return list(zip(*grid))

def transpose_list_of_strings(grid):
    return [''.join(line) for line in transpose(grid)]

def string_to_grid(string):
    return [list(line) for line in string.splitlines()]

def manhattan_distance(a, b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])

def intersect_range(ra, rb):
    return range(max(ra.start, rb.start), min(ra.stop, rb.stop))