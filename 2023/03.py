from aocd.models import Puzzle
from dotenv import load_dotenv
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=3)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

data = [line for line in data.splitlines()]

# find all nums and their coordinates
nums = set()
for y in range(len(data)):
    x = 0
    while x < len(data[y]):
        c = data[y][x]
        if c.isdecimal():
            x0 = x
            num = c
            while True:
                x += 1
                if x == len(data[y]):
                    break
                c = data[y][x]
                if not c.isdecimal():
                    break
                num += c
            nums.add((x0,y,int(num)))
        x += 1

# sum the nums that are adjacent to a symbol
p1 = 0
for num in nums:
    x,y,n = num
    good = False
    while x < len(data[y]) and data[y][x].isdecimal() and not good:
        for xx in range(x-1, x+2):
            if good:
                break
            for yy in range(y-1, y+2):
                if good:
                    break
                if xx == x and yy == y:
                    continue
                if xx >= 0 and xx < len(data[y]) and yy >= 0 and yy < len(data):
                    c = data[yy][xx]
                    if not c.isdecimal() and c != '.':
                        # found part symbol
                        good = True
        x += 1
    if good:
        p1 += n

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for y in range(len(data)):
    for x in range(len(data[y])):
        c = data[y][x]
        if c == '*':
            # part found, search for surrounding part numbers
            nums = []
            checked = set() # keep track of checked cells to avoid finding the same number twice
            for xx in range(x-1, x+2):
                for yy in range(y-1, y+2):
                    if yy == y and xx == x:
                        continue
                    if xx >= 0 and xx < len(data[y]) and yy >= 0 and yy < len(data) and (xx, yy) not in checked:
                        checked.add((xx, yy))
                        cc = data[yy][xx]
                        if cc.isdecimal():
                            part_num = cc
                            # number found
                            xxx = xx
                            # extend number right
                            xxx += 1
                            while xxx < len(data[y]):
                                if (xxx, yy) not in checked:
                                    checked.add((xxx, yy))
                                    cc = data[yy][xxx]
                                    if cc.isdecimal():
                                        part_num += cc
                                    else:
                                        break
                                xxx += 1
                            # extend number left
                            xxx = xx-1
                            while xxx >= 0:
                                cc = data[yy][xxx]
                                if not data[yy][xxx].isdecimal():
                                    break
                                if (xxx, yy) not in checked:
                                    checked.add((xxx, yy))
                                    part_num = cc + part_num
                                xxx -= 1
                            
                            nums.append(int(part_num))

            if len(nums) == 2:
                # it's a gear!
                p2 += nums[0] * nums[1]

print('Part 2:', p2)
# puzzle.answer_b = p2


