from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import utils

load_dotenv()
puzzle = Puzzle(year=2023, day=14)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

grid = data.splitlines()
grid = [''.join(line) for line in utils.transpose(grid)]

p1 = 0
new_grid = []
for line in grid:
    segs = line.split('#')
    new_segs = []
    for seg in segs:
        new_seg = 'O' * seg.count('O') + '.' * seg.count('.')
        new_segs.append(new_seg)
    new = '#'.join(new_segs)
    for i,v in enumerate(new):
        if v == 'O':
            p1 += len(new)-i
            # print(len(new)-i)
    new_grid.append(new)

print('Part 1:', p1)
# puzzle.answer_a = p1

def load(grid):
    l = 0
    grid = [''.join(line) for line in utils.transpose(grid)]
    for line in grid:
        for i,v in enumerate(line):
            if v == 'O':
                l += len(line)-i
                # print(len(new)-i)
    return l

def west(grid):
    new_grid = []
    for line in grid:
        segs = line.split('#')
        new_segs = []
        for seg in segs:
            new_seg = 'O' * seg.count('O') + '.' * seg.count('.')
            new_segs.append(new_seg)
        new_grid.append('#'.join(new_segs))
    return new_grid

def north(grid):
    grid = [''.join(line) for line in utils.transpose(grid)]
    grid = west(grid)
    grid = [''.join(line) for line in utils.transpose(grid)]
    return grid

def east(grid):
    new_grid = []
    for line in grid:
        segs = line.split('#')
        new_segs = []
        for seg in segs:
            new_seg = '.' * seg.count('.') + 'O' * seg.count('O')
            new_segs.append(new_seg)
        new_grid.append('#'.join(new_segs))
    return new_grid

def south(grid):
    grid = [''.join(line) for line in utils.transpose(grid)]
    grid = east(grid)
    grid = [''.join(line) for line in utils.transpose(grid)]
    return grid

def cycle(grid):
    grid = north(grid)
    grid = west(grid)
    grid = south(grid)
    grid = east(grid)
    return grid

grid = data.splitlines()
grids = {''.join(grid):0}

first = None
period = None
for i in range(1,1000000001):
    grid = cycle(grid)
    hsh = '*'.join(grid)
    if hsh in grids:
        period = i - grids[hsh]
        first = grids[hsh]
        break
    grids[hsh] = i

todo = (1000000000 - first) % period

for i in range(0,todo):
    grid = cycle(grid)

p2 = load(grid)
print('Part 2:', p2)
# puzzle.answer_b = p2