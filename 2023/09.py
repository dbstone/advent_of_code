from aocd.models import Puzzle
from dotenv import load_dotenv
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=9)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

p1 = 0

for line in data.splitlines():
    diffs = []
    curr = [int(n) for n in line.split()]
    while not all(n == 0 for n in curr):
        diffs.append(curr)
        diff = []
        for j in range(1, len(curr)):
            diff.append(curr[j]-curr[j-1])
        curr = diff
    diffs[-1].append(diffs[-1][-1])
    i = len(diffs)-2
    while i >= 0:
        diffs[i].append(diffs[i][-1]+diffs[i+1][-1])
        i -= 1
    p1 += diffs[0][-1]


# print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for line in data.splitlines():
    diffs = []
    curr = [int(n) for n in line.split()]
    while not all(n == 0 for n in curr):
        diffs.append(curr)
        diff = []
        for j in range(1, len(curr)):
            diff.append(curr[j]-curr[j-1])
        curr = diff
    diffs[-1].insert(0, diffs[-1][0])
    i = len(diffs)-2
    while i >= 0:
        diffs[i].insert(0, diffs[i][0]-diffs[i+1][0])
        i -= 1
    p2 += diffs[0][0]

print('Part 2:', p2)
puzzle.answer_b = p2