from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=13)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

def find_vert_reflection(l):
    for y in range(len(l[:-1])):
        top = l[:y+1]
        bot = l[y+1:]
        match = True
        for i in range(min(len(top), len(bot))):
            if top[-1-i] != bot[i]:
                match = False
                break
        if match:
            return y+1
    return 0

def find_smudged_vert_reflection(l):
    for y in range(len(l[:-1])):
        top = l[:y+1]
        bot = l[y+1:]
        diffs = 0
        for i in range(min(len(top), len(bot))):
            tline = top[-1-i]
            bline = bot[i]
            for ci in range(len(tline)):
                if tline[ci] != bline[ci]:
                    diffs += 1
        if diffs == 1:
            return y+1
    return 0

def solve(data, p2=False):
    solution = 0
    for note in data.split('\n\n'):
        lines = note.splitlines()
        reflection = find_smudged_vert_reflection(lines) if p2 else find_vert_reflection(lines) 
        if reflection > 0:
            solution += 100 * reflection
        else:
            transposed = utils.transpose(lines)
            reflection = find_smudged_vert_reflection(transposed) if p2 else find_vert_reflection(transposed)
            if reflection > 0:
                solution += reflection
    return solution

p1 = solve(data)
print('Part 1:', p1)
puzzle.answer_a = p1

p2 = solve(data, True)
print('Part 2:', p2)
puzzle.answer_b = p2