from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import re
from functools import cache

load_dotenv()
puzzle = Puzzle(year=2023, day=12)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

@cache
def possible(record, string):
    needed = sum(int(i) for i in string.split(','))
    pounds = record.count('#')
    qs = record.count('?')
    if pounds + qs < needed:
        return False
    if pounds > needed:
        return False

    new = record.replace('.', ' ')    
    new = new.replace('?', ' ')
    longest = max(len(i) for i in new)
    if all(longest > int(i) for i in string.split(',')):
        return False
    return True

@cache
def simplify(record, string):
    if not string and '#' in record:
        return None, None, False
    if string and not possible(record, string):
        return None, None, False
    record = ''.join(c if c != '.' else ' ' for c in record)
    record = record.split()
    string = string.split(',')
    # if len(record) == 1 and len(string) == 1 and '#' in record[0]:
    #     if len(record[0]) < int(string[0]):
    #         return None, None, False
    #     first = record[0].find('#')
    #     last = record[0].rfind('#')
    #     new = record[0][:first] + '#'*(last-first+1) + record[0][last+1:]
    #     record[0] = new
    #     diff = int(string[0]) - record[0].count('#')
    #     if diff == 0:
    #         return None, None, True
    #     elif diff < 0:
    #         return None, None, False
    #     else:
    #         record = record[first-diff:last+diff+1]
    if record and string and string[0]:
        leading = 0
        for i in range(len(record[0])):
            if record[0][i] != '#':
                leading = i
                break
        if leading > int(string[0]):
            return None, None, False
    if record and string and string[-1]:
        trailing = 0
        for i in reversed(range(len(record[-1]))):
            if record[-1][i] != '#':
                leading = len(record[-1]) - i
                break
        if trailing > int(string[-1]):
            return None, None, False
    while record and string:
        r = record[0]
        if '?' in r:
            break
        if len(record[0]) != int(string[0]) if string[0] else 0:
            return None, None, False
        record = record[1:]
        string = string[1:]
    while record and string:
        r = record[-1]
        if '?' in r:
            break
        if len(record[-1]) != int(string[-1]) if string[-1] else 0:
            return None, None, False
        record = record[:-1]
        string = string[:-1]
    return '.'.join(record), ','.join(string), True

@cache
def get_num_valid_configs(record, string):
    # oldrecord = record
    # oldstring = string
    record, string, good = simplify(record, string)
    if not good:
        return 0
    if not record:
        return 1 if not string else 0

    return get_num_valid_configs(record.replace('?', '#', 1), string) + get_num_valid_configs(record.replace('?', '.', 1), string) 

p1 = 0
for line in data.splitlines():
    record, string = line.split()
    record = record.replace('.', ' ')
    record = '.'.join(record.split())
    temp = get_num_valid_configs(record, string) 
    # print(line, temp)
    p1 += temp

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for line in data.splitlines():
    record, string = line.split()
    record = '?'.join([record]*5)
    string = ','.join([string]*5)
    record = record.replace('.', ' ')
    record = '.'.join(record.split())
    temp = get_num_valid_configs(record, string) 
    # print(line, temp)
    p2 += temp

print('Part 2:', p2)
# puzzle.answer_b = p2