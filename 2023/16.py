from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
import sys

sys.setrecursionlimit(10000)
load_dotenv()
puzzle = Puzzle(year=2023, day=16)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

grid = utils.string_to_grid(data)

energized = set()
visited = set()

def valid(coord):
    if coord[0] in range(len(grid[0])) and coord[1] in range(len(grid)):
        return True
    return False

def west(coord):
    if not valid(coord):
        return
    v = (coord, 'w')
    if v in visited:
        return
    visited.add(v)
    x,y = coord
    energized.add(coord)
    c = grid[y][x]
    if c == '.' or c == '-':
        west((x-1,y))
    elif c == '/':
        south((x,y+1))
    elif c == '\\':
        north((x,y-1))
    elif c == '|':
        north((x,y-1))
        south((x,y+1))

def east(coord):
    if not valid(coord):
        return
    v = (coord, 'e')
    if v in visited:
        return
    visited.add(v)
    x,y = coord
    energized.add(coord)
    c = grid[y][x]
    if c == '.' or c == '-':
        east((x+1,y))
    elif c == '/':
        north((x,y-1))
    elif c == '\\':
        south((x,y+1))
    elif c == '|':
        north((x,y-1))
        south((x,y+1))

def south(coord):
    if not valid(coord):
        return
    v = (coord, 's')
    if v in visited:
        return
    visited.add(v)
    x,y = coord
    energized.add(coord)
    c = grid[y][x]
    if c == '.' or c == '|':
        south((x,y+1))
    elif c == '/':
        west((x-1,y))
    elif c == '\\':
        east((x+1,y))
    elif c == '-':
        west((x-1,y))
        east((x+1,y))

def north(coord):
    if not valid(coord):
        return
    v = (coord, 'n')
    if v in visited:
        return
    visited.add(v)
    x,y = coord
    energized.add(coord)
    c = grid[y][x]
    if c == '.' or c == '|':
        north((x,y-1))
    elif c == '/':
        east((x+1,y))
    elif c == '\\':
        west((x-1,y))
    elif c == '-':
        west((x-1,y))
        east((x+1,y))

# def cast(coord, direction):
#     north(coord)

east((0,0))

p1 = len(energized)
print('Part 1:', p1)
# puzzle.answer_a = p1

# p2 = None
# print('Part 2:', p2)
# puzzle.answer_b = p2