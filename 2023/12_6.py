from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import re
from functools import lru_cache

load_dotenv()
puzzle = Puzzle(year=2023, day=12)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

@lru_cache
def possible(record, string):
    needed = sum(int(i) for i in string.split(','))
    pounds = record.count('#')
    qs = record.count('?')
    if pounds + qs < needed:
        return False
    if pounds > needed:
        return False

    # new = record.replace('.', ' ')    
    # new = new.replace('?', ' ')
    # longest = max(len(i) for i in new)
    # if all(longest > int(i) for i in string.split(',')):
    #     return False
    return True

@lru_cache
def simplify(record, string):
    if not string and '#' in record:
        return None, None, False
    if string and not possible(record, string):
        return None, None, False
    record = ''.join(c if c != '.' else ' ' for c in record)
    record = record.split()
    string = string.split(',')
    if len(record) == 1 and len(string) == 1 and '#' in record[0]:
        if len(record[0]) < int(string[0]):
            return None, None, False
        first = record[0].find('#')
        last = record[0].rfind('#')
        new = record[0][:first] + '#'*(last-first+1) + record[0][last+1:]
        record[0] = new
        # THIS DOESN'T WORK:
        # diff = int(string[0]) - record[0].count('#')
        # if diff == 0:
        #     return None, None, True
        # elif diff < 0:
        #     return None, None, False
        # else:
        #     record = record[first-diff:last+diff+1]
    if record and string and string[0]:
        leading = 0
        for i in range(len(record[0])):
            if record[0][i] != '#':
                leading = i
                break
        if leading > int(string[0]):
            return None, None, False
    # if record and string and string[-1]:
    #     trailing = 0
    #     for i in reversed(range(len(record[-1]))):
    #         if record[-1][i] != '#':
    #             leading = len(record[-1]) - i
    #             break
    #     if trailing > int(string[-1]):
    #         return None, None, False
    while record and string:
        stringstart = int(string[0]) if string[0] else 0
        r = record[0]
        if '?' in r:
            break
        if len(record[0]) != stringstart:
            return None, None, False
        record = record[1:]
        string = string[1:]
        # r = record[-1]
        # if '?' in r:
        #     break
        # if len(record[-1]) != int(string[-1]) if string[-1] else 0:
        #     return None, None, False
        # record = record[:-1]
        # string = string[:-1]
    return '.'.join(record), ','.join(string), True

@lru_cache
def get_num_valid_configs(record, string):
    record, string, good = simplify(record, string)
    if not good:
        return 0
    if not record:
        return 1 if not string else 0
    return get_num_valid_configs(record.replace('?', '.', 1), string) + get_num_valid_configs(record.replace('?', '#', 1), string) 

@lru_cache
def get_num_valid_configs2(record, string):
    if not record:
        return 0
    string = string.split(',')
    l = int(string[0])
    valid = 0
    end = len(record)-l+1
    if '#' in record:
        end = min(end, record.find('#')+1)
    for i in range(end):
        if '.' not in record[i:i+l]:
            if i+l >= len(record) or record[i+l] != '#':
                # if i-1 < 0 or '#' not in record[:i-1] != '#':
                if len(string) == 1:
                    if len(record) == i+l or '#' not in record[i+l:]:
                        valid += 1
                elif i+l+1 < len(record):
                    valid += get_num_valid_configs2(record[i+l+1:], ','.join(string[1:]))
    return valid

    record, string, good = simplify(record, string)
    if not good:
        return 0
    if not record:
        return 1 if not string else 0
    
    # last = record.rfind('?')
    # l = record[:last] + '#' + record[last+1:]
    # r = record[:last] + '.' + record[last+1:]
    # return get_num_valid_configs(l, string) + get_num_valid_configs(r, string) 

# correct = []
# p1 = 0
# for line in data.splitlines():
#     record, string = line.split()
#     record = record.replace('.', ' ')
#     record = '.'.join(record.split())
#     temp = get_num_valid_configs(record, string) 
#     # print(line, temp)
#     correct.append(temp)
#     p1 += temp

# print('Part 1:', p1)

p1 = 0
for i,line in enumerate(data.splitlines()):
    record, string = line.split()
    # record = record.replace('.', ' ')
    # record = '.'.join(record.split())
    temp = get_num_valid_configs2(record, string) 
    # if temp != correct[i]:
    #     print(line, temp, correct[i])
    print(line, temp)
    p1 += temp

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for line in data.splitlines():
    record, string = line.split()
    record = '?'.join([record]*5)
    string = ','.join([string]*5)
    record = record.replace('.', ' ')
    record = '.'.join(record.split())
    temp = get_num_valid_configs2(record, string) 
    print(line, temp)
    p2 += temp

print('Part 2:', p2)
puzzle.answer_b = p2