from aocd.models import Puzzle
from dotenv import load_dotenv
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=6)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

t, d = data.splitlines()
ts = [int(t) for t in t[10:].split()]
ds = [int(d) for d in d[10:].split()]

p1 = 1
for i in range(len(ts)):
    t = ts[i]
    d = ds[i]
    wins = 0
    for x in range(t+1):
        dd = x * (t-x)
        if dd > d:
            wins += 1
    p1 *= wins

print('Part 1:', p1)
# puzzle.answer_a = p1

t, d = data.splitlines()
t = int(''.join(t[10:].split()))
d = int(''.join(d[10:].split()))

l = 0
for x in range(t+1):
    dd = x * (t-x)
    if dd > d:
        l = x
        break
h = 0
for x in reversed(range(t+1)):
    dd = x * (t-x)
    if dd > d:
        h = x
        break
p2 = h-l+1

print('Part 2:', p2)
# puzzle.answer_b = p2
