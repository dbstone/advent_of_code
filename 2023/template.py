from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=1)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

# p1 = None
# print('Part 1:', p1)
# puzzle.answer_a = p1

# p2 = None
# print('Part 2:', p2)
# puzzle.answer_b = p2