from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
from copy import deepcopy

load_dotenv()
puzzle = Puzzle(year=2023, day=19)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

ws, ps = data.split('\n\n')

# parse workflows
workflows = {}
for w in ws.splitlines():
    a, b = w.find('{'), w.find('}')
    name = w[:a]
    rs = w[a+1:b]
    rules = []
    for r in rs.split(','):
        if ':' in r:
            condition, outcome = r.split(':')
            if '<' in condition:
                ca, cn = condition.split('<')
                rules.append(((ca, '<', int(cn)), outcome))
            elif '>' in condition:
                ca, cn = condition.split('>')
                rules.append(((ca, '>', int(cn)), outcome))
        else:
            rules.append((None, r))
    workflows[name] = rules

# parse parts
parts = []
for p in ps.splitlines():
    ss = p[1:-1].split(',')
    scores = {}
    for s in ss:
        label, score = s.split('=')
        scores[label] = int(score)
    parts.append(scores)

p1 = 0
for part in parts:
    next = 'in'
    while True:
        if next == 'A':
            p1 += part['x'] + part['m'] + part['a'] + part['s']
            break
        elif next == 'R':
            break
        wf = workflows[next]
        for rule in wf:
            condition, outcome = rule
            if condition:
                a,b,c = condition
                if b == '>':
                    if part[a] > c:
                        next = outcome
                        break
                elif b == '<':
                    if part[a] < c:
                        next = outcome
                        break
            else:
                next = outcome
                break
                
print('Part 1:', p1)
# puzzle.answer_a = p1

def combs(rs):
    c = 1
    for r in rs.values():
        c *= r.stop - r.start if r.stop > r.start else 0
    return c

l,h = 1, 4000

def get_num_valid_ratings(wfid, rs):
    As = 0
    for con, out in workflows[wfid]:
        rs2 = deepcopy(rs)
        if con:
            a,b,c = con
            r = rs2[a]
            if b == '<':
                rs2[a] = utils.intersect_range(r, range(l,c))
                rs[a] = utils.intersect_range(r, range(c,h+1))
            elif b == '>':
                rs2[a] = utils.intersect_range(r, range(c+1,h+1))
                rs[a] = utils.intersect_range(r, range(l,c+1))
            if out == 'A':
                As += combs(rs2)
            elif out != 'R':
                As += get_num_valid_ratings(out, rs2)
        else:
            if out == 'A':
                return combs(rs) + As
            elif out == 'R':
                return As
            else:
                return get_num_valid_ratings(out, rs) + As
                
start = range(l,h+1)
p2 = get_num_valid_ratings('in', {'x':start,'m':start,'a':start,'s':start})
print('Part 2:', p2)
# puzzle.answer_b = p2