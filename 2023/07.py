from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import functools

load_dotenv()
puzzle = Puzzle(year=2023, day=7)
data = puzzle.input_data
# with open("2023/test2.txt") as file:
#     data = file.read()

def get_hand_type(a, p2=False):
    t = 0
    j_cnt = 0
    if p2:
        j_cnt = a.count('J')
        a = a.replace('J', '')
    for c in set(a):
        if c == 'J':
            continue
        cnt = a.count(c)
        if cnt+j_cnt >= 5:
            t = 6
            break
        elif cnt+j_cnt >= 4:
            t = 5
            break
        elif cnt+j_cnt >= 3:
            j_cnt2 = cnt+j_cnt-3
            for cc in set(a):
                if cc != c:
                    if a.count(cc)+j_cnt2 >= 2:
                        t = 4
                        break
            t = max(t, 3)
            # break
        elif cnt+j_cnt >= 2:
            j_cnt2 = cnt+j_cnt-2
            for cc in set(a):
                if cc != c:
                    if a.count(cc)+j_cnt2 == 3:
                        t = max(t,4)
                        break
                    elif a.count(cc)+j_cnt2 == 2:
                        t = max(t,2)
                        break
            t = max(t, 1)
    return t

def get_hand_type2(a, p2=False):
    t = 0
    j_cnt = 0
    if p2:
        j_cnt = a.count('J')
        a = a.replace('J', '')
    for c in set(a):
        cnt = a.count(c)
        if cnt == 5:
            t = 6
            break
        elif cnt == 4:
            t = 5
            break
        elif cnt == 3:
            for cc in set(a):
                if cc != c:
                    if a.count(cc) == 2:
                        t = 4
                        break
            if t == 0:
                t = 3
            break
        elif cnt == 2:
            for cc in set(a):
                if cc != c:
                    if a.count(cc) == 3:
                        t = 4
                        break
                    elif a.count(cc) == 2:
                        t = 2
                        break
            if t == 0:
                t = 1
    if p2:
        while j_cnt>0:
            j_cnt -= 1
            if t == 5:
                t = 6
            elif t == 4:
                # impossible
                print('fuck')
                t = 6
            elif t == 3:
                t = 5
            elif t == 2:
                t = 4
            elif t == 1:
                t = 3
            elif t == 0:
                t = 1
    return t

card_values = {'2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':11, 'Q':12, 'K':13, 'A':14}
card_values2 = {'J':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'Q':12, 'K':13, 'A':14}

data = [tuple(l.split()) for l in data.splitlines()]

p1 = 0
for i,v in enumerate(sorted(data, key=lambda x: (get_hand_type2(x[0]), card_values[x[0][0]], card_values[x[0][1]], card_values[x[0][2]], card_values[x[0][3]], card_values[x[0][4]]))):
    # print(i, v, get_hand_type2(v[0]))
    p1 += int(v[1]) * (i+1)

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for i,v in enumerate(sorted(data, key=lambda x: (get_hand_type2(x[0], True), card_values2[x[0][0]], card_values2[x[0][1]], card_values2[x[0][2]], card_values2[x[0][3]], card_values2[x[0][4]]))):
    # print(i, v, get_hand_type2(v[0], True))
    p2 += int(v[1]) * (i+1)

print('Part 2:', p2)
puzzle.answer_b = p2

# def get_card_value(a, b):
#     at = get_hand_type(a)
#     bt = get_hand_type(b)
    
#     if at > bt:
#         return 1
#     elif bt > at:
#         return -1
#     else:
#         # tiebreak
#         for i in range(len(a)):
#             av = card_values[a[i]]
#             bv = card_values[b[i]]
#             if av > bv:
#                 return 1
#             elif bv > av:
#                 return -1
#         return 0