from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import re
from functools import lru_cache

load_dotenv()
puzzle = Puzzle(year=2023, day=12)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

@lru_cache
def simplify(record, string):
    if not string and '#' in record:
        return None, None, False
    record = ''.join(c if c != '.' else ' ' for c in record)
    record = record.split()
    string = string.split(',')
    while record and string:
        r = record[0]
        if '?' in r:
            break
        if len(record[0]) != int(string[0]) if string[0] else 0:
            return None, None, False
        record = record[1:]
        string = string[1:]
    while record and string:
        r = record[-1]
        if '?' in r:
            break
        if len(record[-1]) != int(string[-1]) if string[-1] else 0:
            return None, None, False
        record = record[:-1]
        string = string[:-1]
    return '.'.join(record), ','.join(string), True

@lru_cache
def get_num_valid_configs(record, string):
    # oldrecord = record
    # oldstring = string
    record, string, good = simplify(record, string)
    if not good:
        return 0
    if not record:
        return 1 if not string else 0

    return get_num_valid_configs(record.replace('?', '#', 1), string) + get_num_valid_configs(record.replace('?', '.', 1), string) 

p1 = 0
for line in data.splitlines():
    record, string = line.split()
    record = record.replace('.', ' ')
    record = '.'.join(record.split())
    temp = get_num_valid_configs(record, string) 
    # print(temp)
    p1 += temp

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for line in data.splitlines():
    record, string = line.split()
    record = '?'.join([record]*5)
    string = ','.join([string]*5)
    record = record.replace('.', ' ')
    record = '.'.join(record.split())
    temp = get_num_valid_configs(record, string) 
    print(temp)
    p2 += temp

print('Part 2:', p2)
# puzzle.answer_b = p2