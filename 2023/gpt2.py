import time
from heapq import heappush, heappop

def read_input(file_path):
    with open(file_path, "r") as file:
        return file.read().splitlines()

def initialize_grids(lines):
    grid = [[int(c) for c in line] for line in lines]
    heat_loss_grid = [[[] for _ in range(len(line))] for line in lines]
    return grid, heat_loss_grid

def reverse_direction(direction):
    return {"up": "down", "down": "up", "left": "right", "right": "left"}[direction]

def get_steps(start, end, grid, heat_loss_grid):
    visited, to_visit = set(), [(0, start, "", 0)]

    while to_visit:
        h, (r, c), last_direction, count = heappop(to_visit)

        if (h, last_direction, count) not in heat_loss_grid[r][c] and (r, c, last_direction, count) not in visited:
            visited.add((r, c, last_direction, count))
            last_steps = [last_direction] * count

            if len(last_steps) > 3 and all(step == last_steps[-1] for step in last_steps[-3:]):
                heappush(heat_loss_grid[r][c], (h, last_direction, count))
                if (r, c) == end:
                    return h

            directions = {"up": 4, "down": 4, "left": 4, "right": 4}
            directions[last_direction] = directions[reverse_direction(last_direction)] = 0

            for direction, (dr, dc) in [("up", (-1, 0)), ("down", (1, 0)), ("left", (0, -1)), ("right", (0, 1))]:
                new_h = h
                new_steps = last_steps[-9:] + [direction] * directions[direction]
                new_h += sum(grid[r + dr * i][c + dc * i] for i in range(1, directions[direction] + 1))

                if last_direction == direction:
                    heappush(to_visit, (new_h, (r + dr * directions[direction], c + dc * directions[direction]), direction, count + directions[direction]))
                else:
                    heappush(to_visit, (new_h, (r + dr * directions[direction], c + dc * directions[direction]), direction, directions[direction]))

if __name__ == "__main__":
    t0 = time.time()
    input_lines = read_input("2023/test.txt")
    grid, heat_loss_grid = initialize_grids(input_lines)

    start, end = (0, 0), (len(input_lines) - 1, len(input_lines[0]) - 1)
    result = get_steps(start, end, grid, heat_loss_grid)

    print(heat_loss_grid[end[0]][end[1]])
    t1 = time.time()
    print(t1 - t0)