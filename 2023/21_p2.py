from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2023, day=21)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

grid = utils.string_to_grid(data)
start = None
for y in range(len(grid)):
    if start:
        break
    for x in range(len(grid[y])):
        if grid[y][x] == 'S':
            start = (x,y)
            break

def valid(x,y):
    if x >= 0 and x < len(grid[0]):
        if y >= 0 and y < len(grid):
            if grid[y][x] != '#':
                return True
    return False

H = len(grid)
W = len(grid[0])

def print_grid(nexts):
    for y in range(-H, 2*H):
        for x in range(-W, 2*W):
            if (x,y) in nexts:
                print('O', end='')
            else:
                print(grid[y%H][x%W], end='')
        print()

def solve(i):
    left_visited = {0:0}
    next = set([start])
    steps = 0
    while steps < i:
        queue = next
        next = set()
        for p in queue:
            for axis in range(2):
                for offset in range(-1, 2):
                    if offset != 0:
                        adj = list(p)
                        adj[axis] = offset + p[axis]
                        adj = tuple(adj)
                        x,y = adj
                        if grid[y%H][x%W] != '#':
                            next.add(adj)
                        # if y >= 0 and y < len(grid):
                        #     xoff = x // W
                        #     if xoff < 0:
                        #         if xoff not in left_visited:
                        #             left_visited[xoff] = steps
                        xoff = x // W
                        yoff = y // H
                        if xoff < 0 and xoff == -yoff:
                            if xoff not in left_visited:
                                left_visited[xoff] = steps
                        # if valid(*adj):
                        #     next.add(adj)
        # print(print_grid(next))
        # print()

        steps += 1
    return len(next), left_visited

answer, left_visited = solve(700)
print(left_visited)

# diffs = []
# answers = []
# last = 0
# for i in range(100)[::2]:
#     this = solve(i)
#     answers.append(this)
#     diffs.append(this - last)
#     last = this

# # print(answers.index(39))

# depth = 0
# while any(diffs[0] != d for d in diffs):
#     depth += 1
#     diffdiffs = []
#     for i in range(1,len(diffs)):
#         diffdiffs.append(diffs[i]-diffs[i-1])
#     diffs = diffdiffs
#     print(diffs)

# print(depth)
# print(diffs)

# print('Part 2:', p1)
# puzzle.answer_b = p1

# p2 = None
# print('Part 2:', p2)
# puzzle.answer_b = p2