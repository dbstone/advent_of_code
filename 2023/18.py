from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2023, day=18)
data = puzzle.input_data
with open("2023/test.txt") as file:
    data = file.read()

dug = set([(0,0)])

ds = {'R':(1,0), 'L':(-1,0), 'U':(0,-1), 'D':(0,1)}

x, y = 0, 0
for line in data.splitlines():
    d, n, color = line.split()
    n = int(n)
    dx, dy = ds[d]
    for _ in range(n):
        x += dx
        y += dy
        dug.add((x,y))

minx = min(x for x,y in dug)
miny = min(y for x,y in dug)
maxx = max(x for x,y in dug)
maxy = max(y for x,y in dug)

# for y in range(miny,maxy+1):
#     for x in range(minx,maxx+1):
#         if (x,y) in dug:
#             print('#', end='')
#         else:
#             print('.', end='')
#     print()

def solve():
    inside = set()
    outside = set()
    for x in range(minx,maxx+1):
        for y in range(miny,maxy+1):
            start = (x,y)
            if start not in inside and start not in outside and start not in dug:
                # BFS flood fill
                queue = deque([(x,y)])
                visited = set()
                is_outside = False
                while queue:
                    if is_outside: break
                    pos = queue.popleft()
                    if pos in outside:
                        is_outside = True
                        outside |= visited
                        break
                    elif pos in inside:
                        inside |= visited
                        break
                    elif pos not in visited:
                        visited.add(pos)
                        for axis in range(2):
                            if is_outside: break
                            for offset in range(-1, 2):
                                if offset != 0:
                                    adj = list(pos)
                                    adj[axis] = offset + pos[axis]
                                    adj = tuple(adj)
                                    if adj not in dug:
                                        if adj[0] > maxx or adj[0] < minx or adj[1] > maxy or adj[1] < miny:
                                            # outside
                                            outside |= visited
                                            is_outside = True
                                            break
                                        else:
                                            queue.append(adj)
                if not is_outside: 
                    inside |= visited
    
    return len(inside | dug)

# for y in range(miny,maxy+1):
#     for x in range(minx,maxx+1):
#         if (x,y) in inside:
#             print('#', end='')
#         else:
#             print('.', end='')
#     print()

# for y in range(miny,maxy+1):
#     for x in range(minx,maxx+1):
#         if (x,y) in outside:
#             print('#', end='')
#         else:
#             print('.', end='')
#     print()

p1 = solve()
print('Part 1:', p1)
# puzzle.answer_a = p1

dug = set([(0,0)])

ds = {'0':(1,0), '2':(-1,0), '3':(0,-1), '1':(0,1)}

minx, maxx, miny, maxy = 0, 0, 0, 0
verticals = set() # (x, y1, y2)
x, y = 0, 0
for line in data.splitlines():
    d, n, color = line.split()
    n = int(color[2:7], 16)
    dx, dy = ds[color[-2]]
    for _ in range(n):
        x += dx
        y += dy
        minx = min(minx, x)
        maxx = max(maxx, x)
        miny = min(miny, y)
        maxy = max(maxy, y)
        dug.add((x,y))

p2 = solve()
print('Part 2:', p2)
puzzle.answer_b = p2