from aocd.models import Puzzle
from dotenv import load_dotenv
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=2)
data = puzzle.input_data
# with open("2023/test2.txt") as file:
#     data = file.read()

colors = {'red':12, 'green':13, 'blue':14}

p1 = 0
for line in data.splitlines():
    id, game = line[5:].split(': ')
    games = game.split('; ')
    good = True
    for game in games:
        cubes = game.split(', ')
        for cube in cubes:
            n, color = cube.split(' ')
            if int(n) > colors[color]:
                good = False
                break
        if not good:
            break
    if good:
        p1 += int(id)

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0
for line in data.splitlines():
    id, game = line[5:].split(': ')
    games = game.split('; ')
    mins = {'red':0, 'green':0, 'blue':0}
    for game in games:
        cubes = game.split(', ')
        for cube in cubes:
            n, color = cube.split(' ')
            mins[color] = max(mins[color], int(n))
    
    p2 += math.prod(mins.values())
            
print('Part 2:', p2)
# puzzle.answer_b = p2


