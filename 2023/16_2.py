from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
import sys
from collections import deque

sys.setrecursionlimit(10000)
load_dotenv()
puzzle = Puzzle(year=2023, day=16)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

grid = utils.string_to_grid(data)

def valid(coord):
    if coord[0] in range(len(grid[0])) and coord[1] in range(len(grid)):
        return True
    return False

def west(coord):
    x,y = coord
    c = grid[y][x]
    if c == '.' or c == '-':
        return [((x-1,y), 'w')]
    elif c == '/':
        return [((x,y+1), 's')]
    elif c == '\\':
        return [((x,y-1), 'n')]
    elif c == '|':
        return [((x,y+1), 's'), ((x,y-1), 'n')]

def east(coord):
    x,y = coord
    c = grid[y][x]
    if c == '.' or c == '-':
        return [((x+1,y), 'e')]
    elif c == '/':
        return [((x,y-1), 'n')]
    elif c == '\\':
        return [((x,y+1), 's')]
    elif c == '|':
        return [((x,y+1), 's'), ((x,y-1), 'n')]

def south(coord):
    x,y = coord
    c = grid[y][x]
    if c == '.' or c == '|':
        return [((x,y+1), 's')]
    elif c == '/':
        return [((x-1,y), 'w')]
    elif c == '\\':
        return [((x+1,y), 'e')]
    elif c == '-':
        return [((x+1,y), 'e'), ((x-1,y), 'w')]

def north(coord):
    x,y = coord
    c = grid[y][x]
    if c == '.' or c == '|':
        return [((x,y-1), 'n')]
    elif c == '/':
        return [((x+1,y), 'e')]
    elif c == '\\':
        return [((x-1,y), 'w')]
    elif c == '-':
        return [((x+1,y), 'e'), ((x-1,y), 'w')]

# BFS
def solve(start):
    energized = set()
    visited = set()
    queue = deque([start])
    while queue:
        next = queue.popleft()
        if next not in visited:
            visited.add(next)
            if valid(next[0]):
                energized.add(next[0])
                if next[1] == 'n':
                    queue += north(next[0])
                elif next[1] == 's':
                    queue += south(next[0])
                elif next[1] == 'e':
                    queue += east(next[0])
                elif next[1] == 'w':
                    queue += west(next[0])
    return len(energized)

p1 = solve(((0,0), 'e'))
print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = 0

# L/R
for y in range(len(grid)):
    p2 = max(p2, solve(((0,y), 'e')))
    p2 = max(p2, solve(((len(grid[0])-1,y), 'w')))

# U/D
for x in range(len(grid[0])):
    p2 = max(p2, solve(((x,0), 's')))
    p2 = max(p2, solve(((x,len(grid)-1), 'n')))

print('Part 2:', p2)
# puzzle.answer_b = p2