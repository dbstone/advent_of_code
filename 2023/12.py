from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cache

load_dotenv()
puzzle = Puzzle(year=2023, day=12)
data = puzzle.input_data

@cache
def get_num_valid_configs(record, string):
    if not record:
        return 0
    string = string.split(',')
    l = int(string[0])
    valid = 0
    end = len(record)-l+1
    if '#' in record:
        end = min(end, record.find('#')+1)
    for i in range(end):
        if '.' not in record[i:i+l]:
            if i+l >= len(record) or record[i+l] != '#':
                if len(string) == 1:
                    if len(record) == i+l or '#' not in record[i+l:]:
                        valid += 1
                elif i+l+1 < len(record):
                    valid += get_num_valid_configs(record[i+l+1:], ','.join(string[1:]))
    return valid

p1 = 0
p2 = 0
for line in data.splitlines():
    record, string = line.split()
    p1 += get_num_valid_configs(record, string) 
    record = '?'.join([record]*5)
    string = ','.join([string]*5)
    p2 += get_num_valid_configs(record, string) 

print('Part 1:', p1)
# puzzle.answer_a = p1

print('Part 2:', p2)
# puzzle.answer_b = p2