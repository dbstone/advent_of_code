from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
from copy import deepcopy

load_dotenv()
puzzle = Puzzle(year=2023, day=19)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

ws, ps = data.split('\n\n')

# parse workflows
workflows = {}
for w in ws.splitlines():
    a, b = w.find('{'), w.find('}')
    name = w[:a]
    rs = w[a+1:b]
    rules = []
    for r in rs.split(','):
        if ':' in r:
            condition, outcome = r.split(':')
            if '<' in condition:
                ca, cn = condition.split('<')
                rules.append(((ca, '<', int(cn)), outcome))
            elif '>' in condition:
                ca, cn = condition.split('>')
                rules.append(((ca, '>', int(cn)), outcome))
            else:
                print('fuck')
        else:
            rules.append((None, r))
    workflows[name] = rules

parts = []
for p in ps.splitlines():
    ss = p[1:-1].split(',')
    scores = {}
    for s in ss:
        label, score = s.split('=')
        scores[label] = int(score)
    parts.append(scores)

p1 = 0
for part in parts:
    next = 'in'
    while True:
        if next == 'A':
            p1 += part['x'] + part['m'] + part['a'] + part['s']
            break
        elif next == 'R':
            break
        wf = workflows[next]
        for rule in wf:
            condition, outcome = rule
            if condition:
                a,b,c = condition
                if b == '>':
                    if part[a] > c:
                        next = outcome
                        break
                elif b == '<':
                    if part[a] < c:
                        next = outcome
                        break
                else:
                    print('fuck')
            else:
                next = outcome
                break
                
# print('Part 1:', p1)
# puzzle.answer_a = p1

l,h = 1, 4000
default = [range(l,h+1)]

wfreqs = {}
def get_reqs(wf):
    if wf in wfreqs:
        return wfreqs[wf]
    wf = workflows[wf]
    base = None
    if len(wf) == 1:
        return get_reqs(wf[0][-1])
    if wf[-1][-1] == 'A':
        base = {'x':default,'m':default,'a':default,'s':default}
    elif wf[-1][-1] == 'R':
        base = {'x':[],'m':[],'a':[],'s':[]}
    else:
        base = get_reqs(wf[-1][-1])

    for con, out in wf[:-1]:
        a,b,c = con
        r = None
        if b == '<':
            r = range(l,c)
        elif b == '>':
            r = range(c+1,h+1)
        if out == 'A':
            base[a].append(r)
        elif out == 'R':
            # subtract range from existing
            new = []
            for r2 in base[a]:
                if r2.start < r.start and r.stop < r2.stop:
                    # split into two
                    new.append(range(r2.start,r.start))
                    new.append(range(r.stop,r2.stop))
                elif r2.start < r.start:
                    new.append(range(r2.start,r.start))
                elif r.stop < r2.stop:
                    new.append(range(r.stop,r2.stop))
                elif r.start > r2.stop or r.stop < r2.start:
                    new.append(r2)
            base[a] = new
        else:
            # intersection
            other = get_reqs(out)
            new = []
            for r2 in other[a]:
                if r.start < r2.stop and r.stop > r2.start: 
                    new.append(range(max(r.start, r2.start), min(r.stop, r2.stop)))
            base[a] += new
    return base

# for wf in workflows.keys():
#     if wf not in wfreqs:
#         wfreqs[wf] = get_reqs(wf)

def consolidate(rs):
    orig = rs
    merged = True
    while merged:
        new = []
        merged = False
        for r in orig:
            done = False
            for r2 in new:
                if r.start < r2.stop and r.stop > r2.start:
                    # union
                    new.remove(r2)
                    new.append(range(min(r.start,r2.start), max(r.stop,r2.stop)))
                    merged = True
                    done = True
                    break
            if not done:
                new.append(r)
        orig = new
    return orig

def intersect(ra, rb):
    return range(max(ra.start, rb.start), min(ra.stop, rb.stop))

def combs(rs):
    c = 1
    for r in rs.values():
        c *= r.stop - r.start if r.stop > r.start else 0
    return c

def get_valid_reqs(wfid, rs):
    As = 0
    for con, out in workflows[wfid]:
        rs2 = deepcopy(rs)
        if con:
            a,b,c = con
            r = rs2[a]
            if b == '<':
                rs2[a] = intersect(r, range(l,c))
                rs[a] = intersect(r, range(c,h+1))
            elif b == '>':
                rs2[a] = intersect(r, range(c+1,h+1))
                rs[a] = intersect(r, range(l,c+1))
            if out == 'A':
                As += combs(rs2)
            elif out != 'R':
                As += get_valid_reqs(out, rs2)
        else:
            if out == 'A':
                return combs(rs) + As
            elif out == 'R':
                return As
            else:
                return get_valid_reqs(out, rs) + As
                
default = range(l,h+1)
reqs = get_valid_reqs('in', {'x':default,'m':default,'a':default,'s':default})
print('Part 2:', reqs)
puzzle.answer_b = reqs