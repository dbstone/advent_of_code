from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math

load_dotenv()
puzzle = Puzzle(year=2023, day=20)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

class Module:
    def __init__(self,dests) -> None:
        self.dests = dests
    
    def recieve(self, sender, pulse):
        pass

class FlipFlop(Module):
    def __init__(self,dests):
        Module.__init__(self,dests)
        self.on = False

    def recieve(self, sender, pulse):
        if not pulse:
            self.on = not self.on
            return [(dest, self.on) for dest in self.dests]
        return []

class Conjunction(Module):
    def __init__(self,dests):
        Module.__init__(self,dests)
        self.inputs = {}
    
    def set_inputs(self, inputs):
        for i in inputs:
            self.inputs[i] = False
    
    def recieve(self, sender, pulse):
        self.inputs[sender] = pulse
        if all(self.inputs.values()):
            return [(dest, False) for dest in self.dests]
        return [(dest, True) for dest in self.dests]
    
class Broadcaster(Module):
    def recieve(self, sender, pulse):
        return [(dest, pulse) for dest in self.dests]

def setup():
    modules = {}
    conjunctions = {}
    for line in data.splitlines():
        name, dests = line.split(' -> ')
        dests = dests.split(', ')
        if name == 'broadcaster':
            module = Broadcaster(dests)
            modules[name] = module
        elif name.startswith('%'):
            module = FlipFlop(dests)
            modules[name[1:]] = module
        elif name.startswith('&'):
            module = Conjunction(dests)
            conjunctions[name[1:]] = module
            modules[name[1:]] = module

    for kc,vc in conjunctions.items():
        inputs = set()
        for km,vm in modules.items():
            if kc in vm.dests:
                inputs.add(km)
        vc.set_inputs(inputs)
    
    return modules

def solve(is_p2):
    modules = setup()
    high = 0
    low = 0
    last = 0
    for step in range(10000):
        to_send = [('button', False, 'broadcaster')]
        while to_send:
            next = []
            for p in to_send:
                if is_p2 and p[2] == 'rx':
                    if not p[1]:
                        return step
                # print(p)
                if p[1]:
                    high += 1
                else:
                    low += 1
                if p[2] in modules:
                    if p[2] == 'dd':
                        # print(*(i for i in modules[p[2]].inputs.values()))
                        inputs = modules[p[2]].inputs
                        if inputs['jq']:
                            print(step-last)
                            last = step
                    outputs = modules[p[2]].recieve(p[0], p[1])
                    for o in outputs:
                        next.append((p[2], o[1], o[0]))
            to_send = next
    return high * low

p1 = solve(False)
print('Part 1:', p1)
# puzzle.answer_a = p1

# p2 = solve(True)
# did this part manually by checking periods for each of dd's inputs (dd is the module that pulses rx)
p2 = math.lcm(3851, 4013, 4001, 3911)
print('Part 2:', p2)
puzzle.answer_b = p2