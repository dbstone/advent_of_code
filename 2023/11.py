from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import functools
import ast
from itertools import combinations
import numpy

load_dotenv()
puzzle = Puzzle(year=2023, day=11)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

data = numpy.array([list(s) for s in data.splitlines()])

yempty = set(y for y,s in enumerate(data) if all(c == '.' for c in s))
xempty = set(x for x,s in enumerate(data.transpose()) if all(c == '.' for c in s))

galaxies = set()
for y, x in numpy.ndindex(data.shape):
    if data[y][x] == '#':
        galaxies.add((x,y))

def manhattan_distance(a, b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])

def dist(a, b, expand_factor):
    d = manhattan_distance(a,b)
    for y in range(min(a[1],b[1]),max(a[1],b[1])):
        if y in yempty:
            d += expand_factor-1
    for x in range(min(a[0],b[0]),max(a[0],b[0])):
        if x in xempty:
            d += expand_factor-1
    return d

p1 = sum(dist(a,b,2) for a,b in combinations(galaxies, 2))
print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = sum(dist(a,b,1000000) for a,b in combinations(galaxies, 2))
print('Part 2:', p2)
# puzzle.answer_b = p2