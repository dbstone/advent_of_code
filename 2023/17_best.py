from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import defaultdict
from heapq import heappop, heappush

load_dotenv()
puzzle = Puzzle(year=2023, day=17)
data = puzzle.input_data

cavern = [[int(c) for c in line] for line in data.splitlines()]

def greedy_dijkstra(cavern, is_part2):
    dists = defaultdict(lambda: float('inf'))
    start = ((0,0), (0,0), 0) # (x, y), (dx,dy), heat
    dists[start] = 0
    openSet = []
    heappush(openSet, (0, start))

    while openSet:
        current = heappop(openSet)
        current = current[1]
        x, y = current[0]
        dx, dy = current[1]
        heat = current[2]

        ds = []
        if (x,y) == (0,0):
            ds += [(1,0), (0,1)]
        elif is_part2:
            if heat >= 4:
                ds += [(-dy,-dx), (dy,dx)]
            if heat < 10:
                ds.append((dx,dy))
        else:
            ds += [(-dy,-dx), (dy,dx)]
            if heat < 3:
                ds.append((dx,dy))
       
        for d in ds:
            ddx, ddy = d
            nx, ny = x+ddx, y+ddy
            if nx in range(len(cavern[0])) and ny in range(len(cavern)):
                n = ((nx, ny), (ddx, ddy), heat+1 if (ddx,ddy) == (dx,dy) else 1)
                dist = dists[current] + cavern[ny][nx]
                if nx == len(cavern[0])-1 and ny == len(cavern)-1:
                    return dist
                if dist < dists[n]:
                    dists[n] = dist
                    heappush(openSet, (dist, n))
    return dists

p1 = greedy_dijkstra(cavern, False)
print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = greedy_dijkstra(cavern, True)
print('Part 2:', p2)
# puzzle.answer_b = p2