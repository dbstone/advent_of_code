from aocd.models import Puzzle
from dotenv import load_dotenv
import re

load_dotenv()
puzzle = Puzzle(year=2023, day=1)
data = puzzle.input_data
# with open("2023/test2.txt") as file:
#     data = file.read()

p1 = 0

for line in data.splitlines():
    first = 0
    last = 0
    for c in line:
        if c.isnumeric():
            first = int(c)

    for c in reversed(line):
        if c.isnumeric():
            last = int(c)

    val = int(str(first) + str(last))
    p1 += val

print('Part 1:', p1)
# puzzle.answer_a = p1

words = {'zero':0,
 'one':1, 
 'two':2, 
 'three':3, 
 'four':4, 
 'five':5, 
 'six':6, 
 'seven':7, 
 'eight':8, 
 'nine':9}

p2 = 0

for line in data.splitlines():
    first_i = len(line)
    last_i = 0

    first = 0
    last = 0

    firstwordnums = []
    for word in words:
        i = line.find(word)
        if i >= 0:
            firstwordnums.append((i, words[word]))
    
    lastwordnums = []
    for word in words:
        i = line.rfind(word)
        if i >= 0:
            lastwordnums.append((i, words[word]))
    # print(wordnums)

    for i in range(0, len(line)):
        c = line[i]
        if c.isnumeric():
            first = int(c)
            first_i = i
            break

    for i in range(len(line)-1, -1, -1):
        c = line[i]
        if c.isnumeric():
            last = int(c)
            last_i = i
            break
    
    for num in firstwordnums:
        if num[0] < first_i:
            first = num[1]
            first_i = num[0]
    for num in lastwordnums:
        if num[0] > last_i:
            last = num[1]
            last_i = num[0]

    # print(line)
    # print(first, last)
    val = int(str(first) + str(last))
    p2 += val

print('Part 2:', p2)
puzzle.answer_b = p2


