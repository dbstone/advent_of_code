from aocd.models import Puzzle
from dotenv import load_dotenv
import math
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2023, day=10)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

neighbors = {'|':[(0,-1), (0,1)],
             '-':[(-1, 0), (1, 0)], 
             'L':[(0, -1), (1, 0)],
             'J':[(0, -1), (-1, 0)], 
             '7':[(-1, 0), (0, 1)], 
             'F':[(1, 0), (0, 1)]}

pipemap = data.splitlines()

start = None
for y in range(len(pipemap)):
    if start:
        break
    for x in range(len(pipemap[0])):
        if pipemap[y][x] == 'S':
            start = (x, y)
            break

# last = start
# curr = (start[0]-1, start[1])
# steps = 1
# while curr != start:
#     x, y = curr
#     for neighbor in neighbors[pipemap[y][x]]:
#         x2 = x+neighbor[0]
#         y2 = y+neighbor[1]
#         if (x2, y2) != last:
#             last = curr
#             curr = (x2, y2)
#             break
#     steps += 1

# p1 = steps // 2
# print('Part 1:', p1)
# puzzle.answer_a = p1

W = len(pipemap[0])
H = len(pipemap)
adj = [[0 for _ in range(W)] for _ in range(H)]

def set_adj(last, curr):
    x1, y1 = last
    x2, y2 = curr
    dx = x2-x2
    dy = y2-y1
    if dx != 0:
        # right/left
        for y in range(0, y2):
            adj[y][x2] -= dx
        for y in range(y2+1, H):
            adj[y][x2] += dx
    else:
        # up/down
        for x in range(0, x2):
            adj[y2][x] -= dy
        for x in range(x2+1, W):
            adj[y2][x] += dy

magnified = [['.' for _ in range(W*4)] for _ in range(H*4)]
path = set()
path.add(start)
last = start
# curr = (start[0]-1, start[1])
curr = (start[0], start[1]-1)
# mag_curr = (curr[0]*2, curr[1]*2)
mag_curr = (W+2*curr[0], H+2*curr[1])
magnified[mag_curr[1]][mag_curr[0]] = '0'
magnified[mag_curr[1]+1][mag_curr[0]] = '0'
big_path = set()
big_path.add(mag_curr)
big_path.add((mag_curr[0], mag_curr[1]+1))
set_adj(start, curr)
while curr != start:
    path.add(curr)
    x, y = curr
    for neighbor in neighbors[pipemap[y][x]]:
        x2 = x+neighbor[0]
        y2 = y+neighbor[1]
        if (x2, y2) != last:
            last = curr
            curr = (x2, y2)
            magnified[H+2*y2][W+2*x2] = '0'
            magnified[H+2*y2-neighbor[1]][W+2*x2-neighbor[0]] = '0'
            big_path.add((W+2*x2, H+2*y2))
            big_path.add((W+2*x2-neighbor[0], H+2*y2-neighbor[1]))
            set_adj(last, curr)
            break


# BFS flood fill
queue = deque([(0,0)])
visited = set()
while queue:
    pos = queue.popleft()
    if pos not in visited and pos not in big_path:
        visited.add(pos)
        for axis in range(2):
            for offset in range(-1, 2):
                if offset != 0:
                    adj = list(pos)
                    adj[axis] = offset + pos[axis]
                    adj = tuple(adj)
                    if adj[0] >= 0 and adj[0] < len(magnified[0]) and adj[1] >= 0 and adj[1] < len(magnified):
                        queue.append(adj)

# print(len(visited))

p2 = 0
for y in range(len(magnified)):
    for x in range(len(magnified[0])):
        if y % 2 == 0 and x % 2 == 0 and (x,y) not in visited and (x,y) not in big_path:
            p2 += 1
            # print('x', end='')
    #   print(magnified[y][x], end='')
        # elif (x,y) in big_path:
        #     print('0', end='')
        # # elif (x,y) in visited:
        # #     print('x', end='')
        # else:
        #     print('.', end='')
    # print()
        # print('0' if (x,y) in big_path else '.', end='')

# for y in range(H):
#     for x in range(W):
#         print('*', end='') if (x,y) in path else print('.', end='')
#     print()
# for y in range(H):
#     for x in range(W):
#       print(pipemap[y][x] if (x,y) in path else abs(adj[y][x]), end='')
#     print()

# print()
# for y in range(H):
#     for x in range(W):
#       print(pipemap[y][x] if (x,y) in path or adj[y][x] == 0 else '*', end='')
#     print()

# p2 = 0
# for y in range(H):
#     for x in range(W):
#         if (x,y) not in path and adj[y][x] != 0:
#             p2 += 1

print('Part 2:', p2)
puzzle.answer_b = p2

# p2 = 0
# W = len(pipemap[0])
# H = len(pipemap)
# enclosed = set()
# for y in range(H):
#     for x in range(W):
#         coord = (x, y)
#         if coord not in path:

#             left = 0
#             for x2 in range(0, x):
#                 if (x2, y) in path and pipemap[y][x2] in neighbors and pipemap[y][x2] != '-':
#                     left += 1
#             if left % 2 == 1:
#                 continue

#             right = 0
#             for x2 in range(x+1, W):
#                 if (x2, y) in path and pipemap[y][x2] in neighbors and pipemap[y][x2] != '-':
#                     right += 1
#             if right % 2 == 1:
#                 continue

#             top = 0
#             for y2 in range(0, y):
#                 if (x, y2) in path and pipemap[y2][x] in neighbors and pipemap[y2][x] != '|':
#                     top += 1
#             if top % 2 == 1:
#                 continue
            
#             bot = 0
#             for y2 in range(y+1, H):
#                 if (x, y2) in path and pipemap[y2][x] in neighbors and pipemap[y2][x] != '|':
#                     bot += 1
#             if bot % 2 == 1:
#                 continue
            
#             enclosed.add(coord)
#             p2 += 1

