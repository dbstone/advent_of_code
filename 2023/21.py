from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2023, day=21)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

grid = utils.string_to_grid(data)
start = None
for y in range(len(grid)):
    if start:
        break
    for x in range(len(grid[y])):
        if grid[y][x] == 'S':
            start = (x,y)
            break

def valid(x,y):
    if x >= 0 and x < len(grid[0]):
        if y >= 0 and y < len(grid):
            if grid[y][x] != '#':
                return True
    return False

next = set([start])
steps = 0
while steps < 64:
    queue = next
    next = set()
    for p in queue:
        for axis in range(2):
            for offset in range(-1, 2):
                if offset != 0:
                    adj = list(p)
                    adj[axis] = offset + p[axis]
                    adj = tuple(adj)
                    if valid(*adj):
                        next.add(adj)
    steps += 1

p1 = len(next)
print('Part 1:', p1)
puzzle.answer_a = p1

# p2 = None
# print('Part 2:', p2)
# puzzle.answer_b = p2