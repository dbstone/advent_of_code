from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
from collections import defaultdict

load_dotenv()
puzzle = Puzzle(year=2023, day=17)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

cavern = [[int(c) for c in line] for line in data.splitlines()]

def get_nearest(dists, closedSet, openSet):
    nearest = None
    min_dist = float('inf')

    for u in openSet:
        if u not in closedSet and dists[u] < min_dist:
            nearest = u
            min_dist = dists[u]
    
    return nearest

# modified dijkstra's
def dijkstra(cavern, is_part2=False):
    dists = defaultdict(lambda: float('inf'))
    start = ((0,0), (0,0), 0) # (x, y), (dx,dy), heat
    dists[start] = 0 

    closedSet = set()
    openSet = set()
    openSet.add(start)

    # bounds check
    def is_valid_point(point):
        x,y = point
        multiplier = 5 if is_part2 else 1
        if x >= 0 and x < len(cavern[0])*multiplier:
            if y >= 0 and y < len(cavern)*multiplier:
                return True
        return False

    N = len(cavern) * len(cavern[0])
    if is_part2:
        N *= 25
    
    current = None

    # while len(closedSet) < N:
    while True:
        current = get_nearest(dists, closedSet, openSet)
        closedSet.add(current)
        openSet.remove(current) # TODO: Use a heap for this instead (python heapq library)
        multiplier = 5 if is_part2 else 1
        x, y = current[0]
        if (x,y) == (len(cavern[0])*multiplier-1, len(cavern)*multiplier-1):
            return dists
        dx, dy = current[1]
        heat = current[2]
        ds = [(-dy,-dx), (dy,dx)]
        if heat < 3:
            ds.append((dx,dy))
        if (x,y) == (0,0):
            ds = [(1,0), (0,1)]
        for d in ds:
            ddx, ddy = d
            nx, ny = x+ddx, y+ddy
            n = ((nx, ny), (ddx, ddy), heat+1 if (ddx,ddy) == (dx,dy) else 1)
            if is_valid_point((nx, ny)) and n not in closedSet:
                # nx_offset = nx // len(cavern[0])
                # ny_offset = ny // len(cavern)
                # nv = cavern[ny%len(cavern)][nx%len(cavern[0])]
                # nv = (nv + nx_offset + ny_offset - 1) % 9 + 1
                dist = dists[current] + cavern[ny][nx]
                if dist < dists[n]:
                    dists[n] = dist
                    openSet.add(n)
        # if len(closedSet) % 1000 == 0:
        #     print(len(closedSet))

    return dists

dists = dijkstra(cavern, False)
p1 = float('inf')
for d,v in dists.items():
    if d[0][0] == len(cavern[0])-1 and d[0][1] == len(cavern)-1:
        p1 = min(p1, v)
# ends = [dists[d] for d in dists.keys() if d[0][0] == len(cavern[0])-1 and d[0][1] == len(cavern)-1]
# for end in ends:
#     p1 = min(p1, end)
# p1 = dists[(len(cavern[0])-1, len(cavern)-1)]
print('Part 1:', p1)
# puzzle.answer_a = p1

# p2 = None
# print('Part 2:', p2)
# puzzle.answer_b = p2