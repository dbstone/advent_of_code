from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import time

load_dotenv()
puzzle = Puzzle(year=2023, day=4)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

data = data.splitlines()
p1 = 0
cardcounts = [1] * len(data)
for i,line in enumerate(data):
    _, card = line.split(': ')
    winners, ours = (set(s.split()) for s in card.split(' | '))
    wins = len(winners & ours)
    p1 += int(2 ** (wins-1))
    for j in range(wins):
        cardcounts[j+1+i] += cardcounts[i]

print('Part 1:', p1)
# puzzle.answer_a = p1

p2 = sum(cardcounts)
print('Part 2:', p2)
# puzzle.answer_b = p2
