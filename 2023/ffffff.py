from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
from collections import defaultdict, deque
from heapq import heappop, heappush

load_dotenv()
puzzle = Puzzle(year=2023, day=17)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

cavern = [[int(c) for c in line] for line in data.splitlines()]

def greedy_dijkstra(cavern, is_part2=False):
    dists = defaultdict(lambda: float('inf'))
    start = ((0,0), (0,0), 0) # (x, y), (dx,dy), heat
    dists[start] = 0 
    openSet = []
    heappush(openSet, (0, start))

    current = None

    # bounds check
    def valid(x,y):
        if x >= 0 and x < len(cavern[0]):
            if y >= 0 and y < len(cavern):
                return True
        return False

    while openSet:
        current = heappop(openSet)
        dist, current = current
        x, y = current[0]
        if (x,y) == (len(cavern[0])-1, len(cavern)-1):
            return dist
        dx, dy = current[1]
        heat = current[2]
        ds = []
        if heat >= 4:
            ds += [(-dy,-dx), (dy,dx)]
        if heat < 10:
            ds.append((dx,dy))
        if (x,y) == (0,0):
            ds = [(1,0), (0,1)]
        for d in ds:
            ddx, ddy = d
            nx, ny = x+ddx, y+ddy
            n = ((nx, ny), (ddx, ddy), heat+1 if (ddx,ddy) == (dx,dy) else 1)
            if valid(nx, ny):
                new_dist = dist + cavern[ny][nx]
                if new_dist < dists[n]:
                    heappush(openSet, (dist, n))

    return dists

dists = greedy_dijkstra(cavern, False)
p2 = float('inf')
for d,v in dists.items():
    if d[0][0] == len(cavern[0])-1 and d[0][1] == len(cavern)-1:
        if v < p2:
            p2 = v

print('Part 2:', p2)