from aocd.models import Puzzle
from dotenv import load_dotenv
import utils
import math
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2023, day=18)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

dug = set([(0,0)])

ds = {'R':(1,0), 'L':(-1,0), 'U':(0,-1), 'D':(0,1)}

x, y = 0, 0
for line in data.splitlines():
    d, n, color = line.split()
    n = int(n)
    dx, dy = ds[d]
    for _ in range(n):
        x += dx
        y += dy
        dug.add((x,y))

minx = min(x for x,y in dug)
miny = min(y for x,y in dug)
maxx = max(x for x,y in dug)
maxy = max(y for x,y in dug)

# for y in range(miny,maxy+1):
#     for x in range(minx,maxx+1):
#         if (x,y) in dug:
#             print('#', end='')
#         else:
#             print('.', end='')
#     print()

def solve():
    inside = set()
    outside = set()
    for x in range(minx,maxx+1):
        for y in range(miny,maxy+1):
            start = (x,y)
            if start not in inside and start not in outside and start not in dug:
                # BFS flood fill
                queue = deque([(x,y)])
                visited = set()
                is_outside = False
                while queue:
                    if is_outside: break
                    pos = queue.popleft()
                    if pos in outside:
                        is_outside = True
                        outside |= visited
                        break
                    elif pos in inside:
                        inside |= visited
                        break
                    elif pos not in visited:
                        visited.add(pos)
                        for axis in range(2):
                            if is_outside: break
                            for offset in range(-1, 2):
                                if offset != 0:
                                    adj = list(pos)
                                    adj[axis] = offset + pos[axis]
                                    adj = tuple(adj)
                                    if adj not in dug:
                                        if adj[0] > maxx or adj[0] < minx or adj[1] > maxy or adj[1] < miny:
                                            # outside
                                            outside |= visited
                                            is_outside = True
                                            break
                                        else:
                                            queue.append(adj)
                if not is_outside: 
                    inside |= visited
    
    return len(inside | dug)

# for y in range(miny,maxy+1):
#     for x in range(minx,maxx+1):
#         if (x,y) in inside:
#             print('#', end='')
#         else:
#             print('.', end='')
#     print()

# for y in range(miny,maxy+1):
#     for x in range(minx,maxx+1):
#         if (x,y) in outside:
#             print('#', end='')
#         else:
#             print('.', end='')
#     print()

# p1 = solve()
# print('Part 1:', p1)
# puzzle.answer_a = p1

ds = {'0':(1,0), '2':(-1,0), '3':(0,-1), '1':(0,1)}

verts = []
x, y = 0, 0
perimeter = 0
for line in data.splitlines():
    d, n, color = line.split()

    # part 2
    n = int(color[2:7], 16)
    dx, dy = ds[color[-2]]

    # part 1
    # n = int(n)
    # dx, dy = ds[d]

    if dx:
        # x2 = x + dx*n
        # if x2 > x:
        #     x = x2
        # else:
        #     x = x2
        x += dx*n
        # if dx > 0:
        #     x += 1
    else:
        y += dy*n
        # if dy > 0:
        #     y += 1

    perimeter += n
    
    verts.append((x,y))

# print('perimeter:', perimeter)
diff = perimeter // 2 + 1
# print('diff:', diff)

import numpy as np
def shoelace(x_y):
    x_y = np.array(x_y)
    x_y = x_y.reshape(-1,2)

    x = x_y[:,0]
    y = x_y[:,1]

    S1 = np.sum(x*np.roll(y,-1))
    S2 = np.sum(y*np.roll(x,-1))

    area = .5*np.absolute(S1 - S2)

    return area

def area_by_shoelace(x, y):
    "Assumes x,y points go around the polygon in one direction"
    return abs( sum(i * j for i, j in zip(x,             y[1:] + y[:1]))
               -sum(i * j for i, j in zip(x[1:] + x[:1], y            ))) / 2
x, y = zip(*verts)
sl = area_by_shoelace(x,y)
# sl = shoelace(verts)
# print(sl)

# print(sl)
# print(p1-sl)

# minx = min(x for x,y in verts)
# miny = min(y for x,y in verts)
# maxx = max(x for x,y in verts)
# maxy = max(y for x,y in verts)

# height = maxy - miny + 1

# p2 = sl + height * 2

p2 = int(sl + diff)
print('Part 2:', p2)
puzzle.answer_b = p2