from aocd.models import Puzzle
from dotenv import load_dotenv
import math
import functools
import ast

load_dotenv()
puzzle = Puzzle(year=2023, day=8)
data = puzzle.input_data
# with open("2023/test.txt") as file:
#     data = file.read()

directions, temp = data.split('\n\n')

nodes = {}
for s in temp.splitlines():
    n, lr = s.split(' = ')
    l, r = lr[1:-1].split(', ')
    nodes[n] = (l,r)

def steps(start, p2=False):
    curr = start
    i = 0
    while curr[2] != 'Z' if p2 else curr != 'ZZZ':
        d = directions[i%len(directions)]
        curr = nodes[curr][0 if d == 'L' else 1]
        i += 1
    return i

p1 = steps('AAA')
print('Part 1:', p1)
puzzle.answer_a = p1

p2 = p1
currs = [i for i in nodes.keys() if i[2] == 'A' and i != 'AAA']
for curr in currs:
    p2 = math.lcm(p2, steps(curr, True))

print('Part 2:', p2)
puzzle.answer_b = p2