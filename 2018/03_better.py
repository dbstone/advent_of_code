claims = []

with open("2018/input/03.txt") as file:
    for line in file:
        coord = line[line.find('@')+1:line.find(':')]
        dims = line[line.find(':')+1:-1]
        claim = {}
        claim['x'], claim['y'] = [int(i) for i in coord.split(',')]
        claim['w'], claim['h'] = [int(i) for i in dims.split('x')]
        claims.append(claim)

fabric = []
for _ in range(1000):
    fabric.append([set() for i in range(1000)])

for claim_id in range(len(claims)):
    claim = claims[claim_id]
    for x in range(claim['x'], claim['x']+claim['w']):
        for y in range(claim['y'], claim['y']+claim['h']):
            fabric[y][x].add(claim_id)

valid_claims = [True] * len(claims)

for x in range(1000):
    for y in range(1000):
        if len(fabric[y][x]) > 1:
            for claim_id in fabric[y][x]:
                valid_claims[claim_id] = False

print(valid_claims.index(True)+1)
