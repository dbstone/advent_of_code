class Node:
    def __init__(self, data):
        self.children = []
        self.metadata = []
        n_children = data[0]
        n_metadata = data[1]
        data = data[2:]
        self.size = 2 + n_metadata
        for i in range(n_children):
            self.children.append(Node(data))
            child_size = self.children[i].size
            self.size += child_size
            data = data[child_size:]
        
        for i in data[:n_metadata]:
            self.metadata.append(i)

        if self.children:
            self.value = 0
            for i in self.metadata:
                i -= 1
                if i not in range(len(self.children)):
                    continue
                self.value += self.children[i].value
        else:
            self.value = sum(self.metadata)
    
    def get_metadata_sum(self):
        metadata_sum = sum(self.metadata)
        for node in self.children:
            metadata_sum += node.get_metadata_sum()
        return metadata_sum

tree = None
with open("2018/input/08.txt") as file:
    data = [int(i) for i in file.read().split(' ')]
    tree = Node(data)
    print('Part 1:', tree.get_metadata_sum())
    print('Part 2:', tree.value)

