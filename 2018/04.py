log = {}

with open("2018/input/04.txt") as file:
    for line in file:
        timestamp = line[1:line.find(']')]
        timestamp = timestamp.replace('-', '')
        timestamp = timestamp.replace(':', '')
        timestamp = timestamp.replace(' ', '')
        log[int(timestamp)] = line[line.find('] ')+2:-1]

guards = {}
curr_guard = None
sleep_start_time = 0

for key in sorted(log):
    val = log[key]
    if val[0] == 'G':
        val = val[val.find('#')+1:]
        curr_guard = int(val[:val.find(' ')])
        is_awake = True
    elif val[0] == 'w':
        if curr_guard not in guards:
            guards[curr_guard] = [0] * 60
        for i in range(sleep_start_time, int(str(key)[-2:])):
            guards[curr_guard][i] += 1
    elif val[0] == 'f':
        sleep_start_time = int(str(key)[-2:])

guard_maxes = {}
for key, val in guards.items():
    guard_maxes[key] = sum(val)

inverse = [(value, key) for key, value in guard_maxes.items()]
sleepiest_guard_id = max(inverse)[1]
sleepiest_guard = guards[sleepiest_guard_id]
sleepiest_minute = sleepiest_guard.index(max(sleepiest_guard))
print('Part 1:', sleepiest_guard_id * sleepiest_minute)

guard = 0
minute = 0
slept = 0
for key, val in guards.items():
    for m in range(len(val)):
        if val[m] > slept:
            slept = val[m]
            minute = m
            guard = key

print('Part 2:', guard * minute)
