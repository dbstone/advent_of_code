polymer_orig = open("2018/input/05.txt").read()[:-1]

def react(p):
    start = 0
    done = False
    while not done:
        N = len(p)
        if start >= len(p)-2:
            # break
            # print('lksjdf')
            pass
        for i in range(start, len(p)-1):
            a = p[i]
            b = p[i+1]
            if a.isupper() != b.isupper():
                if a.lower() == b.lower():
                    if i == 0:
                        p = p[i+2:]
                    else:
                        p = p[:i] + p[i+2:]
                        start = i-1
                    break
            
            if i >= (len(p)-2):
                done = True
    return p

print('Part 1:', len(react(polymer_orig)))

min_N = len(polymer_orig)
for i in range(ord('a'), ord('z')+1):
    l = chr(i)
    polymer = polymer_orig.replace(l, "")
    polymer = polymer.replace(l.upper(), "")
    polymer = react(polymer)
    min_N = min(min_N, len(polymer))

print('Part 2:', min_N)
    