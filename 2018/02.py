from collections import Counter
from itertools import combinations

ids = [i[:-1] for i in open("2018/input/02.txt")]

id_counters = [Counter(i) for i in ids]

twos = 0
threes = 0
for idc in id_counters:
    has_two = False
    has_three = False
    for val in idc.values():
        has_two = has_two or val == 2
        has_three = has_three or val == 3
    if has_two: twos += 1
    if has_three: threes += 1

print(f'Part 1: {twos * threes}')

num_diffs = lambda x, y : sum(1 for a, b in zip(x, y) if a != b)
for x, y in combinations(ids, 2):
    if num_diffs(x, y) == 1:
        print('Part 2: ', end='')
        for a, b in zip(x, y):
            if a == b:
                print(a, end='')
        print('')
        break
