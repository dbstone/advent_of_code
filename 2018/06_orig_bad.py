coords = {}
    
with open("2018/input/06.txt") as file:
    for i, line in enumerate(file):
        a = line[:-1].split(', ')
        a = map(int, a)
        a = tuple(a)
        coords[a] = i

[print(coord) for coord in coords.items()]

# xs = [i[0] for i in coords]
# ys = [i[1] for i in coords]
# x_min = min(xs)
# y_min = min(ys)

# starmap = []
# for y in range(y_min, max(ys)+1):
#     starmap.append([-1] * (max(xs)+1 - x_min)) # bug maybe? +1

# for y in range(len(starmap)):
#     for x in range(len(starmap[0])):
#         coord = (x+x_min, y+y_min)
#         radius = 0
#         found = []
#         while not found:
#             for xx in range(coord[0]-radius, coord[0]+radius):
#                 top = (xx, coord[1]-radius) 
#                 bottom = (xx, coord[1]+radius) 
#                 if top in coords:
#                     found.append(coords[top])
#                 if bottom in coords:
#                     found.append(coords[bottom])
#             for yy in range(coord[1]-radius+1, coord[1]+radius-1):
#                 left = (xx, coord[0]-radius) 
#                 right = (xx, coord[0]+radius) 
#                 if left in coords:
#                     found.append(coords[left])
#                 if right in coords:
#                     found.append(coords[right])
            
#             radius += 1

#         if len(found) == 1:
#             starmap[y][x] = found[0]
#         else:
#             starmap[y][x] = -2 # tie!

# print(starmap)

# print(len(starmap[0]), len(starmap))


