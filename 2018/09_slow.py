n_players = 435
last_marble = 71184 * 100
# n_players = 10
# last_marble = 1618

scores = [0] * n_players
player = 0
circle = [0]
current = 0
for marble in range(1, last_marble+1):
    if marble % 23 == 0:
        current = (current-7+len(circle)) % len(circle)
        scores[player] += marble + circle.pop(current)
    else:
        current = (current + 2) % len(circle)
        circle.insert(current, marble)
    
    player = (player + 1) % n_players

print(max(scores))