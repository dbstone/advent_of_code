changes = [int(i) for i in open("2018/input/01.txt")]

freq = 0
for change in changes:
    freq += change
    
print(f'Part 1: {freq}')

freq = 0
freqs = set()
found = False
while not found:
    for change in changes:
        if freq in freqs:
            found = True
            print(f'Part 2: {freq}')
            break
        freqs.add(freq)
        freq += change
