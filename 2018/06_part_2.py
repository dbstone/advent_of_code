coords = {}
    
with open("2018/input/06.txt") as file:
    for i, line in enumerate(file):
        a = line[:-1].split(', ')
        a = map(int, a)
        a = tuple(a)
        coords[a] = i

xs = [i[0] for i in coords]
ys = [i[1] for i in coords]
x_min = min(xs)-1
y_min = min(ys)-1

dists = []
for y in range(y_min, max(ys)+1):
    dists.append([0] * (max(xs)+1 - x_min)) # bug maybe? +1

directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]

offset_coords = []
for coord in coords:
    offset_coords.append((coord[0] - x_min, coord[1] - y_min))

def manhattan_distance(a, b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])

y_range = range(len(dists))
x_range = range(len(dists[0]))
for y in y_range:
    for x in x_range:
        for coord in offset_coords:
            dists[y][x] += manhattan_distance(coord, (x, y))

size = 0

for y in y_range:
    for x in x_range:
        if dists[y][x] < 10000:
            size += 1

print(size)
