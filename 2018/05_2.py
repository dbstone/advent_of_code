polymer_orig = open("2018/input/05.txt").read()[:-1]

def react(p):
    start = 0
    done = False
    N = len(p)
    last_N = len(p)+1
    while N != last_N:
        for i in range(ord('a'), ord('z')+1):
            c1 = chr(i) + chr(i).upper()
            c2 = chr(i).upper() + chr(i)
            p = p.replace(c1, "")
            p = p.replace(c2, "")
        last_N = N
        N = len(p)
    return p

print('Part 1:', len(react(polymer_orig)))

min_N = len(polymer_orig)
for i in range(ord('a'), ord('z')+1):
    l = chr(i)
    polymer = polymer_orig.replace(l, "")
    polymer = polymer.replace(l.upper(), "")
    polymer = react(polymer)
    min_N = min(min_N, len(polymer))

print('Part 2:', min_N)
    