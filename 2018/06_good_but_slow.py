coords = {}
    
with open("2018/input/06.txt") as file:
    for i, line in enumerate(file):
        a = line[:-1].split(', ')
        a = map(int, a)
        a = tuple(a)
        coords[a] = i

# [print(coord) for coord in coords.items()]

xs = [i[0] for i in coords]
ys = [i[1] for i in coords]
x_min = min(xs)-1
y_min = min(ys)-1

distss = []
for _ in range(len(coords)):
    dists = []
    for y in range(y_min, max(ys)+1):
        dists.append([-1] * (max(xs)+1 - x_min)) # bug maybe? +1
    distss.append(dists)

directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]

offset_coords = []
for coord in coords:
    offset_coords.append((coord[0] - x_min, coord[1] - y_min))

# BFS traversal get distances from Coord to every other coord
def find_dists(coord, grid):
    next_queue = set()  # BFS queue containing tuples of type (origin_index, pos)

    next_queue.add(coord)
    grid[coord[1]][coord[0]] = 0
    
    dist = 1

    while next_queue:
        queue = next_queue
        next_queue = set()
        for pos in queue:
            for direction in directions:
                adjacent = (pos[0] + direction[0], pos[1] + direction[1]) # (xa + xb, ya + yb)
                if adjacent[0] in range(len(distss[0][0])) and adjacent[1] in range(len(distss[0])):
                    if grid[adjacent[1]][adjacent[0]] == -1: # unclaimed
                        grid[adjacent[1]][adjacent[0]] = dist
                        next_queue.add(adjacent)
        dist += 1

find_dists(offset_coords[0], distss[0])

for i, coord in enumerate(offset_coords):
    find_dists(coord, distss[i])

is_infinite = [False] * len(coords)

areas = [0] * len(coords)
is_tie = False

y_range = range(len(distss[0]))
x_range = range(len(distss[0][0]))
for y in y_range:
    for x in x_range:
        min_dist = float('inf')
        closest = None
        for i, dists in enumerate(distss):
            dist = dists[y][x]
            if dist == min_dist:
                is_tie = True
            elif dist < min_dist:
                is_tie = False
                min_dist = dist
                closest = i
        if not is_tie:
            areas[closest] += 1
            if y == 0 or x == 0 or x == len(dists[0])-1 or y == len(dists)-1:
                is_infinite[closest] = True


print(max([area for i, area in enumerate(areas) if not is_infinite[i]]))
                
            

# for each loc:
    # get min of distances
    # if tie, don't count
    # if not tie, add to min area count

# if origin reaches edge point first, it must be infinite