from collections import deque

def solve(n_players, last_marble):
    scores = [0] * n_players
    circle = deque([0])

    for marble in range(1, last_marble+1):
        if marble % 23 == 0:
            circle.rotate(7)
            scores[marble % n_players] += marble + circle.pop()
            circle.rotate(-1)
        else:
            circle.rotate(-1)
            circle.append(marble)
    
    return max(scores)

print('Part 1:', solve(435, 71184))
print('Part 2:', solve(435, 71184*100))