prereqs_orig = {}

with open("2018/input/07.txt") as file:
    for line in file:
        prereq = line[line.find('Step')+5]
        step = line[line.find('step')+5]
        
        if prereq not in prereqs_orig:
            prereqs_orig[prereq] = set()

        if step in prereqs_orig:
            prereqs_orig[step].add(prereq)
        else:
            prereqs_orig[step] = set([prereq])

prereqs = {}
for k, v in prereqs_orig.items():
    prereqs[k] = v.copy()

steps = list(prereqs.keys())
steps.sort()

def remove_step(step, prereqs):
    for prereq in prereqs.values():
        if step in prereq:
            prereq.remove(step)

print('Part 1: ', end='')

while prereqs:
    for step in steps:
        if step in prereqs and not prereqs[step]:
            del prereqs[step]
            remove_step(step, prereqs)
            print(step, end='')
            break
    
print()

for k, v in prereqs_orig.items():
    prereqs[k] = v.copy()

workers = []
for _ in range(5):
    workers.append({'job': None, 'time_remaining': 0})
in_progress = set()

sec = -1
while prereqs:
    # check for finished work and update prereqs
    for worker in workers:
        if worker['job']:
            worker['time_remaining'] -= 1
            if worker['time_remaining'] == 0:
                # job complete
                job = worker['job']
                in_progress.remove(job)
                worker['job'] = None
                del prereqs[job]
                remove_step(job, prereqs)
    
    # queue more jobs
    for worker in workers:
        if not worker['job']:
            for step in steps:
                if step not in in_progress and step in prereqs and not prereqs[step]:
                    worker['job'] = step
                    worker['time_remaining'] = 60 + ord(step) - ord('A') + 1
                    in_progress.add(step)
                    break

    sec += 1

print('Part 2:', sec)
