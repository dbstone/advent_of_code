coords = {}
    
with open("2018/input/06.txt") as file:
    for i, line in enumerate(file):
        a = line[:-1].split(', ')
        a = map(int, a)
        a = tuple(a)
        coords[a] = i

xs = [i[0] for i in coords]
ys = [i[1] for i in coords]
x_min = min(xs)-1
y_min = min(ys)-1

starmap = []
for y in range(y_min, max(ys)+2):
    starmap.append([-1] * (max(xs)+2 - x_min)) # bug maybe? +1

directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]

offset_coords = []
for coord in coords:
    offset_coords.append((coord[0] - x_min, coord[1] - y_min))

# BFS traversal from every "Coordinate" expanding area of influence until
# intersecting other "coordinate"'s area 
def find_dists(coords, grid):
    next_queue = {}  # BFS queue containing tuples of type (origin_index, pos)

    areas = [0] * len(coords)
    is_infinite = [False] * len(coords)

    for i, coord in enumerate(coords):
        next_queue[coord] = i
    
    while next_queue:
        for pos, origin_index in next_queue.items():
            if grid[pos[1]][pos[0]] != -2:
                grid[pos[1]][pos[0]] = origin_index
                areas[origin_index] += 1

        queue = next_queue
        next_queue = {}
        for pos, origin_index in queue.items():
            for direction in directions:
                adjacent = (pos[0] + direction[0], pos[1] + direction[1]) # (xa + xb, ya + yb)
                if adjacent[0] in range(len(starmap[0])) and adjacent[1] in range(len(starmap)):
                    owner = grid[adjacent[1]][adjacent[0]]

                    if owner == -1: # unclaimed
                        if adjacent in next_queue:
                            if origin_index != next_queue[adjacent]:
                                grid[adjacent[1]][adjacent[0]] = -2 # tie!
                                next_queue[adjacent] = -2
                        else:
                            next_queue[adjacent] = origin_index
                elif origin_index != -2:
                    is_infinite[origin_index] = True
    return [area for i, area in enumerate(areas) if not is_infinite[i]]

print(max(find_dists(offset_coords, starmap)))
