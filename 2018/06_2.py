coords = []
    
with open("2018/input/06_2.txt") as file:
    for i, line in enumerate(file):
        a = line[:-1].split(', ')
        a = map(int, a)
        a = tuple(a)
        coords.append(a)

# [print(coord) for coord in coords]

coord_dists = [[(0, 0) for _ in range(len(coords))] for _ in range(len(coords))]
for a, coord_a in enumerate(coords):
    for b, coord_b in enumerate(coords):
        if coord_a == coord_b:
            continue
        diff = (coord_b[0] - coord_a[0], coord_b[1] - coord_a[1])
        coord_dists[a][b] = diff
        coord_dists[b][a] = (-diff[0], -diff[1])

# remove infinites

# get largest area
largest_area = 0
for dists in coord_dists:
    # find bounds

    xs = [dist[0] for dist in dists]
    ys = [dist[1] for dist in dists]

    # top
    above = list(filter(lambda a: a<0, ys))
    if not above:
        continue
    top = min(above) // 2

    # bottom
    below = list(filter(lambda a: a>0, ys))
    if not below:
        continue
    bottom = max(below) // 2

    # left
    leftly = list(filter(lambda a: a<0, xs))
    if not leftly:
        continue
    left = min(leftly) // 2

    # right
    rightly = list(filter(lambda a: a>0, xs))
    if not rightly:
        continue
    right = max(rightly) // 2

    print("top: ", top, "bottom: ", bottom, "left :", left, "right :", right)
    area = (bottom - top) * (right - left)
    print(area)
    largest_area = max(largest_area, area)

print(largest_area)


            









# print(len(starmap[0]), len(starmap))



