claims = []

with open("2018/input/03.txt") as file:
    for line in file:
        coord = line[line.find('@')+1:line.find(':')]
        dims = line[line.find(':')+1:-1]
        claim = {}
        claim['x'], claim['y'] = [int(i) for i in coord.split(',')]
        claim['w'] ,claim['h'] = [int(i) for i in dims.split('x')]
        claims.append(claim)

fabric = []
for _ in range(1000):
    fabric.append([0] * 1000)

for claim in claims:
    for x in range(claim['x'], claim['x']+claim['w']):
        for y in range(claim['y'], claim['y']+claim['h']):
            fabric[y][x] += 1

num_overlapped = 0

for x in range(1000):
    for y in range(1000):
        if fabric[y][x] > 1:
            num_overlapped += 1

print('Part 1:', num_overlapped)

for claim_id in range(len(claims)):
    claim = claims[claim_id]
    overlapped = False
    for x in range(claim['x'], claim['x']+claim['w']):
        for y in range(claim['y'], claim['y']+claim['h']):
            if fabric[y][x] > 1:
                overlapped = True
                break
        if overlapped:
            break
    if not overlapped:
        print('Part 2:', claim_id+1)
