import sys
from functools import lru_cache

grid = []
with open("2019/input/20.txt") as file:
    grid = [line[:-1] for line in file]

incomplete_outer_tps = {} # {name: position}
incomplete_inner_tps = {} # {name: position}
outer_tps = {} # {entrance_position, exit_position}
inner_tps = {} # {entrance_position, exit_position}

for x in range(len(grid[0])):
    for y in range(len(grid)):
        c = grid[y][x]
        if ord('A') <= ord(c) <= ord('Z'):
            dot_pos = None
            name = None
            
            # check right for second char
            if x < len(grid[0])-1:
                c2 = grid[y][x+1]
                if ord('A') <= ord(c2) <= ord('Z'):
                    name = c + c2
                    if x == 0 or (x < len(grid[0])-2 and grid[y][x+2] == '.'):
                        dot_pos = (x+2, y)
                    elif grid[y][x-1] == '.':
                        dot_pos = (x-1, y)
            
            # check down for second char
            if not dot_pos and y < len(grid)-1:
                c2 = grid[y+1][x]
                if ord('A') <= ord(c2) <= ord('Z'):
                    name = c + c2
                    if y == 0 or (y < len(grid)-2 and grid[y+2][x] == '.'):
                        dot_pos = (x, y+2)
                    elif grid[y-1][x] == '.':
                        dot_pos = (x, y-1)
            
            if name and dot_pos:
                is_outer = dot_pos[0] == 2 or dot_pos[0] == len(grid[0])-3 or dot_pos[1] == 2 or dot_pos[1] == len(grid)-3
                if is_outer:
                    if name in incomplete_inner_tps:
                        outer_tps[dot_pos] = incomplete_inner_tps[name]
                        inner_tps[incomplete_inner_tps[name]] = dot_pos
                    else:
                        incomplete_outer_tps[name] = dot_pos
                else:
                    if name in incomplete_outer_tps:
                        inner_tps[dot_pos] = incomplete_outer_tps[name]
                        outer_tps[incomplete_outer_tps[name]] = dot_pos
                    else:
                        incomplete_inner_tps[name] = dot_pos

# BFS traversal
directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
visited = set()  # set of grid cells that have been visted
adj_key_info = {} # {key: (dist, keys_needed, keys_collected)}

next_queue = []  # BFS queue containing tuples of type (pos, keys_needed)

first = (incomplete_outer_tps['AA'], 0, (0,))

next_queue.append(first)
visited.add((first[0], first[1]))
steps_taken = 0

while next_queue:
    queue = next_queue
    next_queue = []
    while queue:
        pos, level, path = queue.pop()
        if pos == incomplete_outer_tps['ZZ'] and level == 0:
            print('steps taken:', steps_taken)
            sys.exit()

        adjacents = []
        for direction in directions:
            adjacents.append(((pos[0] + direction[0], pos[1] + direction[1]), level, path)) # (xa + xb, ya + yb)
        
        if pos in outer_tps and level > 0:
            adjacents.append((outer_tps[pos], level-1, path+(level-1,)))
        elif pos in inner_tps:
            adjacents.append((inner_tps[pos], level+1, path+(level+1,)))
        
        for adjacent in adjacents:
            adj_pos = adjacent[0]
            c = grid[adj_pos[1]][adj_pos[0]]

            if c == '.' and (adjacent[0], adjacent[1]) not in visited:
                next_queue.append(adjacent)
                visited.add((adjacent[0], adjacent[1]))
    steps_taken += 1
