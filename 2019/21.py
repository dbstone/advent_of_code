from intcode import intcode

class SpringdroidController:
    def __init__(self, script):
        self.script = script
        self.script_ptr = 0

    def input(self):
        out = ord(self.script[self.script_ptr])
        self.script_ptr += 1
        return out

    def output(self, val: int):
        if 0 <= val <= 255:
            print(chr(val), end='')
        else:
            print('Hull damage:', val)

program = []
with open("2019/input/21.txt") as file:
    program = [int(instruction) for instruction in file.read().split(',')]

program += [0] * 1000 # allocate extra memory

def run_spring_script(script):
    script = open(script).read()
    controller = SpringdroidController(script)
    intcode.run_intcode(program.copy(), intcode.IntcodeState(), controller.input, controller.output)

print('--- Part 1 ---------------------------')
run_spring_script("2019/21.springscript")
print('\n--- Part 2 ---------------------------')
run_spring_script("2019/21_2.springscript")
