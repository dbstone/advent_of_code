wires = ({'horiz': [], 'vert': []}, {'horiz': [], 'vert': []})

with open("2019/input/03.txt") as file:
    for i in range(2):
        path = file.readline().split(',')
        start = (0, 0)

        # accumulate graph edges, along with the steps required to reach them
        steps = 0
        for segment in path:
            direction = segment[0]
            dist = int(segment[1:])
            end = (0, 0)
        
            if direction == 'U':
                end = (start[0], start[1]+dist)
                wires[i]['vert'].append((start, end, steps))
            elif direction == 'D':
                end = (start[0], start[1]-dist)
                wires[i]['vert'].append((start, end, steps))
            elif direction == 'L':
                end = (start[0]-dist, start[1])
                wires[i]['horiz'].append((start, end, steps))
            elif direction == 'R':
                end = (start[0]+dist, start[1])
                wires[i]['horiz'].append((start, end, steps))
            
            steps += dist
            start = end

def get_intersection_distance(hseg, vseg):
    intersect = (vseg[0][0], hseg[0][1])
    if ((intersect[0] - hseg[0][0]) ^ (intersect[0] - hseg[1][0]) <= 0 and
        (intersect[1] - vseg[0][1]) ^ (intersect[1] - vseg[1][1]) <= 0):
        dist = abs(intersect[0]) + abs(intersect[1])
        steps_h = hseg[2] + abs(vseg[0][0] - hseg[0][0])
        steps_v = vseg[2] + abs(hseg[0][1] - vseg[0][1])
        steps = steps_h + steps_v
        return dist, steps
    return float('inf'), float('inf')

min_intersection_dist = float('inf')
min_intersection_steps = float('inf')

# First compare wire-0 horizontal segments to wire-1 vertical segments,
for hseg in wires[0]['horiz']:
    for vseg in wires[1]['vert']:
        dist, steps = get_intersection_distance(hseg, vseg)
        min_intersection_dist = min(min_intersection_dist, dist)
        min_intersection_steps = min(min_intersection_steps, steps)

# then compare wire-0 vertical segments to wire-1 horizontal segments.
for vseg in wires[0]['vert']:
    for hseg in wires[1]['horiz']:
        dist, steps = get_intersection_distance(hseg, vseg)
        min_intersection_dist = min(min_intersection_dist, dist)
        min_intersection_steps = min(min_intersection_steps, steps)

print('Part 1:', min_intersection_dist)
print('Part 2:', min_intersection_steps)
