def day_4(l, r, is_part_1):
    num_valid = 0

    for n in range(l, r + 1):
        pw = str(n)
        has_double = False
        decreased = False
        for i in range(1, len(pw)):
            if pw[i] == pw[i-1]:
                if is_part_1 or ((i < 2 or pw[i] != pw[i-2]) and 
                                 (i >= len(pw)-1 or pw[i] != pw[i+1])):
                    has_double = True
            elif int(pw[i]) < int(pw[i-1]):
                decreased = True
                break
        if has_double and not decreased:
            num_valid += 1

    return num_valid

L, R = 206938, 679128   # puzzle input

print('Part 1:', day_4(L, R, True))
print('Part 2:', day_4(L, R, False))