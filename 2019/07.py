import itertools
from intcode import intcode

program_orig = []
with open("2019/input/07.txt") as file:
    program_orig  = [int(instruction) for instruction in file.read().split(',')]

class AmpController:
    def __init__(self, program, phase, inpt):
        self.state = intcode.IntcodeState()
        self.input_phase = True
        self.phase = phase
        self.input_val = inpt
        self.output_val = None
        self.program = program

    def input(self):
        if self.input_phase:
            self.input_phase = False
            return self.phase
        else:
            return self.input_val
    
    def output(self, val):
        self.output_val = val
    
perms = itertools.permutations([0,1,2,3,4])
highest = 0
for perm in perms:
    inpt = 0
    for phase in perm:
        amp = AmpController(program_orig.copy(), phase, inpt)
        intcode.run_intcode(amp.program, intcode.IntcodeState(), amp.input, amp.output, False, False)
        inpt = amp.output_val
    highest = max(highest, inpt)

print('Part 1:', highest)

perms = itertools.permutations([5,6,7,8,9])
highest = 0
for perm in perms:
    amps = [AmpController(program_orig.copy(), phase, 0) for phase in perm]
    inpt = 0
    i = 0
    while i < len(perm):
        amps[i].input_val = inpt
        amps[i].output_val = -1
        intcode.run_intcode(amps[i].program, amps[i].state, amps[i].input, amps[i].output, False, True)
        if amps[i].output_val == -1:
            break
        inpt = amps[i].output_val
        if i == 4:
            i = 0
        else:
            i += 1
    highest = max(highest, inpt)

print('Part 2:', highest)