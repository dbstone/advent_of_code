parents = {}    # map of each planet to it's "parent", 
                # i.e. the next planet inward in the orbit chain

with open("2019/input/06.txt") as file:
    for line in file:
        a = line[:3]
        b = line[4:7]
        parents[b] = a

def count_orbits(orbit_counts, planet):
    if planet in orbit_counts:
        return orbit_counts[planet]

    parent = parents[planet]
   
    if parent == 'COM':
        return 1
    else:
        parent_count = count_orbits(orbit_counts, parent)
        orbit_counts[parent] = parent_count
        return parent_count + 1

orbit_counts = {}   # total number of orbits for each planet

for planet in parents:
    orbit_counts[planet] = count_orbits(orbit_counts, planet)
    if len(orbit_counts) == len(parents):
        print('Part 1:', sum(orbit_counts.values()))
        break

# get SAN orbit chain
san_chain = []
curr = 'SAN'
while curr != 'COM':
    curr = parents[curr]
    san_chain.append(curr)

# move inwards from YOU until we meet SAN chain, then combine lengths
curr = 'YOU'
transfers = 0
while curr != 'COM':
    if curr in san_chain:
        print('Part 2:', transfers + san_chain.index(curr) - 1)
        break
    transfers += 1
    curr = parents[curr]