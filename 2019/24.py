import copy
from collections import deque

start_grid = []
with open("2019/input/24.txt") as file:
    for line in file:
        row = []
        for c in line[:-1]:
            if c == '#':
                row.append(True)
            else:
                row.append(False)
        start_grid.append(row)

def print_grid(grid):
    [print(line) for line in grid]

def tuple_add(a, b):
	return tuple(map(lambda x, y: x + y, a, b))

directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]

def get_next_grid(grid):
    w, h = len(grid[0]), len(grid)
    next_grid = []
    for y in range(h):
        row = []
        for x in range(w):
            pos = (x, y)
            num_adjacent = 0
            for direction in directions:
                x2, y2 = tuple_add(pos, direction)
                if x2 in range(w) and y2 in range(h):
                    if grid[y2][x2]:
                        num_adjacent += 1
            
            if grid[y][x]:
                if num_adjacent != 1:
                    row.append(False)
                else:
                    row.append(True)
            else:
                if num_adjacent == 1 or num_adjacent == 2:
                    row.append(True)
                else:
                    row.append(False)
        next_grid.append(row)
    return next_grid

def grid_equals(a, b):
    w, h = len(a[0]), len(a)
    for y in range(h):
        for x in range(w):
            if a[y][x] != b[y][x]:
                return False
    return True

def get_grid_biodiversity(grid):
    w, h = len(grid[0]), len(grid)
    rating = 0
    i = 0
    for y in range(h):
        for x in range(w):
            if grid[y][x]:
                rating += 2 ** i
            i += 1
    return rating

# Part 1
grids = [copy.deepcopy(start_grid)]
part1_complete = False
while not part1_complete:
    grid = get_next_grid(grids[-1])
    for old_grid in grids:
        if grid_equals(old_grid, grid):
            print('Part 1:', get_grid_biodiversity(grid))
            part1_complete = True
            break
    grids.append(grid)

def count_bugs(grids):
    count = 0
    w, h = len(grids[0][0]), len(grids[0])
    for grid in grids:
        for y in range(h):
            for x in range(w):
                if grid[y][x]:
                    count += 1
    return count

def get_next_grid_2(grids, i):
    next_grid = []
    grid = grids[i]
    w, h = len(grid[0]), len(grid)
    for y in range(h):
        row = []
        for x in range(w):
            pos = (x, y)
            num_adjacent = 0
            for direction in directions:
                x2, y2 = tuple_add(pos, direction)
                if x2 in range(w) and y2 in range(h):
                    if grid[y2][x2]:
                        num_adjacent += 1
            
            if i != 0:
                outer_grid = grids[i-1]
                if x == 0:
                    # add left
                    if outer_grid[2][1]:
                        num_adjacent += 1
                elif x == w-1:
                    # add right
                    if outer_grid[2][3]:
                        num_adjacent += 1
                if y == 0:
                    # add top
                    if outer_grid[1][2]:
                        num_adjacent += 1
                elif y == h-1:
                    # add bottom
                    if outer_grid[3][2]:
                        num_adjacent += 1
            
            if i != len(grids)-1:
                inner_grid = grids[i+1]
                if x == 1 and y == 2:
                    # add inner left col
                    for y2 in range(h):
                        if inner_grid[y2][0]:
                            num_adjacent += 1
                elif x == 3 and y == 2:
                    # add inner right col
                    for y2 in range(h):
                        if inner_grid[y2][w-1]:
                            num_adjacent += 1
                elif x == 2 and y == 1:
                    # add inner top row
                    for x2 in range(w):
                        if inner_grid[0][x2]:
                            num_adjacent += 1
                elif x == 2 and y == 3:
                    # add inner bottom row
                    for x2 in range(w):
                        if inner_grid[h-1][x2]:
                            num_adjacent += 1
            
            if grid[y][x]:
                if num_adjacent != 1:
                    row.append(False)
                else:
                    row.append(True)
            else:
                if num_adjacent == 1 or num_adjacent == 2:
                    row.append(True)
                else:
                    row.append(False)
        next_grid.append(row)
    next_grid[2][2] = False
    return next_grid

# Part 2
grids = deque()
grids.append(copy.deepcopy(start_grid))
w, h = len(grid[0]), len(grid)
for _ in range(200):
    new_grids = deque()
   
    # add new outer grid if necessary
    first_grid = grids[0]
    
    n_left_col = 0
    for y in range(h):
        if first_grid[y][0]:
            n_left_col += 1
    
    n_right_col = 0
    for y in range(h):
        if first_grid[y][w-1]:
            n_right_col += 1

    n_top_row = 0
    for x in range(w):
        if first_grid[0][x]:
            n_top_row += 1
    
    n_bottom_row = 0
    for x in range(w):
        if first_grid[h-1][x]:
            n_bottom_row += 1
    
    add_left = n_left_col == 1 or n_left_col == 2
    add_right = n_right_col == 1 or n_right_col == 2
    add_top = n_top_row == 1 or n_top_row == 2
    add_bottom = n_bottom_row == 1 or n_bottom_row == 2

    if add_left or add_right or add_top or add_bottom:
        outer_grid = [[False for x in range(w)] for y in range(h)]
        if add_left:
            outer_grid[2][1] = True
        if add_right:
            outer_grid[2][3] = True
        if add_top:
            outer_grid[1][2] = True
        if add_bottom:
            outer_grid[3][2] = True
        new_grids.append(outer_grid)

    # update existing grids
    for i in range(len(grids)):
        new_grids.append(get_next_grid_2(grids, i))
    
    # add new inner grid if necessary
    last_grid = grids[-1]

    left_bug = last_grid[2][1]
    right_bug = last_grid[2][3]
    top_bug = last_grid[1][2]
    bottom_bug = last_grid[3][2]

    if left_bug or right_bug or top_bug or bottom_bug:
        inner_grid = [[False for x in range(w)] for y in range(h)]
        if left_bug:
            for y in range(h):
                inner_grid[y][0] = True
        if right_bug:
            for y in range(h):
                inner_grid[y][w-1] = True
        if top_bug:
            for x in range(w):
                inner_grid[0][x] = True
        if bottom_bug:
            for x in range(w):
                inner_grid[h-1][x] = True
        new_grids.append(inner_grid)
    
    grids = new_grids

print('Part 2:', count_bugs(grids))