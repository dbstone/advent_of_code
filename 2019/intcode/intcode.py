from typing import List, Callable

class IntcodeState():
    def __init__(self):
        self.eip = 0
        self.relative_base = 0

def get_param_modes(eip_str: str) -> List[int]:
    modes = []
    # maximum 3 params
    for i in range(3):
        mode = 0
        # possible bug!
        if len(eip_str) >= i+3:
            mode = int(eip_str[-3 - i])
        modes.append(mode)
    return modes

def parse_instruction(program: List[int], eip: int, relative_base: int) -> List[int]:
    eip_str = str(program[eip])
    instruction = int(eip_str[-2:] if len(eip_str) >= 2 else eip_str[-1])
    params = [0, 0, 0] # maximum 3 params
    modes = get_param_modes(eip_str)
    for i in range(len(modes)):
        param_pos = eip+i+1
        if modes[i] == 0:
            if param_pos < len(program):
                params[i] = program[param_pos]
        elif modes[i] == 1:
            params[i] = eip+i+1
        elif modes[i] == 2:
            if param_pos < len(program):
                params[i] = relative_base + program[eip+i+1]
    return instruction, params

def run_intcode(
    program: List[int],
    state: IntcodeState,
    input_handler: Callable[[], int],
    output_handler: Callable[[int], None],
    return_on_input: bool = False,
    return_on_output: bool = False) -> None:

    while True:
        instruction, params = parse_instruction(program, state.eip, state.relative_base)

        if instruction == 1:
            # add
            program[params[2]] = program[params[0]] + program[params[1]]
            state.eip += 4
        elif instruction == 2:
            # multiply
            program[params[2]] = program[params[0]] * program[params[1]]
            state.eip += 4
        elif instruction == 3:
            # input
            if input_handler:
                program[params[0]] = input_handler()
            state.eip +=2
            if return_on_input:
                return
        elif instruction == 4:
            # output
            if output_handler:
                output_handler(program[params[0]])
            state.eip += 2
            if return_on_output:
                return
        elif instruction == 5:
            # jump-if-true
            if program[params[0]]:
                state.eip = program[params[1]]
            else:
                state.eip += 3
        elif instruction == 6:
            # jump-if-false
            if not program[params[0]]:
                state.eip = program[params[1]]
            else:
                state.eip += 3
        elif instruction == 7:
            # less than
            program[params[2]] = 1 if program[params[0]] < program[params[1]] else 0
            state.eip += 4
        elif instruction == 8:
            # equals
            program[params[2]] = 1 if program[params[0]] == program[params[1]] else 0
            state.eip += 4
        elif instruction == 9:
            # relative base offset
            state.relative_base += program[params[0]]
            state.eip += 2
        elif instruction == 99:
            # exit
            return
        else:
            print(program)
            print('ERROR - bad op code: ', str(program[state.eip]))
            return