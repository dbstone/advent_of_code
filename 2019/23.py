from collections import deque
from intcode import intcode

class NIC:
    def __init__(self, program, addr, send_packet_func):
        self.program = program
        self.program_state = intcode.IntcodeState()
        self.addr = addr
        self.send_packet_func = send_packet_func
        self.packet_queue = deque()
        self.output_buff = []
        self.input_y = False
        self.booted = False
        self.n_idle = 0

    def input(self):
        n_idle = self.n_idle
        self.n_idle = 0
        if not self.booted:
            self.booted = True
            return self.addr
        else:
            if not self.packet_queue:
                self.n_idle = n_idle + 1
                return -1
            elif self.input_y:
                y = self.packet_queue[0][1]
                self.packet_queue.popleft()
                self.input_y = False
                return y
            else:
                x = self.packet_queue[0][0]
                self.input_y = True
                return x

    def output(self, val: int):
        self.output_buff.append(val)
        if len(self.output_buff) == 3:
            addr = self.output_buff[0]
            packet = (self.output_buff[1], self.output_buff[2])
            self.send_packet_func(addr, packet)
            self.output_buff = []

    def receive_packet(self, packet):
        # print('nic', self.addr, 'received packet:', packet)
        self.packet_queue.append(packet)

    def update(self):
        intcode.run_intcode(
            self.program, 
            self.program_state,
            self.input, 
            self.output,
            return_on_input=True)

class Router():
    def __init__(self):
        self.nics = []
        self.nat = None
        self.last_delivered_nat_y = None
        self.done = False

    def send_packet(self, addr, packet):
        if addr == 255:
            if not self.nat:
                print('Part 1:', packet[1])
            self.nat = packet
        elif addr not in range(len(self.nics)):
            raise Exception('addr out of range')
        else:
            self.nics[addr].receive_packet(packet)
    
    def update(self):
        for nic in self.nics:
            nic.update()
        
        if all(nic.n_idle > 1 for nic in self.nics):
            if self.last_delivered_nat_y == self.nat[1]:
                print('Part 2:', self.last_delivered_nat_y)
                self.done = True
            self.last_delivered_nat_y = self.nat[1]
            self.nics[0].receive_packet(self.nat)

program = []
with open("2019/input/23.txt") as file:
    program = [int(instruction) for instruction in file.read().split(',')]

program += [0] * 1000 # allocate extra memory

rtr = Router()

for i in range(50):
    rtr.nics.append(NIC(program.copy(), i, rtr.send_packet))

while not rtr.done:
    rtr.update()
