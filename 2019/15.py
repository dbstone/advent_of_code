import sys
import math
from enum import IntEnum
from intcode import intcode

def tuple_add(a, b):
	return tuple(map(lambda x, y: x + y, a, b))

Direction = IntEnum('Direction', 'NORTH SOUTH WEST EAST')
directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
opposites = [1, 0, 3, 2]

class RepairDroid:
    def __init__(self):
        self.grid = {(0,0): 0}
        self.pos = (0,0)
        self.x_min, self.x_max, self.y_min, self.y_max = 0, 0, 0, 0
        self.isBacktracking = False
        self.path = []
        self.oxygen_sys_pos = None

    def update_map_bounds(self, point):
        x, y = point
        self.x_min = min(x, self.x_min)
        self.x_max = max(x, self.x_max)
        self.y_min = min(y, self.y_min)
        self.y_max = max(y, self.y_max)
    
    def input(self):
        for direction in range(4):
            if not self.path or opposites[direction] != self.path[-1]:   # don't go backwards
                point = tuple_add(self.pos, directions[direction])
                if point not in self.grid: # if not already visited
                    self.path.append(direction)
                    self.pos = tuple_add(self.pos, directions[direction])
                    self.update_map_bounds(self.pos)
                    return direction+1

        # no where unvisited to go :(
        # backtrack instead 
        if not self.path:
            # backtracking from the beginning? we must have fully explored!
            # self.draw_screen()
            print('Part 2 (minutes until oxygen fill):', self.get_oxygen_fill_time())
            sys.exit()
        direction = opposites[self.path.pop()]
        self.pos = tuple_add(self.pos, directions[direction])
        self.update_map_bounds(self.pos)
       
        return direction+1

    def output(self, val: int):
        if val == 1:    # moved successfully
            if self.pos in self.grid:
                self.grid[self.pos] = min(self.grid[self.pos], len(self.path))
            else:
                self.grid[self.pos] = len(self.path)
        elif val == 0:  # hit a wall, failed to move
            self.grid[self.pos] = -1
            self.pos = tuple_add(self.pos, directions[opposites[self.path.pop()]])
        elif val == 2:  # moved and found oxygen system
            self.grid[self.pos] = -2
            self.oxygen_sys_pos = self.pos
            print("Part 1 (distance to oxygen system):", len(self.path))
    
    def draw_screen(self):
        print('------------------------------------------------------')
        for y in range(self.y_min, self.y_max+1):
            for x in range(self.x_min, self.x_max+1):
                if (x,y) == self.pos:
                    print('@', end='')
                elif (x, y) in self.grid:
                    if self.grid[(x,y)] == -1:
                        print('#', end='')
                    elif self.grid[(x,y)] == -2:
                        print('X', end='')
                    else:
                        print(' ', end='')
                else:
                    print(' ', end='')
            print()

    # BFS traversal to get distance of farthest grid cell
    def get_oxygen_fill_time(self):
        filled = set()  # set of grid cells that have been filled

        next_minute_queue = []  # BFS queue

        pos = self.oxygen_sys_pos
        next_minute_queue.append(pos)
        filled.add(pos)

        minutes_elapsed = -1 # start at -1, since we don't want to count the first step
        while next_minute_queue:
            queue = next_minute_queue
            next_minute_queue = []
            while queue:
                pos = queue.pop()

                for direction in directions:
                    adjacent = tuple_add(pos, direction)
                    if self.grid[adjacent] != -1 and adjacent not in filled:
                        next_minute_queue.append(adjacent)
                        filled.add(adjacent)
            minutes_elapsed += 1
        
        return minutes_elapsed

repair_droid_program = []
with open("2019/input/15.txt") as file:
    repair_droid_program = [int(instruction) for instruction in file.read().split(',')]

repair_droid_program += [0] * 1000 # allocate extra memory

droid = RepairDroid()

intcode.run_intcode(repair_droid_program, intcode.IntcodeState(), droid.input, droid.output)
