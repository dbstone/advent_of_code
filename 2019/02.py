def run_intcode(program):
    eip = 0
    while program[eip] != 99:
        if program[eip] == 1:
            program[program[eip+3]] = program[program[eip+1]] + program[program[eip+2]]
        elif program[eip] == 2:
            program[program[eip+3]] = program[program[eip+1]] * program[program[eip+2]]
        else:
            print('ERROR - bad op code: ', program[eip])
            break
        
        eip += 4
    
program_orig = []
with open("2019/input/02.txt") as file:
    program_orig = [int(instruction) for instruction in file.read().split(',')]

program = program_orig.copy()
program[1] = 12
program[2] = 2
run_intcode(program)
print('Part 1:', program[0])

# find verb if noun given, otherwise find noun
def findNounVerbBinarySearch(l, r, target, noun=None): 
    if r >= l: 
        mid = l + (r - l) // 2
        program = program_orig.copy()
        program[1] = noun if noun else mid
        program[2] = mid if noun else 0
        run_intcode(program)
        if program[0] > target: 
            return findNounVerbBinarySearch(l, mid-1, target, noun) 
        else: 
            return findNounVerbBinarySearch(mid+1, r, target, noun) 
    else: 
        return r

noun = findNounVerbBinarySearch(0, 99, 19690720)
verb = findNounVerbBinarySearch(0, 99, 19690720, noun)
print('Part 2:', 100 * noun + verb)
