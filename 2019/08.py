w, h = 25, 6
layers = []

with open("2019/input/08.txt") as file:
    image = file.read()
    layer_len = w * h
    num_layers = int((len(image)-1) / layer_len)
    for i in range(num_layers):
        start = layer_len * i
        end = start + layer_len
        layers.append(image[start:end])

fewest_zeros = float('inf')
fewest_zero_layer = str()

for i, layer in enumerate(layers):
    zeros = layer.count('0')
    if zeros < fewest_zeros:
        fewest_zeros = zeros
        fewest_zero_layer = layer

ones = fewest_zero_layer.count('1')
twos = fewest_zero_layer.count('2')

print('Part 1:', ones * twos)

composite = layers[0]
for layer in layers[1:]:
    for i in range(len(composite)):
        if composite[i] == '2':
            composite = composite[:i] + layer[i] + composite[i+1:]

for i in range(h):
    start = w*i
    end = start + w

dotted = str()
for c in composite:
    if c == '0':
        dotted += ' '
    else:
        dotted += '#'

print('Part 2:')
for i in range(h):
    start = w*i
    end = start + w
    print(dotted[start:end])