from intcode import intcode

ascii_program = []
with open("2019/input/19.txt") as file:
    ascii_program = [int(instruction) for instruction in file.read().split(',')]

ascii_program += [0] * 1000 # allocate extra memory

class DroneController:
    def __init__(self, x_start, y_start, w, h):
        self.grid = []
        self.x_start = x_start
        self.y_start = y_start
        self.grid_w = w
        for _ in range(h):
            self.grid.append(str())
        self.x, self.y = 0, 0
        self.input_y = False
        self.num_beam_points = 0
        self.state = intcode.IntcodeState()
        self.done = False

    def input(self):
        if self.input_y:
            self.input_y = False
            return self.y + self.y_start
        else:
            self.input_y = True
            return self.x + self.x_start

    def output(self, val: int):
        if val == 1:
            self.grid[self.y] += '#'
            self.num_beam_points += 1
        else:
            self.grid[self.y] += '.'

        self.x += 1
        if self.x == self.grid_w:
            self.x = 0
            self.y += 1
            if self.y == len(self.grid):
                self.done = True
    
    def draw_grid(self):
        for line in self.grid:
            print(line)

controller = DroneController(0, 0, 50, 50)

while not controller.done:
    intcode.run_intcode(
        ascii_program.copy(), 
        intcode.IntcodeState(), 
        controller.input, 
        controller.output)
               
print('Part 1:', controller.num_beam_points)

class DroneController2:
    def __init__(self):
        self.x, self.y = 0, 0
        self.input_is_x = False
        self.horiz_needs_check = True
        self.vert_needs_check = True
        self.output_is_horiz = True
        self.done = False

    def input(self):
        self.input_is_x = not self.input_is_x # toggle between x and y inputs
        
        if self.horiz_needs_check:
            if self.input_is_x:
                return self.x + 99
            else:
                self.output_is_horiz = True
                return self.y
        elif self.vert_needs_check:
            if self.input_is_x:
                return self.x
            else:
                self.output_is_horiz = False
                return self.y + 99

    def output(self, val: int):
        if self.output_is_horiz:
            if val == 1:
                self.horiz_needs_check = False
            else:
                self.y += 1
                self.vert_needs_check = True
        else:
            if val == 1:
                self.vert_needs_check = False
            else:
                self.x += 1
                self.horiz_needs_check = True
        
        if not self.horiz_needs_check and not self.vert_needs_check:
            self.done = True

controller = DroneController2()

while not controller.done:
    intcode.run_intcode(
        ascii_program.copy(), 
        intcode.IntcodeState(), 
        controller.input, 
        controller.output)

print('Part 2:', str(controller.x) + '0' + str(controller.y))