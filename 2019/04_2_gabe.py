# Credit: Gabriel Fy
# This version is gross, but runs significantly faster than mine.

def day4_part2(l, r):
    password_answers_count = 0
    for i in range(l, r + 1):
        num = str(i)
        if num[0] != num[1] != num[2] != num[3] != num[4] != num[5]:
            continue
        elif num[5] == num[4] == num[3] and num[2] != num[1] and num[1] != num[0]:
            continue
        elif num[4] == num[3] == num[2] and num[1] != num[0]:
            continue
        elif num[0] == num[1] == num[2] and num[3] != num[4] and num[4] != num[5]:
            continue
        elif num[1] == num[2] == num[3] and num[4] != num[5]:
            continue
        elif num[0] == num[1] == num[2] == num[3] and num[4] != num[5]:
            continue
        elif num[1] == num[2] == num[3] == num[4]:
            continue
        elif num[2] == num[3] == num[4] == num[5] and num[0] != num[1]:
            continue
        elif num[0] == num[1] == num[2] == num[3] == num[4] == num[5]:
            continue
        elif num[1] == num[2] == num[3] == num[4] == num[5]:
            continue
        elif num[0] == num[1] == num[2] == num[3] == num[4]:
            continue
        elif num[5] == num[4] == num[3] and num[2] == num[1] == num[0]:
            continue
        elif num[5] >= num[4]:
            if num[4] >= num[3]:
                if num[3] >= num[2]:
                    if num[2] >= num[1]:
                        if num[1] >= num[0]:
                            password_answers_count += 1

    return password_answers_count

print(day4_part2(206938, 679128))