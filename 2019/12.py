import itertools
import numpy as np
 
class Moon:
    def __init__(self, position):
        self.position = position
        self.velocity = [0, 0, 0]
    
    def move(self):
        self.position = [sum(coord) for coord in zip(self.position, self.velocity)]

part1_moons = []
original_moons = []
moons = []
with open("2019/input/12.txt") as file:
    for line in file:
        line = line[1:-2] # remove '<', '>', and '\n'
        start_pos = [int(coord[2:]) for coord in line.split(', ')]
        moons.append(Moon(start_pos.copy()))
        original_moons.append(Moon(start_pos.copy()))
        part1_moons.append(Moon(start_pos.copy()))

for _ in range(1000):
    # apply gravitation to velocity
    for moon_a, moon_b in itertools.combinations(part1_moons, 2):
        for i in range(len(moon_a.position)):
            if moon_a.position[i] < moon_b.position[i]:
                moon_a.velocity[i] += 1
                moon_b.velocity[i] -= 1
            elif moon_a.position[i] > moon_b.position[i]:
                moon_a.velocity[i] -= 1
                moon_b.velocity[i] += 1

    # update position based on velocity
    for moon in part1_moons:
        moon.move()

# calculate energy
sum_abs = lambda x: sum(abs(y) for y in x)
energy = lambda moon: sum_abs(moon.position) * sum_abs(moon.velocity)
total_energy = sum(energy(moon) for moon in part1_moons)
print('Part 1:', total_energy)

# Part 2 intuition: x, y, and z are independent, so we can simulate them separately

# Find the period for each coordinate by simulating until position and velocity match original
periods = []
for coord in range(3):
    step = 0
    while True:
        # apply gravitation to velocity
        for moon_a, moon_b in itertools.combinations(moons, 2):
            if moon_a.position[coord] < moon_b.position[coord]:
                moon_a.velocity[coord] += 1
                moon_b.velocity[coord] -= 1
            elif moon_a.position[coord] > moon_b.position[coord]:
                moon_a.velocity[coord] -= 1
                moon_b.velocity[coord] += 1

        # update position based on velocity
        for moon in moons:
            moon.position[coord] = moon.position[coord] + moon.velocity[coord]

        step += 1

        # compare to original
        match = True
        for i in range(len(moons)):
            moon = moons[i]
            original = original_moons[i]
            if moon.position[coord] != original.position[coord] or moon.velocity[coord] != original.velocity[coord]:
                match = False
                break
        if match:
            periods.append(step)
            break

# system period = LCM of all coordinate periods
system_period = np.lcm.reduce(periods, dtype='int64')

print('Part 2:', system_period)