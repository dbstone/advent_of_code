from enum import IntEnum
from intcode import intcode

directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]

move_main = 'A,A,B,C,B,C,B,C,B,A\n'
move_a = 'L,10,L,8,R,8,L,8,R,6\n'
move_b = 'R,6,R,8,R,8\n'
move_c = 'R,6,R,6,L,8,L,10\n'
use_continuous_video = 'n\n'
input_funcs = [move_main, move_a, move_b, move_c, use_continuous_video]

class AsciiController:
    def __init__(self):
        self.grid = []
        self.next_grid_line = str()
        self.input_func_str_i = 0
        self.input_func_i = 0
        self.done = False
        self.part2 = None

    def input(self):
        out = ord(input_funcs[self.input_func_i][self.input_func_str_i])
        self.input_func_str_i += 1
        if self.input_func_str_i >= len(input_funcs[self.input_func_i]):
            self.input_func_i += 1
            self.input_func_str_i = 0
            if self.input_func_i >= len(input_funcs):
                self.done = True
        return out

    def output(self, val: int):
        if val > 255:
            self.part2 = val
        else:
            c = chr(val)
            if c == '\n':
                self.grid.append(self.next_grid_line)
                self.next_grid_line = str()
            else:
                self.next_grid_line += c
    
    def draw_grid(self):
        for line in self.grid:
            print(line)

    def get_intersections(self):
        intersections = []
        for row in range(1, len(self.grid)-2):
            for col in range(1, len(self.grid[0])-1):
                if self.grid[row][col] == '#':
                    is_intersection = True
                    for direction in directions:
                        if self.grid[direction[1] + row][direction[0] + col] != '#':
                            is_intersection = False
                            break
                    if is_intersection:
                        intersections.append((col, row))
        return intersections

ascii_program = []
with open("2019/input/17.txt") as file:
    ascii_program = [int(instruction) for instruction in file.read().split(',')]

ascii_program += [0] * 3000 # allocate extra memory

controller = AsciiController()
intcode.run_intcode(ascii_program.copy(), intcode.IntcodeState(), controller.input, controller.output)
print('Part 1:', sum([x * y for x, y in controller.get_intersections()]))

controller = AsciiController()
ascii_program[0] = 2 # wake up vacuum robot
intcode.run_intcode(ascii_program, intcode.IntcodeState(), controller.input, controller.output)
print('Part 2:', controller.part2)