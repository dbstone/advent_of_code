import math
import collections

def tuple_sub(a, b):
	return tuple(map(lambda x, y: x - y, a, b))

starmap = []
asteroids = []

with open("2019/input/10.txt") as file:
    starmap = [line[:-1] for line in file] # line[-1] to remove newline characters

for y in range(len(starmap)):
    for x in range(len(starmap[0])):
        if starmap[y][x] == '#':
            asteroids.append((x, y))

most_detected = 0
detected_angles = {}

for candidate in asteroids:
    candidate_detected_angles = {}
    for asteroid in asteroids:
        if asteroid is not candidate:
            diff = tuple_sub(asteroid, candidate)
            angle = math.atan2(diff[1], diff[0]) + math.pi/2
            angle = angle % (math.pi*2)
            asteroid_offset = (asteroid[0], asteroid[1], diff[0]**2 + diff[1]**2)
            if angle in candidate_detected_angles:
                candidate_detected_angles[angle].append(asteroid_offset)
            else:
                candidate_detected_angles[angle] = [asteroid_offset]

    num_detected = len(candidate_detected_angles)
    if num_detected > most_detected:
        most_detected = num_detected
        detected_angles = candidate_detected_angles

print('Part 1:', most_detected)

for angle in detected_angles:
    detected_angles[angle].sort(key=lambda asteroid: asteroid[2])

ordered_angles = sorted(detected_angles.items(), key=lambda t: t[0])

angle_i = 0
vaporized = 0
while True:
    angle = ordered_angles[angle_i]
    if angle[1]:
        # print("vaporizing asteroid", angle[1][0])
        vaporized += 1
        if vaporized == 200:
            print('Part 2:', angle[1][0][0] * 100 + angle[1][0][1])
            break
        angle[1].pop(0)
    
    if angle_i == len(ordered_angles):
        angle_i = 0
    else:
        angle_i += 1
