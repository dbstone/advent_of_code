from functools import lru_cache

directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
caps_offset = ord('a') - ord('A')

# BFS traversal to get distance to every key from a certain position
def get_adj_key_info(pos, grid):
    visited = set()  # set of grid cells that have been visted
    adj_key_info = {} # {key: (dist, keys_needed, keys_collected)}

    next_queue = []  # BFS queue containing tuples of type (pos, keys_needed)

    next_queue.append((pos, set(), set()))
    visited.add(pos)
    steps_taken = 1

    while next_queue:
        queue = next_queue
        next_queue = []
        while queue:
            pos, keys_needed, keys_collected = queue.pop()
            
            for direction in directions:
                adjacent = (pos[0] + direction[0], pos[1] + direction[1]) # (xa + xb, ya + yb)
                c = grid[adjacent[1]][adjacent[0]]

                if ord('A') <= ord(c) <= ord('Z'):
                    key_needed = chr(ord(c) + caps_offset)
                    keys_needed.add(key_needed)

                if c != '#' and adjacent not in visited:
                    if ord('a') <= ord(c) <= ord('z'):
                        adj_key_info[c] = (steps_taken, keys_needed, keys_collected.copy())
                        keys_collected.add(c)

                    next_queue.append((adjacent, keys_needed.copy(), keys_collected.copy()))
                    visited.add(adjacent)
        steps_taken += 1
    
    return adj_key_info

@lru_cache(maxsize=None)
def get_steps_to_collect_keys(key, keys_held, keys_n):
    if len(keys_held) == keys_n:
        return 0

    min_steps = float('inf')

    for adj_key, adj_key_info in adj_keys_info[key].items():
        key_dist, keys_needed, keys_collected = adj_key_info
        if (adj_key not in keys_held and 
            keys_needed <= keys_held and 
            keys_collected <= keys_held):
            steps = get_steps_to_collect_keys(
                adj_key, 
                keys_held | frozenset(adj_key),
                keys_n)
            min_steps = min(min_steps, key_dist + steps)
    
    return min_steps

grid = []
with open("2019/input/18.txt") as file:
    grid = [line[:-1] for line in file]

# compile key adjacency matrix including distances, keys_required, 
# and keys_collected when walking from each key to each other key
adj_keys_info = {}
for x in range(len(grid[0])):
    for y in range(len(grid)):
        c = grid[y][x]
        if c == '@' or ord('a') <= ord(c) <= ord('z'):
            adj_keys_info[c] = get_adj_key_info((x, y), grid)


print('Part 1:', get_steps_to_collect_keys('@', frozenset(), len(adj_keys_info) - 1))

@lru_cache(maxsize=None)
def get_steps_to_collect_keys2(keys, keys_held, keys_n):
    if len(keys_held) == keys_n:
        return 0

    min_steps = float('inf')

    for i in range(len(keys)):
        for adj_key, adj_key_info in adj_keys_info[keys[i]].items():
            key_dist, keys_needed, keys_collected = adj_key_info
            if (adj_key not in keys_held and 
                keys_needed <= keys_held and 
                keys_collected <= keys_held):
                next_keys = list(keys)
                next_keys[i] = adj_key
                steps = get_steps_to_collect_keys2(
                    tuple(next_keys),
                    keys_held | frozenset(adj_key),
                    keys_n)
                min_steps = min(min_steps, key_dist + steps)
    
    return min_steps

grid = []
with open("2019/input/18_2.txt") as file:
    grid = [line[:-1] for line in file]

# compile key adjacency matrix including distances, keys_required, 
# and keys_collected when walking from each key to each other key
adj_keys_info = {}
for x in range(len(grid[0])):
    for y in range(len(grid)):
        c = grid[y][x]
        if '1' <= c <= '4' or ord('a') <= ord(c) <= ord('z'):
            adj_keys_info[c] = get_adj_key_info((x, y), grid)

print('Part 2:', get_steps_to_collect_keys2(('1', '2', '3', '4'), frozenset(), len(adj_keys_info) - 4))
