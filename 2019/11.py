from intcode import intcode
from enum import IntEnum

def tuple_add(a, b):
	return tuple(map(lambda x, y: x + y, a, b))

Direction = IntEnum('Direction', 'LEFT UP RIGHT DOWN')
directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]

class HullPaintingRobot:
    def __init__(self, panels_w, panels_h):
        self.panels = [] # 0: black, 1: white, -1: black (unpainted)
        for _ in range(panels_h):
            self.panels.append([-1] * panels_w)
        self.pos = (panels_w // 2, panels_h // 2)
        self.dir = Direction.UP
        self.total_painted = 0
        self.isOutputColor = True
    
    # rotation: 0 = counter-clockwise, 1 = clockwise
    def turn(self, rotation):
        val = self.dir.value - 1
        val += 1 if rotation else -1
        self.dir = Direction((val % len(Direction)) + 1)
    
    def move(self):
        self.pos = tuple_add(self.pos, directions[self.dir.value - 1])
    
    def paint(self, color):
        if self.panels[self.pos[1]][self.pos[0]] == -1:
            self.total_painted += 1
        self.panels[self.pos[1]][self.pos[0]] = color
    
    def get_color(self):
        return self.panels[self.pos[1]][self.pos[0]]

    # called by intcode program 
    def output(self, val):
        if self.isOutputColor:
            self.paint(val)
        else:
            self.turn(val)
            self.move()
        self.isOutputColor = not self.isOutputColor
    
    # called by intcode program 
    def input(self):
        if self.get_color() == 1:
            return 1
        else:
            return 0

    def draw_hull(self):
        for row in self.panels[::-1]:
            for panel in row[len(row)//2:]:
                c = '#' if panel == 1 else '.'
                print(c, end='')
            print('')

robot = HullPaintingRobot(100, 100)

hull_painter_program = []
with open("2019/input/11.txt") as file:
    hull_painter_program = [int(instruction) for instruction in file.read().split(',')]

hull_painter_program += [0] * 1000 # allocate extra memory

intcode.run_intcode(hull_painter_program, intcode.IntcodeState(), robot.input, robot.output)
print('Part 1:', robot.total_painted)

robot = HullPaintingRobot(90, 10)
robot.paint(1) # paint initial panel

intcode.run_intcode(hull_painter_program, intcode.IntcodeState(), robot.input, robot.output) # run the program!

# draw panels
print('Part 2:')
robot.draw_hull()