import math
from collections import Counter

reactions = {}

with open("2019/input/14.txt") as file:
    for line in file:
        arrow = line.find('=>')
        reactants = [tuple(reactant.split(' ')) for reactant in line[:arrow-1].split(', ')]
        product_amt, product_name = tuple(line[arrow+3:-1].split(' '))
        reactions[product_name] = (int(product_amt), reactants)

def get_ore_required(product: str, num: int, stash):
    if product == 'ORE':
        return num
    
    # check to see if we have any product stashed already
    if product in stash:
        if stash[product] >= num:
            # enough stashed
            stash[product] -= num
            return 0
        else:
            # not enough stashed, cover what we can
            num -= stash[product]
            del stash[product]
    
    ore_required = 0
    reaction_amt, reactants = reactions[product]
    num_reactions = math.ceil(num / reaction_amt)
    leftover_product = num_reactions * reaction_amt - num

    for reactant in reactants:
        ore_required += get_ore_required(reactant[1], int(reactant[0]) * num_reactions, stash)
    
    stash[product] += leftover_product
    
    return ore_required
 
def fuelBinarySearch(l, r, ore_supply): 
    if r >= l: 
        mid = l + (r - l)//2
        ore_required = get_ore_required('FUEL', mid, Counter())
        if ore_required > ore_supply: 
            return fuelBinarySearch(l, mid-1, ore_supply) 
        else: 
            return fuelBinarySearch(mid+1, r, ore_supply) 
    else: 
        return r

def fuelExponentialBound(ore_supply):
    bound = 1
    while get_ore_required('FUEL', bound, Counter()) < ore_supply:
        bound *= 2
    
    return bound

part1 = get_ore_required('FUEL', 1, Counter())
print('Part 1: ', part1)

exp_bound = fuelExponentialBound(1000000000000)
part2 = fuelBinarySearch(exp_bound//2, exp_bound, 1000000000000)
print('Part 2: ', part2)
