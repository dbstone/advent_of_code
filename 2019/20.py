import sys
from functools import lru_cache

grid = []
with open("2019/input/20.txt") as file:
    grid = [line[:-1] for line in file]


incomplete_teleporters = {} # {name: position}
teleporters = {} # {entrance_position, exit_position}

for x in range(len(grid[0])):
    for y in range(len(grid)):
        c = grid[y][x]
        if ord('A') <= ord(c) <= ord('Z'):
            dot_pos = None
            name = None
            
            # check right for second char
            if x < len(grid[0])-1:
                c2 = grid[y][x+1]
                if ord('A') <= ord(c2) <= ord('Z'):
                    name = c + c2
                    if x == 0 or (x < len(grid[0])-2 and grid[y][x+2] == '.'):
                        dot_pos = (x+2, y)
                    elif grid[y][x-1] == '.':
                        dot_pos = (x-1, y)
            
            # check down for second char
            if not dot_pos and y < len(grid)-1:
                c2 = grid[y+1][x]
                if ord('A') <= ord(c2) <= ord('Z'):
                    name = c + c2
                    if y == 0 or (y < len(grid)-2 and grid[y+2][x] == '.'):
                        dot_pos = (x, y+2)
                    elif grid[y-1][x] == '.':
                        dot_pos = (x, y-1)
            
            if name and dot_pos:
                if name in incomplete_teleporters:
                    teleporters[dot_pos] = incomplete_teleporters[name]
                    teleporters[incomplete_teleporters[name]] = dot_pos
                else:
                    incomplete_teleporters[name] = dot_pos

# BFS traversal
directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
visited = set()  # set of grid cells that have been visted
adj_key_info = {} # {key: (dist, keys_needed, keys_collected)}

next_queue = []  # BFS queue containing tuples of type (pos, keys_needed)

pos = incomplete_teleporters['AA']
next_queue.append(incomplete_teleporters['AA'])
visited.add(pos)
steps_taken = 0

while next_queue:
    queue = next_queue
    next_queue = []
    while queue:
        pos = queue.pop()
        if pos == incomplete_teleporters['ZZ']:
            print(steps_taken)
            sys.exit()

        adjacents = []
        for direction in directions:
            adjacents.append((pos[0] + direction[0], pos[1] + direction[1])) # (xa + xb, ya + yb)
        
        if pos in teleporters:
            adjacents.append(teleporters[pos])
        
        for adjacent in adjacents:
            c = grid[adjacent[1]][adjacent[0]]

            if c == '.' and adjacent not in visited:
                next_queue.append(adjacent)
                visited.add(adjacent)
    steps_taken += 1
