from intcode import intcode

program_orig = []
with open("2019/input/09.txt") as file:
    program_orig  = [int(instruction) for instruction in file.read().split(',')]

program_orig += [0] * 1000

class BOOST_Controller:
    def __init__(self):
        self.output_val = None
        self.input_val = None
    
    def output(self, val):
        self.output_val = val

controller = BOOST_Controller()

for i in range(1,3):
    intcode.run_intcode(
        program_orig.copy(), 
        intcode.IntcodeState(), 
        lambda: i, 
        lambda x: print('Part ' + str(i) + ': ' + str(x)))
