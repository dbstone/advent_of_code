signal = open('2019/input/16.txt').read()
signal = signal[:-1] # remove \n from end
offset = int(signal[:7]) # first 7 digits give message offset in final signal
signal = [int(c) for c in signal] # convert to integer list
N = len(signal) * 10000

def get_next_phase(sig):
    next_phase = []
    for i in range(len(sig)):
        reps = i+1
        pattern = [0] * reps + [1] * reps + [0] * reps + [-1] * reps
        pattern *= (len(sig) // len(pattern) + 1)
        pattern = pattern[1:len(sig)+1]
        next_phase.append(abs(sum([element * scalar for element, scalar in zip(sig, pattern)])) % 10)
    return next_phase

signal1 = signal.copy()

for i in range(100):
    signal1 = get_next_phase(signal1)

print('Part 1: ', end='')
[print(str(n), end='') for n in signal1[:8]]
print()

# capture full signal, excluding everything before the message offset
signal_big = []
for i in range(offset, N):
    signal_big.append(signal[i % len(signal)])

def get_next_phase_part_2(sig):
    next_phase = [0]

    # cumulative sum from end to start
    for i in reversed(range(len(sig))):
        val = sig[i]
        next_phase.append(next_phase[-1] + val)

    next_phase = next_phase[1:] # remove leading zero
    return [i%10 for i in next_phase[::-1]] # reverse and take only last digit of each

for i in range(100):
    signal_big = get_next_phase_part_2(signal_big)

print('Part 2: ', end='')
[print(str(n), end='') for n in signal_big[:8]]
print()
