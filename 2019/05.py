program = []
with open("2019/input/05.txt") as file:
    program = [int(instruction) for instruction in file.read().split(',')]

eip = 0
while True:
    eip_str = str(program[eip])
    instruction = int(eip_str[-2:] if len(eip_str) >= 2 else eip_str[-1])

    if instruction == 1:
        # add
        param1Immediate = len(eip_str) >= 3 and eip_str[-3] == '1'
        param2Immediate = len(eip_str) >= 4 and eip_str[-4] == '1'
        param1 = program[eip+1] if param1Immediate else program[program[eip+1]]
        param2 = program[eip+2] if param2Immediate else program[program[eip+2]]
        program[program[eip+3]] = param1 + param2
        eip += 4
    elif instruction == 2:
        # multiply
        param1Immediate = len(eip_str) >= 3 and eip_str[-3] == '1'
        param2Immediate = len(eip_str) >= 4 and eip_str[-4] == '1'
        param1 = program[eip+1] if param1Immediate else program[program[eip+1]]
        param2 = program[eip+2] if param2Immediate else program[program[eip+2]]
        program[program[eip+3]] = param1 * param2
        eip += 4
    elif instruction == 3:
        # input
        program[program[eip+1]] = int(input("input: "))
        eip += 2
    elif instruction == 4:
        # output
        param1Immediate = len(eip_str) >= 3 and eip_str[-3] == '1'
        print(program[eip+1] if param1Immediate else program[program[eip+1]])
        eip += 2
    elif instruction == 5:
        # jump-if-true
        param1Immediate = len(eip_str) >= 3 and eip_str[-3] == '1'
        param2Immediate = len(eip_str) >= 4 and eip_str[-4] == '1'
        param1 = program[eip+1] if param1Immediate else program[program[eip+1]]
        param2 = program[eip+2] if param2Immediate else program[program[eip+2]]
        if param1:
            eip = param2
        else:
            eip += 3
    elif instruction == 6:
        # jump-if-false
        param1Immediate = len(eip_str) >= 3 and eip_str[-3] == '1'
        param2Immediate = len(eip_str) >= 4 and eip_str[-4] == '1'
        param1 = program[eip+1] if param1Immediate else program[program[eip+1]]
        param2 = program[eip+2] if param2Immediate else program[program[eip+2]]
        if not param1:
            eip = param2
        else:
            eip += 3
    elif instruction == 7:
        # less than
        param1Immediate = len(eip_str) >= 3 and eip_str[-3] == '1'
        param2Immediate = len(eip_str) >= 4 and eip_str[-4] == '1'
        param1 = program[eip+1] if param1Immediate else program[program[eip+1]]
        param2 = program[eip+2] if param2Immediate else program[program[eip+2]]
        program[program[eip+3]] = 1 if param1 < param2 else 0
        eip += 4
    elif instruction == 8:
        # equals
        param1Immediate = len(eip_str) >= 3 and eip_str[-3] == '1'
        param2Immediate = len(eip_str) >= 4 and eip_str[-4] == '1'
        param1 = program[eip+1] if param1Immediate else program[program[eip+1]]
        param2 = program[eip+2] if param2Immediate else program[program[eip+2]]
        program[program[eip+3]] = 1 if param1 == param2 else 0
        eip += 4
    elif instruction == 99:
        # exit
        break
    else:
        print('ERROR - bad op code: ', str(program[eip]))
        break