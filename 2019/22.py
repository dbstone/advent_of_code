# LCF (linear congruental function) general form: 
# f(x) = ax + b mod m
# defined and stored as tuple (a, b)

# compose two LCF functions: g(f(x))
def compose_lcf(f, g, m):
    a, b = f
    c, d = g
    return ((a * c) % m, (b * c + d) % m)

# compose an LCF with itself k times
def self_compose_lcf(f, m, k):
    # F^k(x) = a^k * x + (b * (1 - a^k)) / (1 - a) mod m
    a, b = f
    a2 = pow(a, k, m)
    b2 = (b * (1 - pow(a, k, m))) * pow(1 - a, m-2, m)
    return a2, b2

# LCF "deal into new stack": 
# f(x) = −x − 1 mod m, so a = −1, b = −1
def compose_deal_into_new_stack(f, N):
    return compose_lcf(f, (-1, -1), N)

# LCF "cut n":
# f(x) = x − n mod m, so a = 1, b = −n
def compose_cut(f, cut_size, N):
    return compose_lcf(f, (1, -cut_size), N)

# LCF "deal with increment n":
# f(x) = n * x mod m, so a = n, b = 0
def compose_deal_with_increment(f, increment, N):
    return compose_lcf(f, (increment, 0), N)

# parse input shuffle operation strings and compose iteratively into a single LCF
def compose_shuffles_lcf(shuffles, N):
    lcf = (1, 0) # start with LCF identity
    for shuffle in shuffles:
        if shuffle.startswith('deal into'):
            lcf = compose_deal_into_new_stack(lcf, N)
        elif shuffle.startswith('cut'):
            lcf = compose_cut(lcf, int(shuffle[shuffle.rfind(' '):]), N)
        elif shuffle.startswith('deal with'):
            lcf = compose_deal_with_increment(lcf, int(shuffle[shuffle.rfind(' '):]), N)
    return lcf

shuffles = [line[:-1] for line in open("2019/input/22.txt")]

deck_size = 10007
lcf = compose_shuffles_lcf(shuffles, deck_size) # compose all shuffle operations into a single LCF
print('Part 1:', (lcf[0] * 2019 + lcf[1]) % deck_size)  # evaluate final LCF

deck_size = 119315717514047
lcf = compose_shuffles_lcf(shuffles, deck_size)

# We need to repeat our shuffle series k=101741582076661 times in reverse. Through 
# experimentation with smaller decks, we know that any deck where deck_size is prime returns 
# to the original order after deck_size - 1 shuffle repetitions. Since deck_size > k, we
# can no-op "backtrack" deck_size - 1 repetitions, then perform deck_size - 1 - k repetitions 
# to reach our target.
# In summary: k reverse-shuffles is equivalent to deck_size - 1 - k shuffles
k = deck_size - 1 - 101741582076661
lcf = self_compose_lcf(lcf, deck_size, k)
print('Part 2:', (lcf[0] * 2020 + lcf[1]) % deck_size)  # evaluate final lcf