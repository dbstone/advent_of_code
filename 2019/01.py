with open("2019/input/01.txt") as f:
    fuel = 0
    lines = f.readlines()
    for line in lines:
        mass = int(line)
        fuel += mass // 3 - 2
    print('Part 1:', fuel)

    total_fuel = 0
    for line in lines:
        fuel = int(line)
        while True:
            fuel = fuel // 3 - 2
            if fuel <= 0:
                break
            total_fuel += fuel
    print('Part 2:', total_fuel)