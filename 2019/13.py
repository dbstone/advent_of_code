from intcode import intcode

class ArcadeCabinet:
    def __init__(self, screen_width: int, screen_height: int):
        self.score = 0
        self.paddle_x = 0
        self.ball_x = 0
        self.output_buffer = [] # [x, y, tile_id]
        self.tiles = []
        for _ in range(screen_height):
            self.tiles.append([0] * screen_width)
    
    def input(self):
        # always move towards ball
        if self.ball_x > self.paddle_x:
            return 1
        elif self.ball_x < self.paddle_x:
            return -1
        else:
            return 0
    
    def output(self, val: int):
        self.output_buffer.append(val)
        if len(self.output_buffer) == 3:
            if self.output_buffer[0] == -1 and self.output_buffer[1] == 0:
                self.score = self.output_buffer[2]
            else:
                self.tiles[self.output_buffer[1]][self.output_buffer[0]] = self.output_buffer[2]
                if self.output_buffer[2] == 3:
                    self.paddle_x = self.output_buffer[0]
                elif self.output_buffer[2] == 4:
                    self.ball_x = self.output_buffer[0]
            self.output_buffer = []
    
    def draw_screen(self):
        for row in self.tiles:
            for c in row:
                print(c, end='')
            print('')

game_program = []
with open("2019/input/13.txt") as file:
    game_program = [int(instruction) for instruction in file.read().split(',')]

game_program += [0] * 1000 # allocate extra memory

cab = ArcadeCabinet(100, 100)
intcode.run_intcode(game_program.copy(), intcode.IntcodeState(), cab.input, cab.output)
num_blocks = sum([row.count(2) for row in cab.tiles])
print('Part 1:', num_blocks)

game_program[0] = 2 # insert coin
cab = ArcadeCabinet(38, 21)
intcode.run_intcode(game_program.copy(), intcode.IntcodeState(), cab.input, cab.output)
print('Part 2:', cab.score)