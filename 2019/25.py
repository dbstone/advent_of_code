from intcode import intcode

class DroidController:
    def __init__(self):
        self.input_buff = ''
        self.input_ptr = 0

    def input(self):
        if self.input_ptr >= len(self.input_buff):
            self.input_buff = input() + '\n'
            self.input_ptr = 0
        out = ord(self.input_buff[self.input_ptr])
        self.input_ptr += 1
        return out

    def output(self, val: int):
        print(chr(val), end='')
    
program = []
with open("2019/input/25.txt") as file:
    program = [int(instruction) for instruction in file.read().split(',')]

program += [0] * 3000 # allocate extra memory

controller = DroidController()

intcode.run_intcode(program, intcode.IntcodeState(), controller.input, controller.output)

# play the game to win!
# (could have wrote an AI to play the game, but it was easy enough anyway)