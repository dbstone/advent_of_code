# puzzle input
row = 2947
col = 3029

# shift to 0-based numbering
row -= 1
col -= 1

# determine how many steps to apply the code generation algorithm
k = ((row + col) ** 2 + row + col) // 2 + col

# code generation algorithm:
# code[n] = code[n-1] * 252533 mod 33554393
# code[0] = 20151125
#
# self-compose this function k times to get explicit form:
# code[k] = 20151125 * (252533^k) mod 33554393
m = 33554393
code_k = (20151125 * pow(252533, k, m)) % m
print(code_k)