with open("2015/input/01.txt") as fp:
    part_2 = None
    n = 0
    sbuf = fp.read()
    for i in range(len(sbuf)):
        c = sbuf[i]
        
        if c == '(':
            n += 1
        elif c == ')':
            n -= 1
        else:
            print("ERROR - bad char:", c)
   
        if not part_2 and n == -1:
            part_2 = i + 1
    
    print("Part 1:", n)
    print("Part 2:", part_2)
