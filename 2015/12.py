import json

def sum_json_nums(jobj, ignoreRed):
    # int
    if type(jobj) is int:
        return jobj
    
    # dict
    if type(jobj) is dict:
        if ignoreRed and "red" in jobj.values():
            return 0
        jobj = list(jobj.values())
        # fall-through
    
    # list
    if type(jobj) is list:
        return sum([sum_json_nums(x, ignoreRed) for x in jobj])

    return 0

with open("2015/input/12.txt") as fp:
    sbuf = fp.read()
    jdoc = json.loads(sbuf)
    print("Part 1:", sum_json_nums(jdoc, False))
    print("Part 2:", sum_json_nums(jdoc, True))
