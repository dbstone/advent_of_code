fs = require('fs')
"use strict"

function main(err, input) {
  if (err) throw err
  let lines = input.split(/\r?\n/)
  let totalPaper = 0
  let totalRibbon = 0

  for (let line of lines) {
    if (line.length > 0)
    {
      let dims = line.split('x').map(Number)
      const [l, w, h] = dims
      const sides = [l*w, w*h, h*l]
      const surface_area = (sides.map(x => x * 2)).reduce((a, b) => a + b, 0)
      totalPaper += surface_area + Math.min(...sides)
      
      dims.sort((a, b) => a - b)
      const perimeter = (dims[0] + dims[1]) * 2
      const volume = dims.reduce((a, b) => a * b, 1)
      totalRibbon += perimeter + volume
    }
  }
  console.log(`Part 1: ${totalPaper}`)
  console.log(`Part 2: ${totalRibbon}`)
}

fs.readFile('2015/input/02.txt', 'utf8', main)