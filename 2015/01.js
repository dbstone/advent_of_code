fs = require('fs')
"use strict"

function main(err, input) {
    if (err) throw err
    let part_2
    let n = 0
    for (let i = 0; i < input.length; i++) {
        const c = input[i]
        if (c === '(') {
            n += 1
        }
        else if (c === ')') {
            n -= 1
        }
        else {
            console.log(`ERROR - bad char: {c}`)
        }

        if (part_2 === undefined && n === -1) {
            part_2 = i + 1
        }
    }

    console.log(`Part 1: ${n}`)
    console.log(`Part 2: ${part_2}`)
}

fs.readFile('2015/input/01.txt', 'utf8', main)