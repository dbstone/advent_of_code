from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=5)

crates, moves = puzzle.input_data.split('\n\n')
N = 9
stacks = [list() for i in range(N)]
for line in crates.splitlines()[:-1]:
    for i in range(len(stacks)):
        c = line[1+4*i]
        if c != ' ':
            stacks[i].append(c)

for move in moves.splitlines():
    _, n, _, src, _, dest = move.split(' ')
    for _ in range(int(n)):
        stacks[int(dest)-1].insert(0, stacks[int(src)-1].pop(0))

p1 = ''
for stack in stacks:
    p1 += stack[0]

print('Part 1:', p1)
# puzzle.answer_a = p1

crates, moves = puzzle.input_data.split('\n\n')
N = 9
stacks = [list() for i in range(N)]
for line in crates.splitlines()[:-1]:
    for i in range(len(stacks)):
        c = line[1+4*i]
        if c != ' ':
            stacks[i].insert(0, c)

for move in moves.splitlines():
    _, n, _, src, _, dest = move.split(' ')
    temp = []
    for _ in range(int(n)):
        temp.insert(0, stacks[int(src)-1].pop())
    for c in temp:
        stacks[int(dest)-1].append(c)

p2 = ''
for stack in stacks:
    p2 += stack[-1]

print('Part 2:', p2)
# puzzle.answer_b = p2
