from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import defaultdict

load_dotenv()
puzzle = Puzzle(year=2022, day=23)

data = puzzle.input_data
with open("2022/test2.txt") as file:
    data = file.read()

rots = [(0, -1), (0, 1), (-1, 0), (1, 0)]

grid = {}
elves = set()

class Elf:
    def __init__(self, pos) -> None:
        self.pos = pos

for y, line in enumerate(data.splitlines()):
    for x, c in enumerate(line):
        if c == '#':
            e = Elf((x, y))
            grid[(x,y)] = e
            elves.add(e)

def get_extents(elves):
    minx, maxx, miny, maxy = 0, 0, 0, 0
    for elf in elves:
        x, y = elf.pos
        minx = min(minx, x)
        maxx = max(maxx, x)
        miny = min(miny, y)
        maxy = max(maxy, y)
    return minx, maxx, miny, maxy

def print_grid(grid, extents):
    minx, maxx, miny, maxy = extents
    for y in range(miny, maxy+1):
        for x in range(minx, maxx+1):
            print('#' if (x,y) in grid else '.', end='')
        print()

r = 0
    
# extents = get_extents(elves)
# print_grid(grid, extents)
# print()

moved = True
rnd = 0
while moved:
    moved = False
    props = defaultdict(int)
    for elf in elves:
        elf.prop = elf.pos
        alone = True
        for xoffset in range(-1,2):
            if alone:
                for yoffset in range(-1,2):
                    if xoffset != 0 or yoffset != 0:
                        neighbor = list(elf.pos)
                        neighbor[0] += xoffset
                        neighbor[1] += yoffset
                        if tuple(neighbor) in grid:
                            alone = False
                            break
        if not alone:
            d = r
            for _ in range(4):
                free = True
                for i in range(-1,2):
                    neighbor = list(rots[d])
                    if neighbor[0] == 0:
                        neighbor[0] = i
                    else:
                        neighbor[1] = i
                    neighbor = (elf.pos[0]+neighbor[0], elf.pos[1]+neighbor[1])
                    if neighbor in grid:
                        free = False
                        break
                if free:
                    elf.prop = (elf.pos[0]+rots[d][0], elf.pos[1]+rots[d][1])
                    props[elf.prop] += 1
                    break
                d = (d + 1) % 4
    new_grid = {}
    for elf in elves:
        if props[elf.prop] == 1:
            moved = True
            elf.pos = elf.prop
        new_grid[elf.pos] = elf
    grid = new_grid
    r = (r + 1) % 4
    rnd += 1
    # extents = get_extents(elves)
    # print_grid(grid, extents)
    # print()

extents = get_extents(elves)
minx, maxx, miny, maxy = extents
W = maxx - minx + 1
H = maxy - miny + 1


part1 = W*H - len(elves)
print('Part 1:', part1)
# puzzle.answer_a = part1

part2 = rnd
print('Part 2:', part2)
# puzzle.answer_b = part2
