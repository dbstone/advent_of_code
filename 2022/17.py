from aocd.models import Puzzle
from dotenv import load_dotenv
from tqdm import tqdm

load_dotenv()
puzzle = Puzzle(year=2022, day=17)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

jets = data

rock_types = []
with open("2022/rock_types.txt") as file:
    for block in file.read().split('\n\n'):
        rock_type = set() # set of coordinates where rock exists, with bottom left as origin
        block = block.splitlines()
        block.reverse()
        for y in range(len(block)):
            for x in range(len(block[0])):
                c = block[y][x]
                if c == '#':
                    rock_type.add((x, y))
        rock_types.append((rock_type, len(block)))

# [print(type) for type in rock_types]
rocks = set() # coords filled by rock
def is_colliding(rock_type, x, y, rocks):
    for pos in rock_type:
        xx = x + pos[0]
        yy = y + pos[1]

        # check floor
        if yy < 0:
            return True

        # check walls
        if xx < 0 or xx > 6:
            return True
        
        # check other rocks
        if (xx, yy) in rocks:
            return True
    
    return False

highest = -1
j = 0
old_highest = 0
old_i = 0
stream = []
for i in tqdm(range(100000000000)):
    rock_type = rock_types[i%len(rock_types)]
    x = 2
    y = highest + 4
    wrap = False
    while True:
        # jet
        xn = x+1 if jets[j%len(jets)] == '>' else x-1
        j += 1
        if j % len(jets) == 0:
            wrap = True

        if not is_colliding(rock_type[0], xn, y, rocks):
            x = xn

        # gravity
        yn = y-1
        if is_colliding(rock_type[0], x, yn, rocks):
            break
        else:
            y = yn

    for pos in rock_type[0]:
        xx = x + pos[0]
        yy = y + pos[1]
        rocks.add((xx,yy))
    highest = max(highest, y + rock_type[1] - 1)
    # if wrap:
    #     stream.append(highest)
        # print(highest-old_highest)
    # if (i-50455*3) % (50455*7) == 0:
    # if (i-50455*3) % (316075) == 0:
    # if (i-1725) % 1600 == 0:
    # if i % 1725 == 0:
        # print(i, highest-old_highest)
        # print(i-old_i, highest-old_highest)
        # old_highest = highest
        # old_i = i
        # printme = []
        # for y in range(highest-10, highest):
        #     line = ''
        #     for x in range(7):
        #         line += '#' if (x,y) in rocks else '.'
        #     printme.insert(0, line)
        # [print(line) for line in printme]
        # print()
    #     print(highest+1)
    # if i % 50455 == 0:
    #     print(highest-old_highest)
    #     old_highest = highest
    # good = True
    # for xtop in range(7):
    #     if (xtop, highest) not in rocks:
    #         good = False
    # if good:
    #     print(i)
    # printme = []
    # for y in range(20):
    #     line = ''
    #     for x in range(7):
    #         line += '#' if (x,y) in rocks else '.'
    #     printme.insert(0, line)
    # [print(line) for line in printme]
    # print(highest+1)

part1 = highest+1
print('Part 1:', part1)
# puzzle.answer_a = part1

# print(len(jets))
# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2

loops = 1000000000000
rem = loops % 1725
print(rem)
div = (loops-1725) // 1725
# # ans = 248 + 424*div + 360
ans = 2723 + 2709*div + 2524
print(ans)
puzzle.answer_b = ans