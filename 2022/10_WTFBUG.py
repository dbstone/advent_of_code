from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=10)

cycle = 1
x = 1

strengths = {}

data = puzzle.input_data.splitlines()

with open("2022/test.txt") as file:
    data = file.read().splitlines()

for line in data:
    if line == 'noop':
        cycle += 1
    else:
        inst, n = line.split()
        x += int(n)
        cycle += 2

    if cycle == 20:
        strengths[20] = x * 20
    elif cycle == 60:
        strengths[60] = x * 60
    elif cycle == 100:
        strengths[100] = x * 100
    elif cycle == 140:
        strengths[140] = x * 140
    elif cycle == 180:
        strengths[180] = x * 180
    elif cycle == 220:
        strengths[220] = x * 220

print(strengths)

part1 = sum(strengths.values())
print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
