from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cache
from itertools import combinations

load_dotenv()
puzzle = Puzzle(year=2022, day=16)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

valves = {}
for line in data.splitlines():
    rate, connections = line.split('; ')
    valve = rate.split()[1]
    rate = int(rate.split('=')[-1])
    if 'valves' in connections:
        connections = connections[connections.find('valves')+7:]
    else:
        connections = connections[connections.find('valve')+6:]
    connections = connections.split(', ')
    valves[valve] = (rate, connections)

dists = {}

# breadth first pathfinding from one valve to another
def get_dist(v1, v2, valves):
    next_queue = set()
    next_queue.add(v1)
    visited = set()
    dist = 0

    while next_queue:
        queue = next_queue
        next_queue = set()
        for v in queue:
            if v == v2:
                return dist
            visited.add(v)
            for vv in valves[v][1]:
                if vv not in visited:
                    next_queue.add(vv)
        dist += 1

# calc dists
for v1 in valves:
    dists[v1] = {}
    for v2 in valves:
        if v1 != v2:
            dists[v1][v2] = get_dist(v1, v2, valves)

# [print(k, v) for k,v in dists.items()]

@cache
def get_max_pressure(t, o, curr):
    if t == 0:
        return 0
    r = 0
    if curr not in o:
        r = max(r, get_max_pressure(t-1, frozenset([curr]) | o, curr)) + valves[curr][0] * (t-1)
    for v in valves[curr][1]:
        r = max(r, get_max_pressure(t-1, o, v))
    return r #+ sum(valves[v][0] for v in o)

@cache
def get_max_pressure2(t, closed, curr):
    r = 0
    for v in closed:
        if v != curr and valves[v][0] > 0:
            t2 = t-dists[curr][v]-1
            if t2 > 0:
                r = max(r, get_max_pressure2(t2, closed - frozenset([curr]), v))

    return r + valves[curr][0] * (t-1)

# @cache
# def get_max_pressure4(t1, t2, closed, curr1, curr2):
#     r = 0
#     for v1 in closed:
#         if v1 != curr1 and v1 != curr2 and valves[v1][0] > 0:
#             for v2 in closed:
#                 if v2 != curr1 and v2 != curr2 and v1 != v2 and valves[v2][0] > 0:
#                     t1n = t1-dists[curr1][v1]-1
#                     t2n = t2-dists[curr2][v2]-1
#                     if t1n > 0 and t2n > 0:
#                         r = max(r, get_max_pressure4(t1n, t2n, closed - frozenset([curr1]) - frozenset([curr2]), v1, v2))
#                     elif t1n > 0:
#                         r = max(r, get_max_pressure2(t1n, closed - frozenset([curr1]), v1))
#                     elif t2n > 0:
#                         r = max(r, get_max_pressure2(t2n, closed - frozenset([curr2]), v2))

#     return r + valves[curr1][0] * (t1-1) + valves[curr2][0] * (t2-1)

@cache
def get_max_pressure4(t1, t2, closed, curr1, curr2):
    r = 0
    for v1, v2 in combinations(closed, 2):
        if v1 != curr1 and v1 != curr2 and valves[v1][0] > 0:
            if v2 != curr1 and v2 != curr2 and v1 != v2 and valves[v2][0] > 0:
                t1n = t1-dists[curr1][v1]-1
                t2n = t2-dists[curr2][v2]-1
                if t1n > 0 and t2n > 0:
                    r = max(r, get_max_pressure4(t1n, t2n, closed - frozenset([curr1]) - frozenset([curr2]), v1, v2))
                elif t1n > 0:
                    r = max(r, get_max_pressure2(t1n, closed - frozenset([curr1]), v1))
                elif t2n > 0:
                    r = max(r, get_max_pressure2(t2n, closed - frozenset([curr2]), v2))

    return r + valves[curr1][0] * (t1-1) + valves[curr2][0] * (t2-1)

@cache
def get_max_pressure3(t, closed, curr):
    if t == 0:
        return 0
    r = 0
    max_val = 0
    vn = None
    for v in closed:
        if v != curr:
            t2 = t-dists[curr][v]-1
            if t2 >= 0:
                value = valves[v][0] * (t2-1)
                if value > max_val:
                    max_val = value
                    vn = v

    if vn: 
        t2 = t-dists[curr][vn]-1
        print(vn)
        return get_max_pressure3(t2, closed - frozenset([curr]), vn) + valves[curr][0] * (t-1)
    else:
        return valves[curr][0] * (t-1)

# part1 = get_max_pressure(30, frozenset(), 'AA')
part1 = get_max_pressure2(31, frozenset(valves.keys()), 'AA')
# part1 = get_max_pressure3(31, frozenset(valves.keys()), 'AA')
print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = get_max_pressure4(27, 27, frozenset(valves.keys()), 'AA', 'AA')
# print('Part 2:', part2)
# puzzle.answer_b = part2
