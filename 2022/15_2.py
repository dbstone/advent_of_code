from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=15)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

def add_range_to_list(a, s):
    s.append(a)
    s2 = []
    for begin,end in sorted(s):
        if s2 and s2[-1][1] >= begin - 1:
            s2[-1][1] = max(s2[-1][1], end)
        else:
            s2.append([begin, end])
    for i in range(len(s2)):
        s2[i] = (s2[i][0], s2[i][1])
    return s2

def add_range_to_set(a, s):
    a1, a2 = a
    added = False
    sc = s.copy()
    for b in sc:
        s.remove(b)
        concat = concat_ranges(a, b)
        if concat:
            added = True
            add_range_to_set(concat, s)
            break
        else:
            s.add(b)

    if not added:
        s.add(a)

def concat_ranges(a, b):
    a1, a2 = a
    b1, b2 = b
    if a1 <= b2 and b1 <= a2:
        # overlap
        return min(a1, b1), max(a2, b2)
    return None

def p1(target, sensors):
    rs = []

    for sensor, extents, dist in sensors:
        sx, sy = sensor
        s1, s2 = extents
        if target >= s1 and target <= s2:
            target_dist = abs(target - sy)
            diff = dist - target_dist
            if diff >= 0:
                rs = add_range_to_list((sx-diff, sx+diff), rs)
    
    return rs

sensors = []
for line in data.splitlines():
    s, b = line.split(': ')
    s = s[10:]
    sx, sy = s.split(', ')
    sx = int(sx.split('=')[-1])
    sy = int(sy.split('=')[-1])
    
    b = b.split(' is at ')[-1]
    bx, by = b.split(', ')
    bx = int(bx.split('=')[-1])
    by = int(by.split('=')[-1])

    dist = abs((bx-sx)) + abs((by-sy))
    sensors.append(((sx, sy), (sy-dist, sy+dist), dist))

# for line in data.splitlines():
#     s, b = line.split(': ')
#     s = s[10:]
#     sx, sy = s.split(', ')
#     sx = int(sx.split('=')[-1])
#     sy = int(sy.split('=')[-1])
    
#     b = b.split(' is at ')[-1]
#     bx, by = b.split(', ')
#     bx = int(bx.split('=')[-1])
#     by = int(by.split('=')[-1])

#     # print(sx, sy, bx, by)

#     dist = abs((bx-sx)) + abs((by-sy))
#     sensors.append((sx, sy))
#     dists.append(dist)
#     if target in range(sy-dist, sy+dist+1):
#         target_dist = abs(target - sy)
#         diff = dist - target_dist
#         if diff >= 0:
#             for x in range(sx-diff, sx+diff+1):
#                 if not (x == bx and target == by):
#                     cannot.add(x)

rs = p1(2000000, sensors)
cannot = 0
for a, b in rs:
    cannot += (b-a)+1

part1 = cannot-1
print('Part 1:', part1)
# puzzle.answer_a = part1

# candidates = []
# for x in range(4000001):
#     good = True
#     for s,e in rs:
#         if x >= s and x <= e:
#             good = False
#             break
#     if good:
#         candidates.append(x)

# print(candidates)

# rrs = []
# for y in range(4000001):
#     rr = p1(y, sensors)
#     if len(rr) > 1:
#         print(rr)
#     r1, r2 = rr[0]
#     if r1 > 0 or r2 < 4000001:
#         print(y)
#         rrs.append((r1, r2))

# print(rrs)

# print(rrs)

# minx = min(x for x in cannot)
# maxx = max(x for x in cannot)
# print(minx, maxx)

# for i in range(len(dists)):
#     print(sensors[i], dists[i])

# cx = set()

# target = 2000000
# sensors = []
# dists = []

# for line in data.splitlines():
#     s, b = line.split(': ')
#     s = s[10:]
#     sx, sy = s.split(', ')
#     sx = int(sx.split('=')[-1])
#     sy = int(sy.split('=')[-1])
    
#     b = b.split(' is at ')[-1]
#     bx, by = b.split(', ')
#     bx = int(bx.split('=')[-1])
#     by = int(by.split('=')[-1])

#     dist = abs((bx-sx)) + abs((by-sy))
#     sensors.append((sx, sy))
#     dists.append(dist)
    
#     if target in range(sy-dist, sy+dist+1):
#         target_dist = abs(target - sy)
#         diff = dist - target_dist
#         if diff >= 0:
#             for x in range(sx-diff, sx+diff+1):
#                 if not (x == bx and by == target):
#                     cx.add(x)

# xs = []
# for x in range(4000001):
#     if x not in cx:
#         xs.append(x)

# cy = set()

# target = xs[0]
# sensors = []
# dists = []

# for line in data.splitlines():
#     s, b = line.split(': ')
#     s = s[10:]
#     sx, sy = s.split(', ')
#     sx = int(sx.split('=')[-1])
#     sy = int(sy.split('=')[-1])
    
#     b = b.split(' is at ')[-1]
#     bx, by = b.split(', ')
#     bx = int(bx.split('=')[-1])
#     by = int(by.split('=')[-1])

#     dist = abs((bx-sx)) + abs((by-sy))
#     sensors.append((sx, sy))
#     dists.append(dist)
    
#     if target in range(sx-dist, sx+dist+1):
#         target_dist = abs(target - sx)
#         diff = dist - target_dist
#         if diff >= 0:
#             for y in range(sy-diff, sy+diff+1):
#                 if not (y == by and bx == target):
#                     cy.add(y)

# ys = []
# for y in range(4000001):
#     if y not in cy:
#         ys.append(y)

# print(xs)
# print(ys)

part2 = 2844848 * 4000000 + 2658764
print('Part 2:', part2)
puzzle.answer_b = part2
