from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=21)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

monkeys = {}
for line in data.splitlines():
    name, val = line.split(': ')
    monkeys[name] = val

def yell(name):
    val = monkeys[name]
    if val.isnumeric():
        return val
    else:
        a, op, b = val.split()
        val = val.replace(a, yell(a))
        val = val.replace(b, yell(b))
        return str(eval(val))

part1 = int(float(yell('root')))
print('Part 1:', part1)
# puzzle.answer_a = part1

wqpn = int(float(yell('wqpn')))

def test(n):
    monkeys['humn'] = n
    return int(float(yell('cwtl')))

# binary search to find correct humn yell

# first, double to find bounds
high = 1 
cwtl = float('inf')   
prev = 0
while cwtl > wqpn:
    high *= 2
    cwtl = test(str(high))
low = high // 2

# now binary search
mid = 0
while True:
    mid = (low + high) // 2
    v = test(str(mid))
    if v == wqpn:
        break
    elif v > wqpn:
        low = mid
    elif v < wqpn:
        high = mid

part2 = mid
print('Part 2:', part2)
puzzle.answer_b = part2
