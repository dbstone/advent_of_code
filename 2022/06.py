from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=6)

def p1(data, n):
    lastn = []
    for i in range(len(data)):
        c = data[i]
        lastn.append(c)
        if len(lastn) > n-1:
            # check for unique
            match = False
            for j in range(len(lastn)):
                if match: break
                cc = lastn[j]
                for k in range(j, len(lastn)):
                    if match: break
                    if j != k:
                        if cc == lastn[k]:
                            match = True
            
            if not match:
                return i+1
            
            lastn.pop(0)

part1 = p1(puzzle.input_data, 4)
print('Part 1:', part1)
puzzle.answer_a = part1

part2 = p1(puzzle.input_data, 14)
print('Part 2:', part2)
puzzle.answer_b = part2
