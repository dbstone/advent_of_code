from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2022, day=20)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

class Node:
    def __init__(self, val) -> None:
        self.val = val

def parse_data(data, is_part2):
    nodes = [Node(int(line) * (811589153 if is_part2 else 1)) for line in data.splitlines()]
    nodes[0].prev = nodes[-1]
    nodes[-1].next = nodes[0]
    for i in range(len(nodes)-1):
        nodes[i].next = nodes[i+1]
        nodes[i+1].prev = nodes[i]
    return nodes

def mix(nodes):
    for i, node in enumerate(nodes):
        dest = node
        if node.val < 0:
            for _ in range(abs(node.val)):
                dest = dest.prev
            # pop
            node.prev.next = node.next
            node.next.prev = node.prev
            # insert
            node.prev = dest.prev
            dest.prev.next = node
            dest.prev = node
            node.next = dest
        elif node.val > 0:
            for _ in range(abs(node.val)):
                dest = dest.next
            # pop
            node.prev.next = node.next
            node.next.prev = node.prev
            # insert
            node.prev = dest
            node.next = dest.next
            dest.next.prev = node
            dest.next = node

def grove_sum(nodes):
    curr = None
    for node in nodes:
        if node.val == 0:
            curr = node
            break
    new = []
    for i in range(len(nodes)):
        new.append(curr)
        curr = curr.next

    return new[1000%len(new)].val + new[2000%len(new)].val + new[3000%len(new)].val

nodes = parse_data(data, False)
mix(nodes)
part1 = grove_sum(nodes)
print('Part 1:', part1)
puzzle.answer_a = part1

nodes = [Node(int(line)*811589153) for line in data.splitlines()]
nodes = parse_data(data, True)
for _ in range(10):
    mix(nodes)
part2 = grove_sum(nodes)
print('Part 2:', part2)
puzzle.answer_b = part2
