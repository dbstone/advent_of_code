from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=4)

p1 = 0
for line in puzzle.input_data.splitlines():
    a, b = line.split(',')
    a1, a2 = (int(i) for i in a.split('-'))
    b1, b2 = (int(i) for i in b.split('-'))
    if (a1 >= b1 and a2 <= b2) or (b1 >= a1 and b2 <= a2):
        p1 += 1

print('Part 1:', p1)
puzzle.answer_a = p1

p2 = 0
for line in puzzle.input_data.splitlines():
    a, b = line.split(',')
    a1, a2 = (int(i) for i in a.split('-'))
    b1, b2 = (int(i) for i in b.split('-'))
    if (a1 >= b1 and a1 <= b2) or (b1 >= a1 and b1 <= a2):
        p2 += 1

print('Part 2:', p2)
puzzle.answer_b = p2
