from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=3)

def get_priority(c):
    n = ord(c)
    if n >= ord('a'):
        return n - ord('a') + 1
    else:
        return  n - ord('A') + 27

p1 = 0
for line in puzzle.input_data.splitlines():
    l = line[0:len(line)//2]
    r = line[len(line)//2:len(line)]
    for c in l:
        if c in r:
            p1 += get_priority(c)
            break

print('Part 1:', p1)
puzzle.answer_a = p1

p2 = 0
lines = puzzle.input_data.splitlines()
for i in range(0, len(lines), 3):
    a, b, c = lines[i], lines[i+1], lines[i+2]
    for t in a:
        if t in b and t in c:
            p2 += get_priority(t)
            break

print('Part 2:', p2)
puzzle.answer_b = p2
