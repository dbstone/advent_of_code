from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=19)

data = puzzle.input_data
with open("2022/test.txt") as file:
    data = file.read()

# def ore_produced(r, t):

def can_make_n_resource(blueprint, n, resource, t):
    if t <= 0:
        return False
    if n == 0:
        return True
    if resource == resources['ore']:
        n //= blueprint[0][0]
        if n <= t:
            return True
        r = 1
        vm = 0
        while True:
            v = t-(r-1)
            for i in range(r-1):
                v += t-(2+i)
            if v >= n:
                return True
            if v >= vm:
                vm = v
            else:
                break
            r += 1
        return vm >= n

    cost = blueprint[resource]
    return all(can_make_n_resource(blueprint, n*n2, i, t) for i, n2 in enumerate(cost))

# def can_open_n_geodes(blueprint, n, minutes):
#     return False

def max_geodes(blueprint, t):
    n = 1
    while can_make_n_resource(blueprint, n, resources['geode'], t):
        n += 1
    return n

resources = {'ore':0, 'clay':1, 'obsidian':2, 'geode':3}

# return tuple(ore, clay, obsidian)
def parse_cost(cost):
    cost = cost.split('costs ')[1]
    cost = cost.split(' and ')
    r = [0, 0, 0, 0]
    for c in cost:
        n, resource = c.split()
        r[resources[resource]] = int(n)
    return tuple(r)

bps = []
for line in data.splitlines():
    n, costs = line.split(': ')
    costs = costs[:-1].split('. ')
    bps.append(tuple(parse_cost(costs[i]) for i in range(4)))

part1 = sum((i+1) * max_geodes(bp, 24) for i, bp in enumerate(bps))
print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2

# maximize g = gr
# or = 4o
# cr = 2o
# br = 3o + 14c
# gr = 2o + 7b
# o = or*t

