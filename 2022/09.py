from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=9)

visited = set()

tx, ty = 0, 0
hx, hy = 0, 0

for line in puzzle.input_data.splitlines():
    d, n = line.split()
    for _ in range(int(n)):
        if d == 'L':
            hx -= 1
        elif d == 'R':
            hx += 1
        elif d == 'U':
            hy -= 1
        elif d == 'D':
            hy += 1
        
        if hx - tx > 1:
            tx += 1
            ty = hy
        elif tx - hx > 1:
            tx -= 1
            ty = hy
        
        if hy - ty > 1:
            ty += 1
            tx = hx
        elif ty - hy > 1:
            ty -= 1
            tx = hx
        
        visited.add((tx, ty))

part1 = len(visited)
print('Part 1:', part1)
puzzle.answer_a = part1

visited = set()

x = [0 for _ in range(10)]
y = [0 for _ in range(10)]

for line in puzzle.input_data.splitlines():
    d, n = line.split()
    for _ in range(int(n)):
        if d == 'L':
            x[0] -= 1
        elif d == 'R':
            x[0] += 1
        elif d == 'U':
            y[0] -= 1
        elif d == 'D':
            y[0] += 1
            
        for h in range(len(x)-1):
            t = h+1
            
            if x[h] - x[t] > 1:
                x[t] += 1
                if y[h] - y[t] > 0:
                    y[t] += 1
                elif y[t] - y[h] > 0:
                    y[t] -= 1
            elif x[t] - x[h] > 1:
                x[t] -= 1
                if y[h] - y[t] > 0:
                    y[t] += 1
                elif y[t] - y[h] > 0:
                    y[t] -= 1
            
            if y[h] - y[t] > 1:
                y[t] += 1
                if x[h] - x[t] > 0:
                    x[t] += 1
                elif x[t] - x[h] > 0:
                    x[t] -= 1
            elif y[t] - y[h] > 1:
                y[t] -= 1
                if x[h] - x[t] > 0:
                    x[t] += 1
                elif x[t] - x[h] > 0:
                    x[t] -= 1
        
        visited.add((x[9], y[9]))

part2 = len(visited)
print('Part 2:', part2)
puzzle.answer_b = part2
