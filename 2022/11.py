from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=11)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

class Monkey:
    def __init__(self, items, op, factor, test, true, false, monkeys, is_part1):
        self.items = items
        self.op = op
        self.factor = factor
        self.test = test
        self.true = true
        self.false = false
        self.monkeys = monkeys
        self.inspects = 0
        self.is_part1 = is_part1
    
    def tick(self):
        while self.items:
            self.inspects += 1
            worry = self.items.pop(0)
            
            if self.op == '*':
                worry *= self.factor
            elif self.op == '+':
                worry += self.factor
            elif self.op == '**':
                worry *= worry
            else:
                print('ERROR')
            
            # if self.is_part1:
            #     worry //= 3
            if worry > 9699690:
                worry = worry % 9699690

            if worry % self.test == 0:
                self.monkeys[self.true].items.append(worry)
            else:
                self.monkeys[self.false].items.append(worry)

monkeys = []

for block in data.split('\n\n'):
    lines = block.splitlines()
    items = [int(item) for item in lines[1].split(': ')[1].split(', ')]
    op = lines[2].split()[-2]
    factor = lines[2].split()[-1]
    if factor == 'old':
        op = '**'
    else:
        factor = int(factor)
    test = int(lines[3].split()[-1])
    true = int(lines[4].split()[-1])
    false = int(lines[5].split()[-1])
    monkeys.append(Monkey(items, op, factor, test, true, false, monkeys, True))

for _ in range(10000):
    for monkey in monkeys:
        monkey.tick()

inspects = []
for monkey in monkeys:
    inspects.append(monkey.inspects)

inspects.sort()

part1 = inspects[-1] * inspects[-2]
print('Part 1:', part1)
# puzzle.answer_a = part1

monkeys = []

for block in data.split('\n\n'):
    lines = block.splitlines()
    items = [int(item) for item in lines[1].split(': ')[1].split(', ')]
    op = lines[2].split()[-2]
    factor = lines[2].split()[-1]
    if factor == 'old':
        op = '**'
    else:
        factor = int(factor)
    test = int(lines[3].split()[-1])
    true = int(lines[4].split()[-1])
    false = int(lines[5].split()[-1])
    monkeys.append(Monkey(items, op, factor, test, true, false, monkeys, False))

for _ in range(10000):
    for monkey in monkeys:
        # monkey.inspects = 0
        monkey.tick()
    
#     inspects = []
#     for monkey in monkeys:
#         inspects.append(monkey.inspects)
    
#     if inspects[0] == 2 and inspects[1] == 4 and inspects[2] == 3 and inspects[3] == 6:
#         print('yay')

#     for monkey in monkeys:
#         print(monkey.inspects, end=',')
#     print()

inspects = []
for monkey in monkeys:
    inspects.append(monkey.inspects)

inspects.sort()

part2 = inspects[-1] * inspects[-2]
print('Part 2:', part2)
# puzzle.answer_b = part2
