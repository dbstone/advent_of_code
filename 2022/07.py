from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=7)

class File:
    def __init__(self, name, size, parent):
        self.name = name
        self.size = size
        self.parent = parent
        self.contents = {}

root = File('/', None, None)
curr = root

for line in puzzle.input_data.splitlines()[1:]:
    tok = line.split()
    if tok[0] == '$':
        if tok[1] == 'cd':
            if tok[2] == '..':
                curr = curr.parent
            elif tok[2] == '/':
                curr = root
            else:
                curr = curr.contents[tok[2]]
    elif tok[0] == 'dir':
        # dir
        if tok[1] not in curr.contents:
            curr.contents[tok[1]] = File(tok[1], None, curr)
    elif tok[0].isnumeric():
        # file
        if tok[1] not in curr.contents:
            curr.contents[tok[1]] = File(tok[1], int(tok[0]), curr)
    else:
        print('ERROR')


def get_size(d, sizes, is_part2):
    if d.contents:
        d.size = 0
        for f in d.contents.values():
            d.size += get_size(f, sizes, is_part2)
        if d.size <= 100000 or is_part2:
            sizes.append(d.size)
    return d.size

sizes = []
get_size(root, sizes, False)

part1 = sum(sizes)
print('Part 1:', part1)
puzzle.answer_a = part1

sizes = []
get_size(root, sizes, True)

unused = 70000000 - root.size
to_delete = 30000000 - unused

part2 = min([s for s in sizes if s >= to_delete])
print('Part 2:', part2)
puzzle.answer_b = part2
