from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import lru_cache

load_dotenv()
puzzle = Puzzle(year=2022, day=12)

data = puzzle.input_data

grid = []
start = None
end = None

for y, line in enumerate(data.splitlines()):
    row = []
    for x, c in enumerate(line):
        if c == 'S':
            start = (x, y)
            row.append(0)
        elif c == 'E':
            end = (x, y)
            row.append(25)
        else:
            row.append(ord(c) - ord('a'))
    grid.append(row)

def is_in_bounds(x, y, w, h):
    if (x < 0 or
        x >= w or
        y < 0 or
        y >= h):
        return False
    return True

directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]

def find_dist(start, end, grid, is_part2):
    next_queue = set()
    next_queue.add(start)
    visited = set()
    dist = 0

    while next_queue:
        queue = next_queue
        next_queue = set()
        for pos in queue:
            if is_part2:
                if grid[pos[1]][pos[0]] == 0:
                    return dist
            else:
                if pos == end:
                    return dist
            visited.add(pos)
            for direction in directions:
                adjacent = (pos[0] + direction[0], pos[1] + direction[1]) # (xa + xb, ya + yb)
                if adjacent not in visited:
                    if is_in_bounds(adjacent[0], adjacent[1], len(grid[0]), len(grid)):
                        if is_part2:
                            if grid[pos[1]][pos[0]] - grid[adjacent[1]][adjacent[0]] < 2:
                                next_queue.add(adjacent)
                        else:
                            if grid[adjacent[1]][adjacent[0]] - grid[pos[1]][pos[0]] < 2:
                                next_queue.add(adjacent)
        dist += 1

part1 = find_dist(start, end, grid, False)
print('Part 1:', part1)
puzzle.answer_a = part1

part2 = find_dist(end, (-1, -1), grid, True)
print('Part 2:', part2)
puzzle.answer_b = part2

