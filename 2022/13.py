from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cmp_to_key

load_dotenv()
puzzle = Puzzle(year=2022, day=13)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

def splitlist(l):
    if l == '[]':
        return []

    if l[0] == '[':
        l = l[1:-1]
    else:
        return [l]

    r = []
    o = 0
    s = 0
    for i, c in enumerate(l):
        if c == '[':
            o += 1
        elif c == ']':
            o -= 1
        elif c == ',':
            if o == 0:
                r.append(l[s:i])
                s = i+1
    r.append(l[s:])

    return r

def is_ordered(a, b):
    if a.isnumeric() and b.isnumeric():
        if int(a) == int(b):
            return -1
        elif int(a) > int(b):
            return 0
        elif int(a) < int(b):
            return 1
    
    aa = splitlist(a)
    bb = splitlist(b)
    
    while aa and bb:
        o = is_ordered(aa.pop(0), bb.pop(0))
        if o != -1:
            return o
    if aa:
        return 0
    elif bb:
        return 1
    else:
        return -1

ordered = []
for i, pair in enumerate(data.split('\n\n')):
    if is_ordered(*pair.split('\n')):
        ordered.append(i+1)

part1 = sum(ordered)
print('Part 1:', part1)
puzzle.answer_a = part1

newdata = []
for block in data.split('\n\n'):
    for line in block.splitlines():
        newdata.append(line)
newdata.append('[[2]]')
newdata.append('[[6]]')
newdata.sort(key=cmp_to_key(lambda a,b: -1 if is_ordered(a,b) else 1))

part2 = (newdata.index('[[2]]') + 1) * (newdata.index('[[6]]') + 1)
print('Part 2:', part2)
puzzle.answer_b = part2
