from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2022, day=24)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

data = [line[1:-1] for line in data.splitlines()[1:-1]]

H = len(data)
W = len(data[0])

# class Bliz:
#     def __init__(self, pos, vel) -> None:
#         self.pos = pos
#         self.vel = vel

blizs = set()
occupieds = [set()]
# occupied = set() # lists grid coords where at least one bliz exists

velmap = {'>': complex(1,0), 'v': complex(0,1), '<': complex(-1,0), '^': complex(0,-1)}

for y in range(H):
    for x in range(W):
        c = data[y][x]
        if c == '.':
            continue
        pos = complex(x,y)
        vel = velmap[c]
        bliz = Bliz(pos, vel)
        blizs.add(bliz)
        occupieds[0].add(pos)

for _ in range(100):
    occupied = set()
    for bliz in blizs:
        bliz.pos += bliz.vel
        x = int(bliz.pos.real)
        y = int(bliz.pos.imag)
        x %= W
        y %= H
        bliz.pos = complex(x,y)
        occupied.add(bliz.pos)
    occupieds.append(occupied)

# part1 = None
# print('Part 1:', part1)
# puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
