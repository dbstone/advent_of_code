from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=8)

visible = set()

grid = puzzle.input_data.splitlines()

W = len(grid[0])
H = len(grid)

for r in range(len(grid)):
    tallest = -1
    for c in range(len(grid[0])):
        tree = int(grid[r][c])
        if tree > tallest:
            tallest = tree
            visible.add((c, r))

    tallest = -1
    for c in reversed(range(len(grid[0]))):
        tree = int(grid[r][c])
        if tree > tallest:
            tallest = tree
            visible.add((c, r))

for c in range(len(grid[0])):
    tallest = -1
    for r in range(len(grid)):
        tree = int(grid[r][c])
        if tree > tallest:
            tallest = tree
            visible.add((c, r))

    tallest = -1
    for r in reversed(range(len(grid))):
        tree = int(grid[r][c])
        if tree > tallest:
            tallest = tree
            visible.add((c, r))

part1 = len(visible)
print('Part 1:', part1)
puzzle.answer_a = part1

def is_in_bounds(x, y, w, h):
    if (x < 0 or
        x >= w or
        y < 0 or
        y >= h):
        return False
    return True

scores = []

for y in range(len(grid)):
    for x in range(len(grid[0])):
        tree = int(grid[y][x])
        
        left = 0
        yy, xx = y, x
        while (True):
            xx -= 1
            if not is_in_bounds(xx, yy, W, H):
                break
            left += 1
            if int(grid[yy][xx]) >= tree:
                break

        right = 0
        yy, xx = y, x
        while (True):
            xx += 1
            if not is_in_bounds(xx, yy, W, H):
                break
            right += 1
            if int(grid[yy][xx]) >= tree:
                break
      
        up = 0
        yy, xx = y, x
        while (True):
            yy -= 1
            if not is_in_bounds(xx, yy, W, H):
                break
            up += 1
            if int(grid[yy][xx]) >= tree:
                break
       
        down = 0
        yy, xx = y, x
        while (True):
            yy += 1
            if not is_in_bounds(xx, yy, W, H):
                break
            down += 1
            if int(grid[yy][xx]) >= tree:
                break
        
        scores.append(left * right * up * down)

part2 = max(scores)
print('Part 2:', part2)
puzzle.answer_b = part2
