from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=1)

highest = 0
for elf in puzzle.input_data.split('\n\n'):
    calories = sum(int(i) for i in elf.split('\n'))
    highest = max(highest, calories)

print('Part 1:', highest)
puzzle.answer_a = highest

top3 = [0, 0, 0]
for elf in puzzle.input_data.split('\n\n'):
    calories = sum(int(i) for i in elf.split('\n'))
    if calories > top3[0]:
        top3[0] = calories
        top3.sort()

p2 = sum(top3)
print('Part 2:', p2)
puzzle.answer_b = p2


