from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=14)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

rock = set()

for line in data.splitlines():
    points = line.split(' -> ')
    for i, start in enumerate(points[:-1]):
        x1, y1 = (int(c) for c in start.split(','))
        x2, y2 = (int(c) for c in points[i+1].split(','))
        if x1 == x2:
            for y in range(min(y1, y2), max(y1, y2)+1):
                rock.add((x1, y))
        else:
            for x in range(min(x1, x2), max(x1, x2)+1):
                rock.add((x, y1))

rock_max = max(y for x, y in rock)
sand = set()

done = False
while not done:
    sx, sy = 500, 0
    while True:
        if sy > rock_max:
            done = True
            break
        below = (sx, sy+1)
        downleft = (sx-1, sy+1)
        downright = (sx+1, sy+1)
        if below not in rock and below not in sand:
            sx, sy = below
        elif downleft not in rock and downleft not in sand:
            sx, sy = downleft
        elif downright not in rock and downright not in sand:
            sx, sy = downright
        else:
            sand.add((sx, sy))
            break

# # visualization
# sand_min_y = min(y for x, y in sand)
# sand_min_x = min(x for x, y in sand)
# sand_max_y = max(y for x, y in sand)
# sand_max_x = max(x for x, y in sand)
# for y in range(sand_min_y, sand_max_y+1):
#     for x in range(sand_min_x, sand_max_x+1):
#         if (x, y) in sand:
#             print('o', end='')
#         elif (x, y) in rock:
#             print('#', end='')
#         else:
#             print('.', end='')
#     print()

part1 = len(sand)
print('Part 1:', part1)
puzzle.answer_a = part1

floor = 2 + rock_max

sand = set()

done = False
while (500, 0) not in sand:
    sx, sy = 500, 0
    while True:
        if sy == floor-1:
            sand.add((sx, sy))
            break
        below = (sx, sy+1)
        downleft = (sx-1, sy+1)
        downright = (sx+1, sy+1)
        if below not in rock and below not in sand:
            sx, sy = below
        elif downleft not in rock and downleft not in sand:
            sx, sy = downleft
        elif downright not in rock and downright not in sand:
            sx, sy = downright
        else:
            sand.add((sx, sy))
            break

part2 = len(sand)
print('Part 2:', part2)
puzzle.answer_b = part2
