from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=22)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

map_data, dirs = data.split('\n\n')

mp = {}
xmins = []
xmaxs = []
for y, line in enumerate(map_data.splitlines()):
    minx = -1
    mp[y] = {}
    for x, c in enumerate(line):
        if minx == -1 and c != ' ':
            minx = x
        if c == '#':
            mp[y][x] = True
        elif c == '.':
            mp[y][x] = False
    xmins.append(minx)
    xmaxs.append(x)

ymins = {}
ymaxs = {}
for y in range(len(map_data)):
    for x in range(max(xmaxs)+1):
        if y in mp and x in mp[y]:
            ymaxs[x] = y
            if x not in ymins:
                ymins[x] = y

rots = [(1, 0), (0, 1), (-1, 0), (0, -1)]
ds = {'R': 0, 'D': 1, 'L': 2, 'U': 3}

x = xmins[0]
y = 0
r = 0

def scale_rot(rot, scalar):
    rot = list(rot)
    rot[0] *= scalar
    rot[1] *= scalar
    return tuple(rot)

n = ''
i = 0
while i < len(dirs) + 1:
    if i < len(dirs):
        c = dirs[i]
    else:
        c = None
    if c in ['R','L'] or i == len(dirs):
        # vec = scale_rot(rots[r], int(n))
        rot = rots[r]
        if rot[0]:
            # x movement
            cx = x
            for _ in range(int(n)):
                cx = ((cx - xmins[y] + rot[0]) % (xmaxs[y] - xmins[y] + 1)) + xmins[y]
                # cx = ((x - xmins[y] + vec[0]) % (xmaxs[y] - xmins[y])) + xmins[y]
                if mp[y][cx]:
                    break
                else:
                    x = cx
        else:
            # y movement
            cy = y
            for _ in range(int(n)):
                cy = ((y - ymins[x] + rot[1]) % (ymaxs[x] - ymins[x] + 1)) + ymins[x]
            # cy = ((y - ymins[x] + vec[1]) % (ymaxs[x] - ymins[x])) + ymins[x]
                if mp[cy][x]:
                    break
                else:
                    y = cy
        if c == 'R':
            r = (r + 1) % 4
        elif c == 'L':
            r = (r - 1) % 4
        n = ''
    else:
        n += c
    i += 1

y += 1
x += 1
part1 = 1000 * y + 4 * x + r
print('Part 1:', part1)
puzzle.answer_a = part1

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
