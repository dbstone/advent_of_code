from aocd.models import Puzzle
from dotenv import load_dotenv
from functools import cache

load_dotenv()
puzzle = Puzzle(year=2022, day=16)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

valves = {}
for line in data.splitlines():
    rate, connections = line.split('; ')
    valve = rate.split()[1]
    rate = int(rate.split('=')[-1])
    if 'valves' in connections:
        connections = connections[connections.find('valves')+7:]
    else:
        connections = connections[connections.find('valve')+6:]
    connections = connections.split(', ')
    valves[valve] = (rate, connections)

@cache
def get_max_pressure(t, o, curr):
    if t == 0:
        return 0
    r = 0
    if curr not in o:
        r = max(r, get_max_pressure(t-1, frozenset([curr]) | o, curr)) + valves[curr][0] * (t-1)
    for v in valves[curr][1]:
        r = max(r, get_max_pressure(t-1, o, v))
    return r #+ sum(valves[v][0] for v in o)

part1 = get_max_pressure(30, frozenset(), 'AA')
print('Part 1:', part1)
# puzzle.answer_a = part1

# TODO calculate distances from each valve to each valve

# part2 = None
# print('Part 2:', part2)
# puzzle.answer_b = part2
