from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2022, day=18)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

cubes = set(eval(cube) for cube in data.splitlines())

def get_surface_area(cubes, is_part2, outside):
    area = 0
    for cube in cubes:
        for axis in range(3):
            for offset in range(-1, 2):
                if offset != 0:
                    adj = list(cube)
                    adj[axis] = offset + cube[axis]
                    adj = tuple(adj)
                    if adj not in cubes:
                        if (not is_part2) or adj in outside:
                            area += 1
    return area
        
part1 = get_surface_area(cubes, False, None)
print('Part 1:', part1)
# puzzle.answer_a = part1

mins = [float('inf'), float('inf'), float('inf')]
maxs = [0, 0, 0]
for cube in cubes:
    for axis in range(3):
        mins[axis] = min(mins[axis], cube[axis])
        maxs[axis] = max(maxs[axis], cube[axis])

def is_valid(pos):
    x, y, z = pos
    if (x < mins[0]-1 or
        x > maxs[0]+1 or
        y < mins[1]-1 or
        y > maxs[1]+1 or
        z < mins[2]-1 or
        z > maxs[2]+1):
        return False
    return True

# BFS flood fill
def flood(start, cubes):
    queue = deque([start])
    visited = set()
    while queue:
        pos = queue.popleft()
        if pos not in visited and pos not in cubes:
            visited.add(pos)
            for axis in range(3):
                for offset in range(-1, 2):
                    if offset != 0:
                        adj = list(pos)
                        adj[axis] = offset + pos[axis]
                        adj = tuple(adj)
                        if is_valid(adj):
                            queue.append(adj)
    return visited

outside = flood((mins[0]-1,mins[1]-1,mins[2]-1), cubes)
part2 = get_surface_area(cubes, True, outside)
print('Part 2:', part2)
# puzzle.answer_b = part2
