from aocd.models import Puzzle
from dotenv import load_dotenv

load_dotenv()
puzzle = Puzzle(year=2022, day=2)

score = 0
for line in puzzle.input_data.splitlines():
    p1, p2 = line.split(' ')
    if p1 == 'A':
        if p2 == 'X':
            score += 1 + 3
        elif p2 == 'Y':
            score += 2 + 6
        elif p2 == 'Z':
            score += 3 + 0
    elif p1 == 'B':
        if p2 == 'X':
            score += 1 + 0
        elif p2 == 'Y':
            score += 2 + 3
        elif p2 == 'Z':
            score += 3 + 6
    elif p1 == 'C':
        if p2 == 'X':
            score += 1 + 6
        elif p2 == 'Y':
            score += 2 + 0
        elif p2 == 'Z':
            score += 3 + 3

print('Part 1:', score)
puzzle.answer_a = score

score = 0
for line in puzzle.input_data.splitlines():
    p1, p2 = line.split(' ')
    if p1 == 'A':
        if p2 == 'X':
            score += 0 + 3
        elif p2 == 'Y':
            score += 3 + 1
        elif p2 == 'Z':
            score += 6 + 2
    elif p1 == 'B':
        if p2 == 'X':
            score += 0 + 1
        elif p2 == 'Y':
            score += 3 + 2
        elif p2 == 'Z':
            score += 6 + 3
    elif p1 == 'C':
        if p2 == 'X':
            score += 0 + 2
        elif p2 == 'Y':
            score += 3 + 3
        elif p2 == 'Z':
            score += 6 + 1

print('Part 2:', score)
puzzle.answer_b = score
