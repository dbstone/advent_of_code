from aocd.models import Puzzle
from dotenv import load_dotenv
from collections import deque

load_dotenv()
puzzle = Puzzle(year=2022, day=20)

data = puzzle.input_data
# with open("2022/test.txt") as file:
#     data = file.read()

class Node:
    def __init__(self, val, idx) -> None:
        self.val = val
        self.idx = idx

nodes = [Node(int(line), i) for i, line in enumerate(data.splitlines())]

def mix(nodes):
    for i, node in enumerate(nodes):
        new_idx = (node.idx + node.val) % (len(nodes)-1)
        dist = new_idx - node.idx
        for node2 in nodes:
            if dist < 0:
                if node2.idx < node.idx and node2.idx >= new_idx:
                    node2.idx += 1
                    node2.idx %= len(nodes)
            elif dist > 0:
                if node2.idx > node.idx and node2.idx <= new_idx:
                    node2.idx -= 1
                    node2.idx %= len(nodes)
        node.idx = new_idx

def grove_sum(nodes):
    new = [0 for _ in range(len(nodes))]
    zero = 0
    for node in nodes:
        if node.val == 0:
            zero = node.idx
        new[node.idx] = node.val
    return new[(zero+1000)%len(new)] + new[(zero+2000)%len(new)] + new[(zero+3000)%len(new)]

mix(nodes)
part1 = grove_sum(nodes)
print('Part 1:', part1)
puzzle.answer_a = part1

nodes = [Node(int(line)*811589153, i) for i, line in enumerate(data.splitlines())]
for _ in range(10):
    mix(nodes)
part2 = grove_sum(nodes)
print('Part 2:', part2)
puzzle.answer_b = part2
